package com.gitlab.hear4me.watchapp.android;

import com.gitlab.hear4me.compartido.android.IntentFacadeAndroid;
import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.compartido.util.JsonUtil;

public class AlertaServiceAndroid {

    private AlertaService alertaService;
    private IntentFacadeAndroid intentFacadeAndroid;

    public AlertaServiceAndroid(
            AlertaService alertaService,
            IntentFacadeAndroid intentFacadeAndroid) {
        this.alertaService = alertaService;
        this.intentFacadeAndroid = intentFacadeAndroid;
    }

    public void mostrarAlertaSiguienteSiExiste() {
        Alerta alertaMasAntiguaSinVer = alertaService.buscarAlertaMasAntiguaSinVer();
        if (alertaMasAntiguaSinVer != null) {
            String alertaJson = JsonUtil.toJson(alertaMasAntiguaSinVer);
            intentFacadeAndroid.mostrarInterfazAlerta(alertaJson);
        }
    }
}
