package com.gitlab.hear4me.watchapp.model.repository;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.model.repository.AlertaRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AlertaRepositoryImpl implements AlertaRepository {

    private List<Alerta> alertasCache;

    public AlertaRepositoryImpl() {
        alertasCache = new ArrayList<>();
    }

    @Override
    public List<Alerta> buscarTodas() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void guardar(Alerta alerta) {
        alertasCache.add(alerta);
    }

    @Override
    public void actualizar(Alerta alerta) {
        alertasCache.stream()
                .filter((otraAlerta) -> otraAlerta.getId() == alerta.getId())
                .findFirst()
                .ifPresent((alertaEncontrada) -> alertaEncontrada.setVista(alerta.isVista()));
    }

    @Override
    public int contarTodasLasAlertasNoVistas() {
        return (int) alertasCache.stream()
                .filter((alerta) -> !alerta.isVista())
                .count();
    }

    @Override
    public void marcarTodasComoVistas() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void marcarTodasMenosLaUltima() {
        alertasCache.stream()
                .max((a1, a2) ->
                        Long.compare(a1.getTiempo().getTime(), a2.getTiempo().getTime()))
                .ifPresent((alerta) -> alerta.setVista(true));
    }

    @Override
    public List<Alerta> buscarTodasLasAlertasNoVistas() {
        return alertasCache.stream()
                .filter((alerta) -> !alerta.isVista())
                .collect(Collectors.toList());
    }

    @Override
    public void borrarTodas() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void borrarAlertasVistas() {
        throw new UnsupportedOperationException();
    }
}
