package com.gitlab.hear4me.watchapp.di.module;

import com.gitlab.hear4me.compartido.android.WearableServiceAndroid;
import com.gitlab.hear4me.compartido.model.repository.AlertaRepository;
import com.gitlab.hear4me.compartido.model.repository.ConfiguracionesRepository;
import com.gitlab.hear4me.compartido.model.repository.MicrofonoRepository;
import com.gitlab.hear4me.watchapp.model.repository.AlertaRepositoryImpl;
import com.gitlab.hear4me.watchapp.model.repository.ConfiguracionesRepositoryImpl;
import com.gitlab.hear4me.watchapp.model.repository.MicrofonoRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class WatchRepositoriesModule {

    @Provides
    @Singleton
    ConfiguracionesRepository configuracionesRepository(WearableServiceAndroid wearableServiceAndroid) {
        return new ConfiguracionesRepositoryImpl(wearableServiceAndroid);
    }

    @Provides
    @Singleton
    AlertaRepository alertaRepository() {
        return new AlertaRepositoryImpl();
    }

    @Provides
    @Singleton
    MicrofonoRepository microfonoRepository(WearableServiceAndroid wearableServiceAndroid) {
        return new MicrofonoRepositoryImpl(wearableServiceAndroid);
    }

}
