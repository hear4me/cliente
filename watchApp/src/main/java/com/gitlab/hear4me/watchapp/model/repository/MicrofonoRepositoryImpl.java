package com.gitlab.hear4me.watchapp.model.repository;

import com.gitlab.hear4me.compartido.android.WearableServiceAndroid;
import com.gitlab.hear4me.compartido.config.MensajesWearable;
import com.gitlab.hear4me.compartido.model.domain.EstadoMicrofono;
import com.gitlab.hear4me.compartido.model.repository.MicrofonoRepository;
import com.gitlab.hear4me.compartido.util.JsonUtil;

public class MicrofonoRepositoryImpl implements MicrofonoRepository {

    private WearableServiceAndroid wearableServiceAndroid;

    private EstadoMicrofono estadoMicrofonoCache;


    public MicrofonoRepositoryImpl(WearableServiceAndroid wearableServiceAndroid) {
        this.wearableServiceAndroid = wearableServiceAndroid;
    }

    @Override
    public EstadoMicrofono cargar() {
        if (estadoMicrofonoCache == null) {
            estadoMicrofonoCache = new EstadoMicrofono(false);
            wearableServiceAndroid.enviarMensaje(MensajesWearable.WATCH_REQ_ESTADO_MIC_PATH, null);
        }
        return estadoMicrofonoCache;
    }

    @Override
    public void guardar(EstadoMicrofono estado) {
        estadoMicrofonoCache.setEstado(estado.getEstado());
        String estadoMicrofonoJson = JsonUtil.toJson(estado);
        wearableServiceAndroid.enviarMensaje(MensajesWearable.WATCH_WRITE_ESTADO_MIC_PATH, estadoMicrofonoJson.getBytes());
    }

}
