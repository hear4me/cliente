package com.gitlab.hear4me.watchapp.di.module;

import com.gitlab.hear4me.compartido.model.repository.AlertaRepository;
import com.gitlab.hear4me.compartido.model.repository.ConfiguracionesRepository;
import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class WatchServicesModule {

    @Provides
    @Singleton
    public ConfiguracionesService configuracionesService(ConfiguracionesRepository configuracionesRepository) {
        return new ConfiguracionesService(configuracionesRepository);
    }

    @Provides
    @Singleton
    public AlertaService alertaService(AlertaRepository alertaRepository) {
        return new AlertaService(alertaRepository);
    }

}
