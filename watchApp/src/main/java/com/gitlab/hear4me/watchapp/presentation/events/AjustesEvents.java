package com.gitlab.hear4me.watchapp.presentation.events;

import android.view.View;

public interface AjustesEvents {
    void irAAtras(View view);
    void clickModoCasa(View view);
    void clickModoCalle(View view);
    void clickModoDormir(View view);
}
