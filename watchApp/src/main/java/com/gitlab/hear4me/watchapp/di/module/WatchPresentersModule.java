package com.gitlab.hear4me.watchapp.di.module;

import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.compartido.model.service.MicrofonoService;
import com.gitlab.hear4me.watchapp.android.AlertaServiceAndroid;
import com.gitlab.hear4me.watchapp.presentation.presenter.AjustesPresenter;
import com.gitlab.hear4me.watchapp.presentation.presenter.AlertaPresenter;
import com.gitlab.hear4me.watchapp.presentation.presenter.MainPresenter;
import com.gitlab.hear4me.watchapp.presentation.presenter.impl.AjustesPresenterImpl;
import com.gitlab.hear4me.watchapp.presentation.presenter.impl.AlertaPresenterImpl;
import com.gitlab.hear4me.watchapp.presentation.presenter.impl.MainPresenterImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class WatchPresentersModule {

    @Provides
    @Singleton
    MainPresenter mainPresenter(MicrofonoService microfonoService) {
        return new MainPresenterImpl(microfonoService);
    }

    @Provides
    @Singleton
    AjustesPresenter ajustesPresenter(ConfiguracionesService configuracionesService) {
        return new AjustesPresenterImpl(configuracionesService);
    }

    @Provides
    @Singleton
    AlertaPresenter alertaPresenter(AlertaService alertaService,
                                    AlertaServiceAndroid alertaServiceAndroid,
                                    ConfiguracionesService configuracionesService) {
        return new AlertaPresenterImpl(alertaService, alertaServiceAndroid, configuracionesService);
    }

}
