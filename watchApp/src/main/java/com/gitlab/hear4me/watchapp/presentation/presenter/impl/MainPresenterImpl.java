package com.gitlab.hear4me.watchapp.presentation.presenter.impl;

import android.view.View;

import com.gitlab.hear4me.compartido.model.service.MicrofonoService;
import com.gitlab.hear4me.compartido.presentation.view.ActivityView;
import com.gitlab.hear4me.watchapp.presentation.presenter.MainPresenter;
import com.gitlab.hear4me.watchapp.presentation.view.MainView;

import javax.inject.Inject;

public class MainPresenterImpl implements MainPresenter {

    private MainView mainView;
    private MicrofonoService microfonoService;

    @Inject
    public MainPresenterImpl(MicrofonoService microfonoService) {
        this.microfonoService = microfonoService;
    }

    @Override
    public void irAAjustes(View view) {

    }

    @Override
    public boolean activarMicrofono(View view) {
        boolean microfonoActivo = microfonoService.obtenerEstadoMicrofono();
        if (!microfonoActivo) {
            microfonoService.activarMicrofono();
        } else {
            microfonoService.desactivarMicrofono();
        }
        return !microfonoActivo;
    }

    @Override
    public void onCreate(ActivityView activityView) {

    }

    @Override
    public void onStart(ActivityView activityView) {

    }

    @Override
    public void onResume(ActivityView activityView) {
        this.mainView = (MainView) activityView;
        boolean estadoMicrofono = microfonoService.obtenerEstadoMicrofono();
        mainView.actualizarEstadoMicrofono(estadoMicrofono);
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void actualizarEstadoMicrofono(boolean estado) {
        mainView.actualizarEstadoMicrofono(estado);
    }
}
