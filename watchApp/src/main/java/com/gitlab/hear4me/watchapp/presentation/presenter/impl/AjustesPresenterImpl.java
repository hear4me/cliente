package com.gitlab.hear4me.watchapp.presentation.presenter.impl;

import android.view.View;

import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.compartido.presentation.view.ActivityView;
import com.gitlab.hear4me.watchapp.WatchApp;
import com.gitlab.hear4me.watchapp.presentation.presenter.AjustesPresenter;
import com.gitlab.hear4me.watchapp.presentation.view.AjustesView;

import javax.inject.Inject;

public class AjustesPresenterImpl implements AjustesPresenter {

    private AjustesView view;
    private ConfiguracionesService configuracionesService;

    @Inject
    public AjustesPresenterImpl(ConfiguracionesService configuracionesService) {
        this.configuracionesService = configuracionesService;
    }

    @Override
    public void onCreate(ActivityView activityView) {

    }

    @Override
    public void onStart(ActivityView activityView) {

    }

    @Override
    public void onResume(ActivityView activityView) {
        view = (AjustesView) activityView;
        activarModoCorrespondiente();
        WatchApp.uiAjustesIniciada = true;
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void irAAtras(View view) {

    }

    @Override
    public void clickModoCasa(View view) {
        ModoConfiguracion modoConfiguracionSiguiente = ModoConfiguracion.CASA;
        actualizarConfiguracion(modoConfiguracionSiguiente);
    }

    @Override
    public void clickModoCalle(View view) {
        ModoConfiguracion modoConfiguracionSiguiente = ModoConfiguracion.CALLE;
        actualizarConfiguracion(modoConfiguracionSiguiente);
    }

    @Override
    public void clickModoDormir(View view) {
        ModoConfiguracion modoConfiguracionActual
                = configuracionesService.obtenerModoConfiguracionActivo();

        ModoConfiguracion modoConfiguracionSiguiente = null;

        switch (modoConfiguracionActual) {
            case CASA:
            case CALLE:
            case CALLE_DORMIR:
                modoConfiguracionSiguiente = ModoConfiguracion.CASA_DORMIR;
                break;
            case CASA_DORMIR:
                modoConfiguracionSiguiente = ModoConfiguracion.CASA;
                break;
        }

        if (modoConfiguracionSiguiente == null) {
            throw new IllegalStateException("Modo de configuración actual inválido");
        }

        actualizarConfiguracion(modoConfiguracionSiguiente);
    }

    @Override
    public void activarModoCorrespondiente() {
        ModoConfiguracion modoConfiguracionActual
                = configuracionesService.obtenerModoConfiguracionActivo();

        switch (modoConfiguracionActual) {
            case CASA:
                view.activarModoCasa();
                view.desactivarModoDormir();
                break;
            case CALLE:
            case CALLE_DORMIR:
                view.activarModoCalle();
                view.desactivarModoDormir();
                break;
            case CASA_DORMIR:
                view.activarModoCasa();
                view.activarModoDormir();
                break;
        }
    }

    private void actualizarConfiguracion(ModoConfiguracion modoConfiguracionSiguiente) {
        configuracionesService.cambiarModoConfiguracion(modoConfiguracionSiguiente);
    }

}
