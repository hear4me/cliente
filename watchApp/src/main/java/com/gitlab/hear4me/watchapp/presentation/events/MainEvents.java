package com.gitlab.hear4me.watchapp.presentation.events;

import android.view.View;

public interface MainEvents {
    void irAAjustes(View view);
    boolean activarMicrofono(View view);
}
