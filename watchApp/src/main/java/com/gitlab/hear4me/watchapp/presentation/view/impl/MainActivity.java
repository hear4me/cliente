package com.gitlab.hear4me.watchapp.presentation.view.impl;

import android.content.Intent;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.Button;

import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.TextViewCompat;

import com.gitlab.hear4me.watchapp.R;
import com.gitlab.hear4me.watchapp.WatchApp;
import com.gitlab.hear4me.watchapp.presentation.presenter.MainPresenter;
import com.gitlab.hear4me.watchapp.presentation.view.MainView;

import javax.inject.Inject;

public class MainActivity extends WearableActivity implements MainView {

    @Inject
    MainPresenter mainPresenter;

    private Button btnAjustes;
    private Button btnGrabar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WatchApp.getComponent().inject(this);

        btnAjustes = findViewById(R.id.btnAjustesBack);
        btnAjustes.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));
        btnAjustes.setOnClickListener(this::irAAjustes);

        btnGrabar = findViewById(R.id.btnGrabar);
        btnGrabar.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));
        btnGrabar.setOnClickListener(this::activarMicrofono);

        setAmbientEnabled();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mainPresenter.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void actualizarEstadoMicrofono(boolean estado) {
        setBtnMicrofonoActivo(estado);
    }

    @Override
    public void irAAjustes(View view) {
        mainPresenter.irAAjustes(view);
        Intent intent = new Intent(this, AjustesActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @Override
    public boolean activarMicrofono(View view) {
        return mainPresenter.activarMicrofono(view);
    }

    private void setBtnMicrofonoActivo(boolean activo) {
        if (activo) {
            btnGrabar.setText(R.string.icon_microphone);
            TextViewCompat.setTextAppearance(btnGrabar, R.style.BotonMic_On);
            btnGrabar.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));

            TextViewCompat.setTextAppearance(btnAjustes, R.style.BotonIcono_MicOn);
            btnAjustes.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));
        } else {
            btnGrabar.setText(R.string.icon_microphone_slash);
            TextViewCompat.setTextAppearance(btnGrabar, R.style.BotonMic_Off);
            btnGrabar.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));

            TextViewCompat.setTextAppearance(btnAjustes, R.style.BotonIcono_MicOff);
            btnAjustes.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));
        }
    }
}
