package com.gitlab.hear4me.watchapp.android;

import android.content.Context;
import android.content.Intent;

import com.gitlab.hear4me.compartido.android.IntentFacadeAndroid;
import com.gitlab.hear4me.watchapp.presentation.view.impl.AlertaActivity;


public class WatchIntentFacadeAndroid implements IntentFacadeAndroid {

    private Context context;

    public WatchIntentFacadeAndroid(Context context) {
        this.context = context;
    }

    public void activarMicrofono() {

    }

    public void desactivarMicrofono() {

    }

    public void mostrarInterfazAlerta(String alertaJson) {
        Intent intent = new Intent(context, AlertaActivity.class)
                .setAction("hear4me_alerta")
                .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_ANIMATION)
                .putExtra("resultado_clasificacion", alertaJson);
        context.startActivity(intent);
    }

}
