package com.gitlab.hear4me.watchapp.presentation.view;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.presentation.view.ActivityView;
import com.gitlab.hear4me.watchapp.presentation.events.AlertaEvents;

public interface AlertaView extends ActivityView, AlertaEvents {
    void mostrarAlerta(Alerta alerta);
    void activarVibracion();
    void cerrarAlerta();
}
