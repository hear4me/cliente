package com.gitlab.hear4me.watchapp;

import android.app.Application;

import com.gitlab.hear4me.watchapp.di.component.DaggerWatchAppComponent;
import com.gitlab.hear4me.watchapp.di.component.WatchAppComponent;
import com.gitlab.hear4me.watchapp.di.module.AppModule;
import com.gitlab.hear4me.watchapp.di.module.WatchPresentersModule;
import com.gitlab.hear4me.watchapp.di.module.WatchRepositoriesModule;
import com.gitlab.hear4me.watchapp.di.module.WatchServicesModule;

public class WatchApp extends Application {

    public static boolean uiAjustesIniciada = false;

    private static WatchAppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = buildAppComponent();
    }

    public static WatchAppComponent getComponent() {
        return component;
    }

    private WatchAppComponent buildAppComponent() {
        return DaggerWatchAppComponent.builder()
                .appModule(new AppModule(this))
                .watchPresentersModule(new WatchPresentersModule())
                .watchRepositoriesModule(new WatchRepositoriesModule())
                .watchServicesModule(new WatchServicesModule())
                .build();
    }
}
