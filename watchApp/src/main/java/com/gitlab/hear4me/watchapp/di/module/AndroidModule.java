package com.gitlab.hear4me.watchapp.di.module;

import android.content.Context;

import com.gitlab.hear4me.compartido.android.IntentFacadeAndroid;
import com.gitlab.hear4me.compartido.android.WearableServiceAndroid;
import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.watchapp.android.AlertaServiceAndroid;
import com.gitlab.hear4me.watchapp.android.DatalayerListenerService;
import com.gitlab.hear4me.watchapp.android.WatchIntentFacadeAndroid;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AndroidModule {

    @Provides
    @Singleton
    public IntentFacadeAndroid intentFacadeAndroid(Context context) {
        return new WatchIntentFacadeAndroid(context);
    }

    @Provides
    @Singleton
    public DatalayerListenerService datalayerListenerService() {
        return new DatalayerListenerService();
    }

    @Provides
    @Singleton
    public WearableServiceAndroid wearableServiceAndroid(Context context) {
        return new WearableServiceAndroid(context);
    }

    @Provides
    @Singleton
    public AlertaServiceAndroid alertaServiceAndroid(AlertaService alertaService,
                                                     IntentFacadeAndroid intentFacadeAndroid) {
        return new AlertaServiceAndroid(alertaService, intentFacadeAndroid);
    }

}