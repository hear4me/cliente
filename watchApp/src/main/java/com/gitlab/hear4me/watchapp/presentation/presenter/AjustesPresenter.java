package com.gitlab.hear4me.watchapp.presentation.presenter;

import com.gitlab.hear4me.compartido.presentation.events.ActivityEvents;
import com.gitlab.hear4me.watchapp.presentation.events.AjustesEvents;

public interface AjustesPresenter extends AjustesEvents, ActivityEvents {
    void activarModoCorrespondiente();
}
