package com.gitlab.hear4me.watchapp.presentation.view.impl;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.Button;

import androidx.core.content.res.ResourcesCompat;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.util.JsonUtil;
import com.gitlab.hear4me.watchapp.R;
import com.gitlab.hear4me.watchapp.WatchApp;
import com.gitlab.hear4me.watchapp.presentation.presenter.AlertaPresenter;
import com.gitlab.hear4me.watchapp.presentation.view.AlertaView;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;

public class AlertaActivity extends WearableActivity implements AlertaView {

    @Inject
    AlertaPresenter alertaPresenter;

    private static Handler actualizadorContadorTiempoTranscurrido = new Handler();
    private static Handler actualizadorContadorAlertas = new Handler();

    private Vibrator vibracion;
    private CircleImageView imgCategoriaDetectada;
    private Button btnVerAlertaMasReciente;
    private Button btnDescartarAlerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alerta);
        WatchApp.getComponent().inject(this);
        alertaPresenter.onCreate(this);

        imgCategoriaDetectada = findViewById(R.id.imgCategoriaDetectada);
        imgCategoriaDetectada.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        btnVerAlertaMasReciente = findViewById(R.id.btnVerAlertaMasReciente);
        btnVerAlertaMasReciente.setOnClickListener(this::mostrarAlertaMasReciente);
        btnVerAlertaMasReciente.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));

        btnDescartarAlerta = findViewById(R.id.btnDescartarAlerta);
        btnDescartarAlerta.setOnClickListener(this::descartarAlertaActual);
        btnDescartarAlerta.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));


        activarAlerta(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        alertaPresenter.onResume(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        activarAlerta(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        alertaPresenter.onPause();
        actualizadorContadorTiempoTranscurrido.removeCallbacksAndMessages(null);
        actualizadorContadorAlertas.removeCallbacksAndMessages(null);
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        alertaPresenter.onDestroy();
    }

    @Override
    public void alertaActivada(Alerta alerta) {
        alertaPresenter.alertaActivada(alerta);
    }

    @Override
    public void mostrarAlertaMasReciente(View view) {
        alertaPresenter.mostrarAlertaMasReciente(view);
    }

    @Override
    public void descartarAlertaActual(View view) {
        alertaPresenter.descartarAlertaActual(view);
    }

    @Override
    public void activarVibracion() {
        vibracion = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {0, 400, 100, 400};
        vibracion.vibrate(pattern, -1);
    }

    @Override
    public void cerrarAlerta() {
        finish();
    }

    @Override
    public void mostrarAlerta(Alerta alerta) {
        mostrarAlerta(alerta, imgCategoriaDetectada);
    }

    private void activarAlerta(Intent intent) {
        if (intent.getExtras() != null) {
            String resultadoJson = intent.getExtras().getString("resultado_clasificacion");
            Alerta alerta = parsearResultadoClasificacion(resultadoJson);
            alertaActivada(alerta);
        }
    }

    private void mostrarAlerta(Alerta alerta, CircleImageView imgResultado) {
        int idImagenCategoria = getResources().getIdentifier(
                alerta.getPrediccion().getCategoria().toString(), "drawable", getPackageName());
        imgResultado.setImageResource(idImagenCategoria);
    }

    private Alerta parsearResultadoClasificacion(String resultadoJson) {
        return JsonUtil.fromJson(resultadoJson, Alerta.class);
    }

}
