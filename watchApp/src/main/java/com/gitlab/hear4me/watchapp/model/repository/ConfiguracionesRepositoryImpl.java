package com.gitlab.hear4me.watchapp.model.repository;

import com.gitlab.hear4me.compartido.android.WearableServiceAndroid;
import com.gitlab.hear4me.compartido.config.MensajesWearable;
import com.gitlab.hear4me.compartido.model.domain.Configuraciones;
import com.gitlab.hear4me.compartido.model.repository.ConfiguracionesRepository;
import com.gitlab.hear4me.compartido.util.JsonUtil;

public class ConfiguracionesRepositoryImpl implements ConfiguracionesRepository {

    private WearableServiceAndroid wearableServiceAndroid;
    private Configuraciones configuracionesCache;

    public ConfiguracionesRepositoryImpl(WearableServiceAndroid wearableServiceAndroid) {
        this.wearableServiceAndroid = wearableServiceAndroid;
    }

    @Override
    public Configuraciones cargar() {
        if(configuracionesCache == null) {
            configuracionesCache = new Configuraciones();
            wearableServiceAndroid.enviarMensaje(MensajesWearable.WATCH_REQ_CONFIG_PATH, null);
        }
        return configuracionesCache;
    }

    @Override
    public void guardar(Configuraciones configuraciones) {
        configuracionesCache = configuraciones;
        sincronizar();
    }

    private void sincronizar() {
        String configuracionesJson = JsonUtil.toJson(configuracionesCache);
        wearableServiceAndroid.enviarMensaje(MensajesWearable.WATCH_WRITE_CONFIG_PATH, configuracionesJson.getBytes());
    }
}
