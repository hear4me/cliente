package com.gitlab.hear4me.watchapp.presentation.view;

import com.gitlab.hear4me.compartido.presentation.view.ActivityView;
import com.gitlab.hear4me.watchapp.presentation.events.AjustesEvents;

public interface AjustesView extends ActivityView, AjustesEvents {
    void activarModoCasa();
    void activarModoCalle();
    void activarModoDormir();
    void desactivarModoDormir();
}
