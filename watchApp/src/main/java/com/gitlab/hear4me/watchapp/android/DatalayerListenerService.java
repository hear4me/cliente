package com.gitlab.hear4me.watchapp.android;

import android.content.Context;

import com.gitlab.hear4me.compartido.android.IntentFacadeAndroid;
import com.gitlab.hear4me.compartido.config.MensajesWearable;
import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.model.domain.Configuraciones;
import com.gitlab.hear4me.compartido.model.domain.EstadoMicrofono;
import com.gitlab.hear4me.compartido.model.repository.AlertaRepository;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.compartido.model.service.MicrofonoService;
import com.gitlab.hear4me.compartido.util.JsonUtil;
import com.gitlab.hear4me.compartido.util.TareaUtil;
import com.gitlab.hear4me.watchapp.WatchApp;
import com.gitlab.hear4me.watchapp.presentation.presenter.AjustesPresenter;
import com.gitlab.hear4me.watchapp.presentation.presenter.MainPresenter;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import javax.inject.Inject;

public class DatalayerListenerService extends WearableListenerService {

    @Inject
    Context context;

    @Inject
    ConfiguracionesService configuracionesService;

    @Inject
    MicrofonoService microfonoService;

    @Inject
    AjustesPresenter ajustesPresenter;

    @Inject
    MainPresenter mainPresenter;

    @Inject
    AlertaRepository alertaRepository;

    @Inject
    IntentFacadeAndroid intentFacadeAndroid;

    public DatalayerListenerService() {}

    @Override
    public void onCreate() {
        super.onCreate();
        WatchApp.getComponent().inject(this);
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);

        switch (messageEvent.getPath()) {
            /** MAIN */
            case MensajesWearable.PHONE_WRITE_ESTADO_MIC_PATH:
            case MensajesWearable.PHONE_REQ_ANS_ESTADO_MIC_PATH:
                actualizarEstadoMicrofono(messageEvent.getData());
                break;
            /** ALERTA */
            case MensajesWearable.PHONE_ACTIVA_ALERTA_PATH:
                guardarYActivarAlerta(messageEvent.getData());
                break;
            /** CONFIGURACION */
            case MensajesWearable.PHONE_WRITE_CONFIG_PATH:
            case MensajesWearable.PHONE_REQ_ANS_CONFIG_PATH:
                actualizarConfiguracion(messageEvent.getData());
                break;
        }
    }

    private void actualizarEstadoMicrofono(byte[] data) {
        String nuevoEstadoMicJson = new String(data);
        EstadoMicrofono nuevoEstadoMicrofono = JsonUtil.fromJson(nuevoEstadoMicJson, EstadoMicrofono.class);
        microfonoService.actualizarEstadoMicrofono(nuevoEstadoMicrofono.getEstado());
        TareaUtil.ejecutarEnUiThread(() ->
                mainPresenter.actualizarEstadoMicrofono(nuevoEstadoMicrofono.getEstado()));
    }

    /***
     * Se ejecuta cuando en el celular se activa una alerta
     */
    private void guardarYActivarAlerta(byte[] data) {
        String alertaJson = new String(data);

        Alerta alerta = JsonUtil.fromJson(alertaJson, Alerta.class);
        alertaRepository.guardar(alerta);

        intentFacadeAndroid.mostrarInterfazAlerta(alertaJson);
    }

    /***
     * Se ejecuta cuando en el celular se actualiza la configuracion
     */
    private void actualizarConfiguracion(byte[] data) {
        String configuracionesJson = new String(data);
        Configuraciones configuraciones = JsonUtil.fromJson(configuracionesJson, Configuraciones.class);
        configuracionesService.actualizarModoConfiguracion(configuraciones.getModoConfiguracionSeleccionado());
        if (WatchApp.uiAjustesIniciada) {
            TareaUtil.ejecutarEnUiThread(ajustesPresenter::activarModoCorrespondiente);
        }
    }
}
