package com.gitlab.hear4me.watchapp.presentation.view;

import com.gitlab.hear4me.compartido.presentation.view.ActivityView;
import com.gitlab.hear4me.watchapp.presentation.events.MainEvents;

public interface MainView extends ActivityView, MainEvents {
    void actualizarEstadoMicrofono(boolean estado);
}
