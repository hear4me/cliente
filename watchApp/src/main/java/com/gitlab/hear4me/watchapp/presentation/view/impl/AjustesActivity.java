package com.gitlab.hear4me.watchapp.presentation.view.impl;

import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.Button;

import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.TextViewCompat;

import com.gitlab.hear4me.watchapp.R;
import com.gitlab.hear4me.watchapp.WatchApp;
import com.gitlab.hear4me.watchapp.presentation.presenter.AjustesPresenter;
import com.gitlab.hear4me.watchapp.presentation.view.AjustesView;

import javax.inject.Inject;

public class AjustesActivity extends WearableActivity implements AjustesView {

    @Inject
    AjustesPresenter ajustesPresenter;

    private Button btnAtras;
    private Button btnModoCasa;
    private Button btnModoCalle;
    private Button btnModoDormir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajustes);
        WatchApp.getComponent().inject(this);

        btnAtras = findViewById(R.id.btnAjustesBack);
        btnAtras.setOnClickListener(this::irAAtras);
        btnAtras.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));

        btnModoCasa = findViewById(R.id.btnCasa);
        btnModoCasa.setOnClickListener(this::clickModoCasa);
        btnModoCasa.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));

        btnModoCalle = findViewById(R.id.btnCalle);
        btnModoCalle.setOnClickListener(this::clickModoCalle);
        btnModoCalle.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));

        btnModoDormir = findViewById(R.id.btnDormir);
        btnModoDormir.setOnClickListener(this::clickModoDormir);
        btnModoDormir.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));

        btnAtras.setOnClickListener(this::irAAtras);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ajustesPresenter.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ajustesPresenter.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void activarModoCasa() {
        modoCasaOn();
        modoCalleOff();
    }

    @Override
    public void activarModoCalle() {
        modoCalleOn();
        modoCasaOff();
    }

    @Override
    public void activarModoDormir() {
        modoDormirOn();

    }

    @Override
    public void desactivarModoDormir() {
        modoDormirOff();
    }

    @Override
    public void irAAtras(View view) {
        ajustesPresenter.irAAtras(view);
        finish();
    }

    @Override
    public void clickModoCasa(View view) {
        ajustesPresenter.clickModoCasa(view);
    }

    @Override
    public void clickModoCalle(View view) {
        ajustesPresenter.clickModoCalle(view);
    }

    @Override
    public void clickModoDormir(View view) {
        ajustesPresenter.clickModoDormir(view);
    }

    private void modoCasaOn() {
        btnModoCasa.setBackgroundResource(R.drawable.selector_boton_izquierdo_activo);
        TextViewCompat.setTextAppearance(btnModoCasa, R.style.BotonIcono_SeleccionadoIzquierdo);
        btnModoCasa.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));
    }

    private void modoCasaOff() {
        btnModoCasa.setBackgroundResource(R.drawable.fondo_selector);
        TextViewCompat.setTextAppearance(btnModoCasa, R.style.BotonIcono);
        btnModoCasa.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));
    }

    private void modoCalleOn() {
        btnModoCalle.setBackgroundResource(R.drawable.boton_circulo_activo);
        TextViewCompat.setTextAppearance(btnModoCalle, R.style.BotonIcono_SeleccionadoSeparado);
        btnModoCalle.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));
    }

    private void modoCalleOff() {
        btnModoCalle.setBackgroundResource(R.drawable.fondo_selector);
        TextViewCompat.setTextAppearance(btnModoCalle, R.style.BotonIcono);
        btnModoCalle.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));
    }

    private void modoDormirOn() {
        btnModoDormir.setBackgroundResource(R.drawable.selector_boton_derecho_activo);
        TextViewCompat.setTextAppearance(btnModoDormir, R.style.BotonIcono_SeleccionadoDerecho);
        btnModoDormir.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));
    }

    private void modoDormirOff() {
        btnModoDormir.setBackgroundResource(R.drawable.fondo_selector);
        TextViewCompat.setTextAppearance(btnModoDormir, R.style.BotonIcono);
        btnModoDormir.setTypeface(ResourcesCompat.getFont(this, R.font.fa_solid_900));
    }

}
