package com.gitlab.hear4me.watchapp.presentation.presenter.impl;

import android.view.View;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.model.domain.Configuracion;
import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.model.domain.ModoNotificacion;
import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.compartido.presentation.view.ActivityView;
import com.gitlab.hear4me.compartido.util.TareaUtil;
import com.gitlab.hear4me.watchapp.android.AlertaServiceAndroid;
import com.gitlab.hear4me.watchapp.presentation.presenter.AlertaPresenter;
import com.gitlab.hear4me.watchapp.presentation.view.AlertaView;

import java.util.Map;

import javax.inject.Inject;

public class AlertaPresenterImpl implements AlertaPresenter {

    private static boolean alertaVisible = false;

    private AlertaView alertaView;

    private AlertaService alertaService;
    private AlertaServiceAndroid alertaServiceAndroid;
    private ConfiguracionesService configuracionesService;

    @Inject
    public AlertaPresenterImpl(AlertaService alertaService,
                               AlertaServiceAndroid alertaServiceAndroid,
                               ConfiguracionesService configuracionesService) {
        this.alertaService = alertaService;
        this.alertaServiceAndroid = alertaServiceAndroid;
        this.configuracionesService = configuracionesService;
    }

    @Override
    public void alertaActivada(Alerta alerta) {
        if (!alertaVisible) {
            alertaView.mostrarAlerta(alerta);
            activarNotificaciones();
            alertaVisible = true;
        }
    }

    @Override
    public void mostrarAlertaMasReciente(View view) {
        alertaVisible = false;
        TareaUtil.ejecutar(() -> {
            alertaService.marcarTodasLasAlertasMenosLaUltima();
            alertaServiceAndroid.mostrarAlertaSiguienteSiExiste();
        });
        cerrarVistaSiNoHayMasAlertas();
    }

    @Override
    public void descartarAlertaActual(View view) {
        alertaVisible = false;
        TareaUtil.ejecutar(() -> {
            alertaService.marcarAlertaActual();
            alertaServiceAndroid.mostrarAlertaSiguienteSiExiste();
        });
        alertaView.cerrarAlerta();
    }

    @Override
    public void onCreate(ActivityView activityView) {
        this.alertaView = (AlertaView) activityView;
    }

    @Override
    public void onStart(ActivityView activityView) {

    }

    @Override
    public void onResume(ActivityView activityView) {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {
        alertaVisible = false;
        TareaUtil.ejecutar(() -> alertaService.marcarAlertaActual());
    }

    private void activarNotificaciones() {
        ModoConfiguracion modoActual =
                configuracionesService.obtenerModoConfiguracionActivo();
        Configuracion configuracion =
                configuracionesService.obtenerConfiguracion(modoActual);

        Map<ModoNotificacion, Boolean> modosNotificacion = configuracion.getModosNotificacion();
        if (modosNotificacion.get(ModoNotificacion.VIBRACION)) {
            alertaView.activarVibracion();
        }
    }

    private void cerrarVistaSiNoHayMasAlertas() {
        TareaUtil.ejecutar(() -> {
            if (alertaService.contarTodasLasAlertasNoVistas() - 1 > 0) {
                alertaView.cerrarAlerta();
            }
        });
    }

}
