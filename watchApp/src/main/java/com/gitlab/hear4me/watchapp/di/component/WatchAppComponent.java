package com.gitlab.hear4me.watchapp.di.component;

import com.gitlab.hear4me.watchapp.android.DatalayerListenerService;
import com.gitlab.hear4me.watchapp.di.module.AndroidModule;
import com.gitlab.hear4me.watchapp.di.module.AppModule;
import com.gitlab.hear4me.watchapp.di.module.WatchPresentersModule;
import com.gitlab.hear4me.watchapp.di.module.WatchRepositoriesModule;
import com.gitlab.hear4me.watchapp.di.module.WatchServicesModule;
import com.gitlab.hear4me.watchapp.presentation.view.impl.AjustesActivity;
import com.gitlab.hear4me.watchapp.presentation.view.impl.AlertaActivity;
import com.gitlab.hear4me.watchapp.presentation.view.impl.MainActivity;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AndroidModule.class,
        AppModule.class,
        WatchPresentersModule.class,
        WatchRepositoriesModule.class,
        WatchServicesModule.class})
public interface WatchAppComponent {
    void inject(MainActivity mainActivity);
    void inject(AjustesActivity ajustesActivity);
    void inject(AlertaActivity alertaActivity);
    void inject(DatalayerListenerService datalayerListenerService);
}
