package com.gitlab.hear4me.phoneApp.model.service;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.phoneApp.TestBase;
import com.gitlab.hear4me.phoneApp.builder.AlertaBuilder;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class AlertaServiceTest extends TestBase {

    @Test
    public void buscarAlertaMasAntiguaNoVista_conUnaAlertaGuardada_retornaAlertaGuardada() {
        Alerta nuevaAlerta = new AlertaBuilder().conTiempo(100).build();

        alertaRepository.guardar(nuevaAlerta);

        Alerta alerta = alertaService.buscarAlertaMasAntiguaSinVer();
        assertThat(alerta)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(nuevaAlerta);
    }

    @Test
    public void buscarAlertaMasAntiguaNoVista_conDosAlertasGuardadas_retornaAlertaMasAntigua() {
        Alerta alertaMasAntigua = new AlertaBuilder().conTiempo(100).build();
        Alerta alertaMasNueva = new AlertaBuilder().conTiempo(150).build();

        alertaRepository.guardar(alertaMasAntigua);
        alertaRepository.guardar(alertaMasNueva);

        Alerta alerta = alertaService.buscarAlertaMasAntiguaSinVer();
        assertThat(alerta)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(alertaMasAntigua);
    }

    @Test
    public void buscarAlertaMasAntiguaNoVista_conDosAlertasGuardadasDesordenadas_retornaAlertaMasAntigua() {
        Alerta alertaMasAntigua = new AlertaBuilder().conTiempo(100).build();
        Alerta alertaMasNueva = new AlertaBuilder().conTiempo(150).build();

        alertaRepository.guardar(alertaMasNueva);
        alertaRepository.guardar(alertaMasAntigua);

        Alerta alerta = alertaService.buscarAlertaMasAntiguaSinVer();
        assertThat(alerta)
                .usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(alertaMasAntigua);
    }

    @Test
    public void buscarAlertaMasAntiguaNoVista_sinAlertasSinVer_retornaNull() {
        Alerta alerta = alertaService.buscarAlertaMasAntiguaSinVer();
        assertThat(alerta).isNull();
    }

    @Test
    public void borrarAlertasVistas_conUnaAlertaVistaGuardada_retornaListaVacia() {
        Alerta alerta = new AlertaBuilder().conVista(true).build();

        alertaRepository.guardar(alerta);

        alertaService.borrarAlertasVistas();

        List<Alerta> alertas = alertaRepository.buscarTodas();

        assertThat(alertas).isEmpty();
    }

    @Test
    public void borrarAlertasVistas_conUnaAlertaNoVistaGuardada_retornaListaConAlertaNoVista() {
        Alerta alerta = new AlertaBuilder().build();

        alertaRepository.guardar(alerta);

        alertaService.borrarAlertasVistas();

        List<Alerta> alertas = alertaRepository.buscarTodas();

        assertThat(alertas).isNotEmpty();
    }

    @Test
    public void borrarAlertasVistas_conUnaAlertaVistaYUnaNoVistaGuardada_retornaListaConAlertaNoVista() {
        Alerta alerta = new AlertaBuilder().build();

        alertaRepository.guardar(alerta);

        alertaService.borrarAlertasVistas();

        List<Alerta> alertas = alertaRepository.buscarTodas();

        assertThat(alertas).hasSize(1);
        assertThat(alertas.get(0))
                .isEqualToComparingOnlyGivenFields(alerta, "vista");
    }

    @Test
    public void marcarAlertaActual_conUnaAlertaSinMarcar_alertaMarcada() {
        Alerta alertaSinMarcar = new AlertaBuilder().build();
        alertaRepository.guardar(alertaSinMarcar);

        alertaService.marcarAlertaActual();

        assertThat(alertaRepository.contarTodasLasAlertasNoVistas()).isEqualTo(0);
    }

    @Test
    public void marcarAlertaActual_sinAlertas_lanzaExcepcion() {
        assertThatExceptionOfType(IllegalStateException.class)
                .isThrownBy(() -> alertaService.marcarAlertaActual())
                .withMessage("No existe ninguna alerta para marcar");
    }
}
