package com.gitlab.hear4me.phoneApp.di.component;

import com.gitlab.hear4me.phoneApp.TestBase;
import com.gitlab.hear4me.phoneApp.di.module.AndroidModule;
import com.gitlab.hear4me.phoneApp.di.module.AppModule;
import com.gitlab.hear4me.phoneApp.di.module.FileModule;
import com.gitlab.hear4me.phoneApp.di.module.ModelLoaderModule;
import com.gitlab.hear4me.phoneApp.di.module.PhonePresentersModule;
import com.gitlab.hear4me.phoneApp.di.module.PhoneRepositoriesModule;
import com.gitlab.hear4me.phoneApp.di.module.PhoneServicesModule;
import com.gitlab.hear4me.phoneApp.di.module.TestDatabaseModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        FileModule.class,
        ModelLoaderModule.class,
        PhonePresentersModule.class,
        PhoneRepositoriesModule.class,
        PhoneServicesModule.class,
        AndroidModule.class,
        TestDatabaseModule.class})
public interface TestAppComponent {
    void inject(TestBase testBase);
}
