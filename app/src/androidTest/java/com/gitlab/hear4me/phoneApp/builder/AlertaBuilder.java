package com.gitlab.hear4me.phoneApp.builder;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.model.domain.Categoria;
import com.gitlab.hear4me.compartido.model.domain.Prediccion;

import java.sql.Timestamp;

public class AlertaBuilder {

    private Alerta alerta;

    public AlertaBuilder() {
        alerta = new Alerta();
        alerta.setVista(false);
        alerta.setPrediccion(new Prediccion(Categoria.ALARMA_AUTO, 0.5f));
        alerta.setTiempo(new Timestamp(0));
    }

    public AlertaBuilder conTiempo(long tiempo) {
        alerta.setTiempo(new Timestamp(tiempo));
        return this;
    }

    public AlertaBuilder conVista(boolean vista) {
        alerta.setVista(vista);
        return this;
    }

    public Alerta build() {
        return alerta;
    }

}
