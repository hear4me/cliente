package com.gitlab.hear4me.phoneApp;

import android.content.Context;

import com.gitlab.hear4me.compartido.model.repository.AlertaRepository;
import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.phoneApp.model.db.AppDatabase;

import org.junit.After;

import javax.inject.Inject;

public class TestBase {

    @Inject
    protected Context context;

    @Inject
    protected AppDatabase database;

    @Inject
    protected AlertaService alertaService;

    @Inject
    protected AlertaRepository alertaRepository;

    public TestBase() {
        TestApp.getComponent().inject(this);
    }

    @After
    public void resetDatabase() {
        database.clearAllTables();
    }
}
