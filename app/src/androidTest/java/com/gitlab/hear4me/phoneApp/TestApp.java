package com.gitlab.hear4me.phoneApp;

import android.app.Application;

import com.gitlab.hear4me.phoneApp.di.component.DaggerTestAppComponent;
import com.gitlab.hear4me.phoneApp.di.component.TestAppComponent;
import com.gitlab.hear4me.phoneApp.di.module.AppModule;
import com.gitlab.hear4me.phoneApp.di.module.FileModule;
import com.gitlab.hear4me.phoneApp.di.module.ModelLoaderModule;
import com.gitlab.hear4me.phoneApp.di.module.PhonePresentersModule;
import com.gitlab.hear4me.phoneApp.di.module.PhoneRepositoriesModule;
import com.gitlab.hear4me.phoneApp.di.module.PhoneServicesModule;

public class TestApp extends Application {

    private static TestAppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = buildAppComponent();
    }

    public static TestAppComponent getComponent() {
        return component;
    }

    private TestAppComponent buildAppComponent() {
        return DaggerTestAppComponent.builder()
                .appModule(new AppModule(this))
                .fileModule(new FileModule())
                .modelLoaderModule(new ModelLoaderModule())
                .phonePresentersModule(new PhonePresentersModule())
                .phoneRepositoriesModule(new PhoneRepositoriesModule())
                .phoneServicesModule(new PhoneServicesModule())
                .build();
    }
}
