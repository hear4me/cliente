package com.gitlab.hear4me.phoneApp.di.module;

import android.content.Context;

import androidx.room.Room;

import com.gitlab.hear4me.phoneApp.model.db.AppDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    @Provides
    @Singleton
    public AppDatabase database(Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, "Hear4MeDatabase")
                .allowMainThreadQueries().build();
    }
}
