package com.gitlab.hear4me.phoneApp.model.repository.impl;

import com.gitlab.hear4me.compartido.android.WearableServiceAndroid;
import com.gitlab.hear4me.compartido.config.MensajesWearable;
import com.gitlab.hear4me.compartido.model.domain.Configuraciones;
import com.gitlab.hear4me.compartido.model.repository.ConfiguracionesRepository;
import com.gitlab.hear4me.compartido.util.FileUtil;
import com.gitlab.hear4me.compartido.util.JsonUtil;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

public class ConfiguracionesRepositoryImpl implements ConfiguracionesRepository {

    private WearableServiceAndroid wearableServiceAndroid;
    private File archivoConfiguracion;
    private Configuraciones configuracionesCache;

    @Inject
    public ConfiguracionesRepositoryImpl(
            File archivoConfiguracion,
            WearableServiceAndroid wearableServiceAndroid) {
        this.wearableServiceAndroid = wearableServiceAndroid;
        this.archivoConfiguracion = archivoConfiguracion;
    }

    public Configuraciones cargar() {
        try {
            if (existeArchivoConfiguracion()) {
                String input = FileUtil.leerAString(archivoConfiguracion);
                configuracionesCache = JsonUtil.fromJson(input, Configuraciones.class);
            } else {
                archivoConfiguracion.createNewFile();
                configuracionesCache = new Configuraciones();
                guardar(configuracionesCache);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return configuracionesCache;
    }

    public void guardar(Configuraciones configuraciones) {
        configuracionesCache = configuraciones;
        try {
            String output = JsonUtil.toJson(configuraciones);
            FileUtil.guardar(archivoConfiguracion, output);
        } catch (IOException e) {
            e.printStackTrace();
        }
        sincronizar();
    }

    private void sincronizar() {
        String configuracionesJson = JsonUtil.toJson(configuracionesCache);
        wearableServiceAndroid.enviarMensaje(MensajesWearable.PHONE_WRITE_CONFIG_PATH, configuracionesJson.getBytes());
    }

    private boolean existeArchivoConfiguracion() {
        return archivoConfiguracion.exists() && archivoConfiguracion.length() != 0;
    }

}
