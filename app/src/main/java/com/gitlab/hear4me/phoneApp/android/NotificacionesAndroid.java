package com.gitlab.hear4me.phoneApp.android;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.gitlab.hear4me.phoneApp.R;
import com.gitlab.hear4me.phoneApp.presentation.view.impl.MainActivity;

public class NotificacionesAndroid {

    private static final String CHANNEL_ID = "ForegroundServiceChannel";

    public NotificacionesAndroid() {}

    public Notification crearNotificacion(Context context) {
        Intent notificacionIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,
                0, notificacionIntent, 0);

        Notification notificacion = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setContentTitle("Hear4Me")
                .setContentText("Detectando peligros")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentIntent(pendingIntent)
                .build();

        return notificacion;
    }

    public void crearCanalNotificaciones(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = context.getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
}
