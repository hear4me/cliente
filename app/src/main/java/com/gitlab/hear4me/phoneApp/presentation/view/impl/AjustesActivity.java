package com.gitlab.hear4me.phoneApp.presentation.view.impl;

import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.TextViewCompat;

import com.gitlab.hear4me.compartido.model.domain.Categoria;
import com.gitlab.hear4me.compartido.model.domain.Configuracion;
import com.gitlab.hear4me.compartido.model.domain.Configuraciones;
import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.model.domain.ModoNotificacion;
import com.gitlab.hear4me.phoneApp.PhoneApp;
import com.gitlab.hear4me.phoneApp.R;
import com.gitlab.hear4me.phoneApp.presentation.presenter.AjustesPresenter;
import com.gitlab.hear4me.phoneApp.presentation.view.AjustesView;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

public class AjustesActivity extends AppCompatActivity implements AjustesView {

    @Inject
    AjustesPresenter ajustesPresenter;

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm");

    private boolean modosAutomaticosVisible = false;
    private boolean modosDispositivosVisible = false;
    private boolean modosNotificacionVisible = false;
    private boolean modosCategoriasVisible = false;
    private List<CheckBox> checkBoxesCategoriasCasa = new ArrayList<>();
    private List<CheckBox> checkBoxesCategoriasCalle = new ArrayList<>();
    private List<CheckBox> checkBoxesModosNotificacion = new ArrayList<>();
    private Button btnModoCasa;
    private Button btnModoCalle;
    private Button btnModoDormir;
    private LinearLayoutCompat subContainerModoCasaAuto;
    private LinearLayoutCompat subContainerModoDormirAuto;
    private LinearLayoutCompat categoriasModoCasa;
    private LinearLayoutCompat categoriasModoCalle;
    private RelativeLayout tabAuto;
    private RelativeLayout tabDispositivo;
    private RelativeLayout tabModoNotificacion;
    private RelativeLayout tabCategorias;
    private Button btnMostrarAuto;
    private Button btnMostrarDispositivos;
    private Button btnMostrarModosNotificacion;
    private Button btnMostrarCategorias;
    private Button btnAjustesBack;
    private Button btnAjustesMas;
    private Button btnDeshacerCambios;
    private Button btnRestablecerConfiguracion;
    private EditText txtDesde;
    private EditText txtHasta;
    private RadioButton radCelular;
    private RadioButton radReloj;
    private ScrollView listaAuto;
    private ScrollView listaDispositivos;
    private ScrollView listaModosNotificacion;
    private ScrollView listaCategorias;
    private ViewGroup listaModosNotificacionGroup;
    private ConstraintLayout fondoFalso;
    private TextView txtNombreWifi;
    private Switch switchActivarAutoParaRed;
    private Switch switchModoCasa;
    private Switch switchModoDormir;
    private BroadcastReceiver wifiReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajustes);
        PhoneApp.getComponent().inject(this);

        btnModoCasa = findViewById(R.id.Bcasa);
        btnModoCalle = findViewById(R.id.Bcalle);
        btnModoDormir = findViewById(R.id.Bdormir);

        tabAuto = findViewById(R.id.tabAuto);
        tabDispositivo = findViewById(R.id.tabDispositivo);
        tabModoNotificacion = findViewById(R.id.tabModoNotificacion);
        tabCategorias = findViewById(R.id.tabCategorias);

        btnMostrarAuto = findViewById(R.id.btnMostrarAuto);
        btnMostrarDispositivos = findViewById(R.id.btnMostrarDispositivos);
        btnMostrarModosNotificacion = findViewById(R.id.btnMostrarModosNotificacion);
        btnMostrarCategorias = findViewById(R.id.btnMostrarCategorias);
        btnAjustesBack = findViewById(R.id.btnAjustesBack);
        btnAjustesMas = findViewById(R.id.btnAjustesMas);
        btnRestablecerConfiguracion = findViewById(R.id.btnRestablecerConfiguracion);
        btnDeshacerCambios = findViewById(R.id.btnDeshacerCambios);

        txtDesde = findViewById(R.id.txtDesde);
        txtHasta = findViewById(R.id.txtHasta);

        switchModoCasa = findViewById(R.id.switchModoCasa);
        switchActivarAutoParaRed = findViewById(R.id.switchActivarAutoParaRed);
        switchModoDormir = findViewById(R.id.switchModoDormir);

        fondoFalso = findViewById(R.id.AjustesFondoFalso);

        subContainerModoCasaAuto = findViewById(R.id.subContainerModoCasaAuto);
        subContainerModoDormirAuto = findViewById(R.id.subContainerModoDormirAuto);

        listaAuto = findViewById(R.id.listaAuto);
        listaDispositivos = findViewById(R.id.listaDispositivos);
        listaModosNotificacion = findViewById(R.id.listaModosNotificacion);
        listaCategorias = findViewById(R.id.listaCategorias);

        tabAuto.setOnClickListener(this::mostrarOpcionesModosAutomaticos);
        btnMostrarAuto.setOnClickListener(this::mostrarOpcionesModosAutomaticos);
        tabDispositivo.setOnClickListener(this::mostrarOpcionesDispositivos);
        btnMostrarDispositivos.setOnClickListener(this::mostrarOpcionesDispositivos);
        tabModoNotificacion.setOnClickListener(this::mostrarOpcionesModosNotificacion);
        btnMostrarModosNotificacion.setOnClickListener(this::mostrarOpcionesModosNotificacion);
        tabCategorias.setOnClickListener(this::mostrarOpcionesCategorias);
        btnMostrarCategorias.setOnClickListener(this::mostrarOpcionesCategorias);

        btnAjustesBack.setOnClickListener(this::irAAtras);
        btnAjustesMas.setOnClickListener(this::mostrarMenu);
        btnRestablecerConfiguracion.setOnClickListener(this::restablecerConfiguracion);
        btnDeshacerCambios.setOnClickListener(this::deshacerCambios);
        fondoFalso.setOnClickListener(this::clickFondoFalso);
        btnModoCasa.setOnClickListener(this::clickModoCasa);
        btnModoCalle.setOnClickListener(this::clickModoCalle);
        btnModoDormir.setOnClickListener(this::clickModoDormir);
        txtDesde.setOnClickListener(this::clickDesde);
        txtHasta.setOnClickListener(this::clickHasta);

        switchModoCasa.setOnClickListener(this::clickSwitchModoCasaAuto);
        switchActivarAutoParaRed.setOnClickListener(this::clickSwitchActivarAutoParaRed);
        switchModoDormir.setOnClickListener(this::clickSwitchModoDormirAuto);

        radCelular = findViewById(R.id.radCelular);
        radCelular.setOnClickListener(this::activarCelular);

        radReloj = findViewById(R.id.radReloj);
        radReloj.setOnClickListener(this::activarReloj);

        txtNombreWifi = findViewById(R.id.txtNombreWifi);

        listaModosNotificacionGroup = (ViewGroup) listaModosNotificacion.getChildAt(0);
        categoriasModoCasa = findViewById(R.id.categoriasModoCasa);
        categoriasModoCalle = findViewById(R.id.categoriasModoCalle);

        wifiReceiver = new WifiReceiver();

        cargarCheckboxesUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        registerReceiver(wifiReceiver, intentFilter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ajustesPresenter.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ajustesPresenter.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(wifiReceiver);
    }

    @Override
    public void mostrarMenu(View view) {
        fondoFalso.setVisibility(View.VISIBLE);
    }

    @Override
    public void clickFondoFalso(View view) {
        fondoFalso.setVisibility(View.GONE);
    }

    @Override
    public Configuracion leerCambiosConfiguracion(Configuracion configuracion) {
        ModoConfiguracion modoConfiguracion = configuracion.getModoConfiguracion();
        Categoria[] categorias;
        List<CheckBox> listaChecks;
        if (modoConfiguracion == ModoConfiguracion.CALLE) {
            categorias = Categoria.deModoCalle();
            listaChecks = checkBoxesCategoriasCalle;
        } else {
            categorias = Categoria.deModoCasa();
            listaChecks = checkBoxesCategoriasCasa;
        }
        for (int i = 0; i < categorias.length; ++i) {
            boolean checked = listaChecks.get(i).isChecked();
            configuracion.getCategorias().put(categorias[i], checked);
        }

        ModoNotificacion[] modosNotificacion = ModoNotificacion.values();
        for (int i = 0; i < modosNotificacion.length; ++i) {
            boolean checked = checkBoxesModosNotificacion.get(i).isChecked();
            configuracion.getModosNotificacion().put(modosNotificacion[i], checked);
        }

        return configuracion;
    }

    @Override
    public void actualizarCambiosConfiguracion(Configuraciones configuraciones, ModoConfiguracion modoConfiguracion, String[] infoWifi) {
        Configuracion configuracion = configuraciones.getConfiguracion(modoConfiguracion);
        ModoNotificacion[] modosNotificacion = ModoNotificacion.values();

        String idRedActual = infoWifi[0];
        String nombreRedActual = infoWifi[1];

        boolean modoCasaAutomaticoConfigurado = configuraciones.isModoCasaAutomaticoConfigurado();
        switchModoCasa.setChecked(modoCasaAutomaticoConfigurado);
        subContainerModoCasaAuto.setVisibility(modoCasaAutomaticoConfigurado
                ? View.VISIBLE
                : View.GONE);
        if (idRedActual != null && !configuraciones.isModoDormirAutomaticoActivado()) {
            switchActivarAutoParaRed.setEnabled(true);
        } else {
            switchActivarAutoParaRed.setEnabled(false);
        }
        if (idRedActual == null) {
            switchActivarAutoParaRed.setChecked(false);
            txtNombreWifi.setText(getString(R.string.red_desconectada));
        } else {
            configuraciones.getRedesModoCasa().stream()
                    .filter((red) -> red.getId().equals(idRedActual))
                    .findFirst()
                    .ifPresent((red) -> switchActivarAutoParaRed.setChecked(true));
            txtNombreWifi.setText(nombreRedActual);
        }

        switchModoDormir.setChecked(configuraciones.isModoDormirAutomaticoConfigurado());
        subContainerModoDormirAuto.setVisibility(configuraciones.isModoDormirAutomaticoConfigurado()
                ? View.VISIBLE
                : View.GONE);

        switch (configuraciones.getDispositivo()) {
            case CELULAR:
                activarRadioCelular();
                break;
            case RELOJ:
                activarRadioReloj();
                break;
        }

        Categoria[] categorias;
        List<CheckBox> listaChecks;
        if (modoConfiguracion == ModoConfiguracion.CALLE) {
            categorias = Categoria.deModoCalle();
            listaChecks = checkBoxesCategoriasCalle;
        } else {
            categorias = Categoria.deModoCasa();
            listaChecks = checkBoxesCategoriasCasa;
        }
        for (int i = 0; i < categorias.length; ++i) {
            listaChecks.get(i).setChecked(
                    configuracion.getCategorias().get(categorias[i]));
        }

        for (int i = 0; i < modosNotificacion.length; ++i) {
            checkBoxesModosNotificacion.get(i).setChecked(
                    configuracion.getModosNotificacion().get(modosNotificacion[i]));
        }
    }

    @Override
    public void activarModoCasa() {
        modoCasaOn();
        modoCalleOff();
    }

    @Override
    public void activarModoCalle() {
        modoCalleOn();
        modoCasaOff();
    }

    @Override
    public void activarModoDormir() {
        modoDormirOn();
    }

    @Override
    public void desactivarModoDormir() {
        modoDormirOff();
    }

    @Override
    public void mostrarNombreWifi(String nombre) {
        txtNombreWifi.setText(nombre);
    }

    @Override
    public void mostrarSubContainerModoCasaAuto(boolean estado) {
        if (estado) {
            subContainerModoCasaAuto.setVisibility(View.VISIBLE);
        } else {
            subContainerModoCasaAuto.setVisibility(View.GONE);
        }
    }

    @Override
    public void mostrarSubContainerModoDormirAuto(boolean estado) {
        if (estado) {
            subContainerModoDormirAuto.setVisibility(View.VISIBLE);
        } else {
            subContainerModoDormirAuto.setVisibility(View.GONE);
        }
    }

    @Override
    public void mostrarListaCategoriasModoCasa() {
        categoriasModoCasa.setVisibility(View.VISIBLE);
        categoriasModoCalle.setVisibility(View.GONE);
    }

    @Override
    public void mostrarListaCategoriasModoCalle() {
        categoriasModoCalle.setVisibility(View.VISIBLE);
        categoriasModoCasa.setVisibility(View.GONE);
    }

    @Override
    public void mostrarHoraDesde(long tiempo) {
        String horaConFormato = DATE_FORMAT.format(new Date(tiempo));
        txtDesde.setText(horaConFormato);
    }

    @Override
    public void mostrarHoraHasta(long tiempo) {
        String horaConFormato = DATE_FORMAT.format(new Date(tiempo));
        txtHasta.setText(horaConFormato);
    }

    @Override
    public void modoCasaAutomaticoHabilitado() {
        switchActivarAutoParaRed.setEnabled(true);
    }

    @Override
    public void modoCasaAutomaticoDeshabilitado() {
        switchActivarAutoParaRed.setEnabled(false);
    }

    @Override
    public void irAAtras(View view) {
        ajustesPresenter.irAAtras(view);
        finish();
    }

    @Override
    public void deshacerCambios(View view) {
        ajustesPresenter.deshacerCambios(view);
        fondoFalso.setVisibility(View.GONE);
    }

    @Override
    public void restablecerConfiguracion(View view) {
        ajustesPresenter.restablecerConfiguracion(view);
        fondoFalso.setVisibility(View.GONE);
    }

    @Override
    public void mostrarOpcionesModosAutomaticos(View view) {
        ajustesPresenter.mostrarOpcionesModosAutomaticos(view);
        if (modosAutomaticosVisible) {
            ocultarModosAutomaticosUI();
        } else {
            ocultarDispositivosUI();
            ocultarCategoriasUI();
            ocultarModosNotificacionUI();
            mostrarModosAutomaticosUI();
        }
    }

    @Override
    public void mostrarOpcionesDispositivos(View view) {
        if (modosDispositivosVisible) {
            ocultarDispositivosUI();
        } else {
            ocultarModosAutomaticosUI();
            ocultarCategoriasUI();
            ocultarModosNotificacionUI();
            mostrarDispositivosUI();
        }
    }

    @Override
    public void mostrarOpcionesCategorias(View view) {
        ajustesPresenter.mostrarOpcionesCategorias(view);
        if (modosCategoriasVisible) {
            ocultarCategoriasUI();
        } else {
            ocultarModosAutomaticosUI();
            ocultarDispositivosUI();
            ocultarModosNotificacionUI();
            mostrarCategoriasUI();
        }
    }

    @Override
    public void mostrarOpcionesModosNotificacion(View view) {
        if (modosNotificacionVisible) {
            ocultarModosNotificacionUI();
        } else {
            ocultarModosAutomaticosUI();
            ocultarDispositivosUI();
            ocultarCategoriasUI();
            mostrarModosNotificacionUI();
        }
    }

    @Override
    public void activarCelular(View view) {
        ajustesPresenter.activarCelular(view);
        activarRadioCelular();
    }

    @Override
    public void activarReloj(View view) {
        ajustesPresenter.activarReloj(view);
        activarRadioReloj();
    }

    @Override
    public void clickDesde(View view) {
        obtenerHora(true);
    }

    @Override
    public void clickHasta(View view) {
        obtenerHora(false);
        ajustesPresenter.clickHasta(view);
    }

    @Override
    public void clickModoCasa(View view) {
        ajustesPresenter.clickModoCasa(view);
    }

    @Override
    public void clickModoCalle(View view) {
        ajustesPresenter.clickModoCalle(view);
    }

    @Override
    public void clickModoDormir(View view) {
        ajustesPresenter.clickModoDormir(view);
    }

    @Override
    public void clickSwitchModoCasaAuto(View view) {
        ajustesPresenter.clickSwitchModoCasaAuto(view);
    }

    @Override
    public void clickSwitchModoDormirAuto(View view) {
        ajustesPresenter.clickSwitchModoDormirAuto(view);
    }

    @Override
    public void clickSwitchActivarAutoParaRed(View view) {
        ajustesPresenter.clickSwitchActivarAutoParaRed(view);
    }

    private void obtenerHora(boolean desde){
        TimePickerDialog recogerHora = new TimePickerDialog(this, (view, hourOfDay, minute) -> {
            String horaConFormato = formatearHora(hourOfDay, minute);
            if (desde) {
                ajustesPresenter.horaDesdeSeteada(calcularTiempo(hourOfDay, minute));
                txtDesde.setText(horaConFormato);
            } else {
                ajustesPresenter.horaHastaSeteada(calcularTiempo(hourOfDay, minute));
                txtHasta.setText(horaConFormato);
            }
        }, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), true);
        recogerHora.show();
    }

    private long calcularTiempo(int hora, int minuto) {
        Calendar calendario = Calendar.getInstance();
        return calendario.getTimeInMillis()
                - calendario.get(Calendar.HOUR_OF_DAY) * 60000 * 60
                - calendario.get(Calendar.MINUTE) * 60000
                - calendario.get(Calendar.SECOND) * 1000
                + hora * 60000 * 60
                + minuto * 60000;
    }

    private String formatearHora(int hourOfDay, int minute) {
        String formatoDosNumerosHora = StringUtils.right("0" + hourOfDay, 2);
        String formatoDosNumerosMinuto = StringUtils.right("0" + minute, 2);
        return String.format("%s:%s", formatoDosNumerosHora, formatoDosNumerosMinuto);
    }

    private void modoCasaOn() {
        btnModoCasa.setBackgroundResource(R.drawable.selector_boton_izquierdo_activo);
        TextViewCompat.setTextAppearance(btnModoCasa, R.style.BotonIcono_SeleccionadoIzquierdo);
    }

    private void modoCasaOff() {
        btnModoCasa.setBackgroundResource(R.drawable.fondo_selector);
        TextViewCompat.setTextAppearance(btnModoCasa, R.style.BotonIcono);
    }

    private void modoCalleOn() {
        btnModoCalle.setBackgroundResource(R.drawable.boton_circulo_activo);
        TextViewCompat.setTextAppearance(btnModoCalle, R.style.BotonIcono_SeleccionadoSeparado);
    }

    private void modoCalleOff() {
        btnModoCalle.setBackgroundResource(R.drawable.fondo_selector);
        TextViewCompat.setTextAppearance(btnModoCalle, R.style.BotonIcono);
    }

    private void modoDormirOn() {
        btnModoDormir.setBackgroundResource(R.drawable.selector_boton_derecho_activo);
        TextViewCompat.setTextAppearance(btnModoDormir, R.style.BotonIcono_SeleccionadoDerecho);
    }

    private void modoDormirOff() {
        btnModoDormir.setBackgroundResource(R.drawable.fondo_selector);
        TextViewCompat.setTextAppearance(btnModoDormir, R.style.BotonIcono);
    }

    private void mostrarModosAutomaticosUI() {
        btnMostrarAuto.setText(R.string.icon_caret_up);
        listaAuto.setVisibility(View.VISIBLE);
        modosAutomaticosVisible = true;
    }

    private void ocultarModosAutomaticosUI() {
        btnMostrarAuto.setText(R.string.icon_caret_down);
        listaAuto.setVisibility(View.GONE);
        modosAutomaticosVisible = false;

    }

    private void mostrarDispositivosUI() {
        btnMostrarDispositivos.setText(R.string.icon_caret_up);
        listaDispositivos.setVisibility(View.VISIBLE);
        modosDispositivosVisible = true;
    }

    private void ocultarDispositivosUI() {
        btnMostrarDispositivos.setText(R.string.icon_caret_down);
        listaDispositivos.setVisibility(View.GONE);
        modosDispositivosVisible = false;
    }

    private void mostrarModosNotificacionUI() {
        btnMostrarModosNotificacion.setText(R.string.icon_caret_up);
        listaModosNotificacion.setVisibility(View.VISIBLE);
        modosNotificacionVisible = true;
    }

    private void ocultarModosNotificacionUI() {
        btnMostrarModosNotificacion.setText(R.string.icon_caret_down);
        listaModosNotificacion.setVisibility(View.GONE);
        modosNotificacionVisible = false;
    }

    private void mostrarCategoriasUI() {
        btnMostrarCategorias.setText(R.string.icon_caret_up);
        listaCategorias.setVisibility(View.VISIBLE);
        modosCategoriasVisible = true;
    }

    private void ocultarCategoriasUI() {
        btnMostrarCategorias.setText(R.string.icon_caret_down);
        listaCategorias.setVisibility(View.GONE);
        modosCategoriasVisible = false;
    }

    private void cargarCheckboxesUI() {
        for (int i = 0; i < categoriasModoCasa.getChildCount(); ++i) {
            ViewGroup n1 = (ViewGroup) categoriasModoCasa.getChildAt(i);
            ViewGroup n2 = (ViewGroup) n1.getChildAt(0);
            CheckBox chk = (CheckBox) n2.getChildAt(1);
            checkBoxesCategoriasCasa.add(chk);
        }
        for (int i = 0; i < categoriasModoCalle.getChildCount(); ++i) {
            ViewGroup n1 = (ViewGroup) categoriasModoCalle.getChildAt(i);
            ViewGroup n2 = (ViewGroup) n1.getChildAt(0);
            CheckBox chk = (CheckBox) n2.getChildAt(1);
            checkBoxesCategoriasCalle.add(chk);
        }
        for (int i = 0; i < listaModosNotificacionGroup.getChildCount(); ++i) {
            ViewGroup n1 = (ViewGroup) listaModosNotificacionGroup.getChildAt(i);
            ViewGroup n2 = (ViewGroup) n1.getChildAt(0);
            CheckBox chk = (CheckBox) n2.getChildAt(1);
            checkBoxesModosNotificacion.add(chk);
        }
    }

    private void activarRadioCelular() {
        radCelular.setChecked(true);
        radReloj.setChecked(false);
    }

    private void activarRadioReloj() {
        radReloj.setChecked(true);
        radCelular.setChecked(false);
    }

    public class WifiReceiver extends BroadcastReceiver {

        private boolean conectado;

        public WifiReceiver() {
            conectado = isRedConectada();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean redConectada = isRedConectada();
            if (redConectada != conectado) {
                ajustesPresenter.cambioEnLaRed();
                conectado = redConectada;
            }
        }

        private boolean isRedConectada() {
            WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
            if (wifiManager.isWifiEnabled()) {
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                if( wifiInfo.getNetworkId() == -1 ){
                    return false;
                }
                return true;
            }
            else {
                return false;
            }
        }
    }

}
