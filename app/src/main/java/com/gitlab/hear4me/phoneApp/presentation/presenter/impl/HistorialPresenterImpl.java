package com.gitlab.hear4me.phoneApp.presentation.presenter.impl;

import android.view.View;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.compartido.presentation.view.ActivityView;
import com.gitlab.hear4me.phoneApp.presentation.presenter.HistorialPresenter;
import com.gitlab.hear4me.phoneApp.presentation.view.HistorialView;

import java.util.List;

import javax.inject.Inject;

public class HistorialPresenterImpl implements HistorialPresenter {

    private HistorialView historialView;
    private AlertaService alertaService;

    @Inject
    public HistorialPresenterImpl(AlertaService alertaService) {
        this.alertaService = alertaService;
    }

    @Override
    public void onCreate(ActivityView activityView) {

    }

    @Override
    public void onStart(ActivityView activityView) {

    }

    @Override
    public void onResume(ActivityView activityView) {
        historialView = (HistorialView) activityView;
        List<Alerta> historialAlertas = alertaService.buscarHistorial();
        historialView.mostrarHistorial(historialAlertas);
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void irAAtras(View view) {

    }

    @Override
    public void eliminarHistorial(View view) {
        alertaService.borrarHistorial();
    }
}
