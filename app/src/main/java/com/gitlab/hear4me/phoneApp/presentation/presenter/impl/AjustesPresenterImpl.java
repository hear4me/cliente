package com.gitlab.hear4me.phoneApp.presentation.presenter.impl;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.view.View;

import com.gitlab.hear4me.compartido.model.domain.Configuracion;
import com.gitlab.hear4me.compartido.model.domain.Configuraciones;
import com.gitlab.hear4me.compartido.model.domain.Dispositivo;
import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.compartido.presentation.view.ActivityView;
import com.gitlab.hear4me.phoneApp.PhoneApp;
import com.gitlab.hear4me.phoneApp.android.ConfiguracionesServiceAndroid;
import com.gitlab.hear4me.phoneApp.android.PhoneIntentFacadeAndroid;
import com.gitlab.hear4me.phoneApp.presentation.presenter.AjustesPresenter;
import com.gitlab.hear4me.phoneApp.presentation.view.AjustesView;

import javax.inject.Inject;

public class AjustesPresenterImpl implements AjustesPresenter {

    private AjustesView ajustesView;
    private ConfiguracionesService configuracionesService;
    private ConfiguracionesServiceAndroid configuracionesServiceAndroid;
    private PhoneIntentFacadeAndroid phoneIntentFacadeAndroid;

    @Inject
    public AjustesPresenterImpl(ConfiguracionesService configuracionesService,
                                ConfiguracionesServiceAndroid configuracionesServiceAndroid,
                                PhoneIntentFacadeAndroid phoneIntentFacadeAndroid) {
        this.configuracionesService = configuracionesService;
        this.configuracionesServiceAndroid = configuracionesServiceAndroid;
        this.phoneIntentFacadeAndroid = phoneIntentFacadeAndroid;
    }

    @Override
    public void onCreate(ActivityView activityView) {

    }

    @Override
    public void onStart(ActivityView activityView) {

    }

    @Override
    public void onResume(ActivityView activityView) {
        ajustesView = (AjustesView) activityView;
        cargarConfiguracion();
        activarModoCorrespondiente();
        PhoneApp.uiAjustesIniciada = true;
        ajustesView.mostrarHoraDesde(configuracionesService.obtenerHoraDesde());
        ajustesView.mostrarHoraHasta(configuracionesService.obtenerHoraHasta());
    }

    @Override
    public void onPause() {
        guardarConfiguracion();
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void irAAtras(View view) {
        onPause();
    }

    @Override
    public void deshacerCambios(View view) {
        Configuraciones configuraciones = configuracionesService.obtenerConfiguraciones();
        ModoConfiguracion modoConfiguracion = configuraciones.getModoConfiguracionSeleccionado();
        WifiInfo wifiInfo = buscarInfoWifi();

        this.ajustesView.actualizarCambiosConfiguracion(
                configuraciones,
                modoConfiguracion,
                new String[]{wifiInfo.getBSSID(), wifiInfo.getSSID()});
    }

    @Override
    public void restablecerConfiguracion(View view) {
        ModoConfiguracion modoConfiguracion = configuracionesService.obtenerModoConfiguracionActivo();
        configuracionesService.restablecerConfiguraciones();
        Configuraciones configuraciones = configuracionesService.obtenerConfiguraciones();
        WifiInfo wifiInfo = buscarInfoWifi();

        this.ajustesView.actualizarCambiosConfiguracion(
                configuraciones,
                modoConfiguracion,
                new String[]{wifiInfo.getBSSID(), wifiInfo.getSSID()});

        activarModoCorrespondiente();
    }

    @Override
    public void mostrarOpcionesModosAutomaticos(View view) {
        Configuraciones configuraciones = configuracionesService.obtenerConfiguraciones();
        ModoConfiguracion modoConfiguracion = configuraciones.getModoConfiguracionSeleccionado();
        WifiInfo wifiInfo = buscarInfoWifi();
        actualizarConfiguracion(configuraciones, modoConfiguracion, wifiInfo);
    }

    @Override
    public void mostrarOpcionesDispositivos(View view) {

    }

    @Override
    public void mostrarOpcionesCategorias(View view) {
        ModoConfiguracion modoConfiguracion =
                configuracionesService.obtenerConfiguraciones().getModoConfiguracionSeleccionado();
        mostrarListaCategoriasCorrespondiente(modoConfiguracion);
    }

    @Override
    public void mostrarOpcionesModosNotificacion(View view) {

    }

    @Override
    public void activarCelular(View view) {
        configuracionesService.setDispositivo(Dispositivo.CELULAR);
    }

    @Override
    public void activarReloj(View view) {
        configuracionesService.setDispositivo(Dispositivo.RELOJ);
    }

    @Override
    public void clickModoCasa(View view) {
        if (configuracionesService.isModoCasaAutomaticoActivado()) return;

        ModoConfiguracion modoConfiguracionActual
                = configuracionesService.obtenerModoConfiguracionActivo();

        if (modoConfiguracionActual == ModoConfiguracion.CASA_DORMIR) {
            this.ajustesView.desactivarModoDormir();
        }

        ModoConfiguracion modoConfiguracionSiguiente = ModoConfiguracion.CASA;
        actualizarConfiguracion(modoConfiguracionSiguiente);

        this.ajustesView.activarModoCasa();
        mostrarListaCategoriasCorrespondiente(modoConfiguracionSiguiente);
    }

    @Override
    public void clickModoCalle(View view) {
        if (configuracionesService.isModoCasaAutomaticoActivado()) return;
        ModoConfiguracion modoConfiguracionSiguiente = ModoConfiguracion.CALLE;
        actualizarConfiguracion(modoConfiguracionSiguiente);
        this.ajustesView.desactivarModoDormir();
        this.ajustesView.activarModoCalle();
        mostrarListaCategoriasCorrespondiente(modoConfiguracionSiguiente);
    }

    @Override
    public void clickModoDormir(View view) {
        if (configuracionesService.isModoCasaAutomaticoActivado()) return;
        ModoConfiguracion modoConfiguracionActual
                = configuracionesService.obtenerModoConfiguracionActivo();

        ModoConfiguracion modoConfiguracionSiguiente = null;

        switch (modoConfiguracionActual) {
            case CASA:
            case CALLE:
            case CALLE_DORMIR:
                modoConfiguracionSiguiente = ModoConfiguracion.CASA_DORMIR;
                break;
            case CASA_DORMIR:
                modoConfiguracionSiguiente = ModoConfiguracion.CASA;
                break;
        }

        if (modoConfiguracionSiguiente == null) {
            throw new IllegalStateException("Modo de configuración actual inválido");
        }

        actualizarConfiguracion(modoConfiguracionSiguiente);

        if (modoConfiguracionSiguiente == ModoConfiguracion.CASA_DORMIR) {
            this.ajustesView.activarModoCasa();
            this.ajustesView.activarModoDormir();
        } else {
            this.ajustesView.desactivarModoDormir();
        }
        mostrarListaCategoriasCorrespondiente(modoConfiguracionSiguiente);
    }

    @Override
    public void clickSwitchModoCasaAuto(View view) {
        WifiInfo wifiInfo = buscarInfoWifi();
        if (configuracionesService.isModoCasaAutomaticoConfigurado()) {
            configuracionesService.setModoCasaAutomaticoConfigurado(false);
            configuracionesService.setModoCasaAutomaticoActivado(false);
            ajustesView.mostrarSubContainerModoCasaAuto(false);
        } else {
            configuracionesService.setModoCasaAutomaticoConfigurado(true);
            configuracionesService.setModoCasaAutomaticoActivado(true);
            ajustesView.mostrarSubContainerModoCasaAuto(true);
            ajustesView.mostrarNombreWifi(wifiInfo.getSSID());
            configuracionesServiceAndroid.determinarModoAutomaticamente();
        }
        ModoConfiguracion modoConfiguracion = configuracionesService.obtenerModoConfiguracionActivo();
        actualizarConfiguracion(configuracionesService.obtenerConfiguraciones(), modoConfiguracion, wifiInfo);
    }

    @Override
    public void clickSwitchModoDormirAuto(View view) {
        if (configuracionesService.isModoDormirAutomaticoConfigurado()) {
            configuracionesService.setModoDormirAutomaticoConfigurado(false);
            ajustesView.mostrarSubContainerModoDormirAuto(false);
            phoneIntentFacadeAndroid.cancelarAlarmaCasa((Context) ajustesView);
            phoneIntentFacadeAndroid.cancelarAlarmaDormir((Context) ajustesView);
            configuracionesService.setModoDormirAutomaticoActivado(false);
        } else {
            configuracionesService.setModoDormirAutomaticoConfigurado(true);
            ajustesView.mostrarSubContainerModoDormirAuto(true);
            if (configuracionesService.isDentroDeHorarioModoDormirAutomatico()) {
                configuracionesService.setModoDormirAutomaticoActivado(true);
            }
        }
        configuracionesServiceAndroid.determinarModoAutomaticamente();
    }

    @Override
    public void clickSwitchActivarAutoParaRed(View view) {
        WifiInfo wifiInfo = buscarInfoWifi();
        configuracionesService.activarModoAutomaticoParaRed(wifiInfo.getBSSID(), wifiInfo.getSSID());
        configuracionesServiceAndroid.determinarModoAutomaticamente();

        ModoConfiguracion modoConfiguracion = configuracionesService.obtenerModoConfiguracionActivo();
        actualizarConfiguracion(configuracionesService.obtenerConfiguraciones(), modoConfiguracion, wifiInfo);
    }

    @Override
    public void clickDesde(View view) {

    }

    @Override
    public void clickHasta(View view) {

    }

    @Override
    public void activarModoCorrespondiente() {
        ModoConfiguracion modoConfiguracionActual
                = configuracionesService.obtenerModoConfiguracionActivo();

        switch (modoConfiguracionActual) {
            case CASA:
                ajustesView.activarModoCasa();
                ajustesView.desactivarModoDormir();
                break;
            case CALLE:
            case CALLE_DORMIR:
                ajustesView.activarModoCalle();
                ajustesView.desactivarModoDormir();
                break;
            case CASA_DORMIR:
                ajustesView.activarModoCasa();
                ajustesView.activarModoDormir();
                break;
        }
    }

    @Override
    public void actualizarConfiguracion(ModoConfiguracion modoConfiguracionSiguiente) {
        guardarConfiguracion();
        configuracionesService.cambiarModoConfiguracion(modoConfiguracionSiguiente);
        cargarConfiguracion();
    }

    @Override
    public void cambioEnLaRed() {
        Configuraciones configuraciones = configuracionesService.obtenerConfiguraciones();
        ModoConfiguracion modoConfiguracion = configuraciones.getModoConfiguracionSeleccionado();
        WifiInfo wifiInfo = buscarInfoWifi();
        actualizarConfiguracion(configuraciones, modoConfiguracion, wifiInfo);
        configuracionesServiceAndroid.determinarModoAutomaticamente();
    }

    @Override
    public void horaHastaSeteada(long tiempo) {
        configuracionesService.setModoDormirAutomaticoActivado(false);
        phoneIntentFacadeAndroid.configurarAlarmaCasa((Context) ajustesView, tiempo);
        configuracionesService.setHoraHasta(tiempo);
        actualizarCambiosConfiguracionEnLaVista();
    }

    @Override
    public void horaDesdeSeteada(long tiempo) {
        configuracionesService.setModoDormirAutomaticoActivado(false);
        phoneIntentFacadeAndroid.configurarAlarmaDormir((Context) ajustesView, tiempo);
        configuracionesService.setHoraDesde(tiempo);
        actualizarCambiosConfiguracionEnLaVista();
    }

    @Override
    public void habilitarModoCasaAutomatico() {
        ajustesView.modoCasaAutomaticoHabilitado();
    }

    @Override
    public void deshabilitarModoCasaAutomatico() {
        ajustesView.modoCasaAutomaticoDeshabilitado();
    }

    private void actualizarCambiosConfiguracionEnLaVista() {
        Configuraciones configuraciones = configuracionesService.obtenerConfiguraciones();
        ModoConfiguracion modoConfiguracion = configuraciones.getModoConfiguracionSeleccionado();
        WifiInfo infoWifi = buscarInfoWifi();
        String[] infoWifiArr = new String[] {infoWifi.getBSSID(), infoWifi.getSSID()};
        ajustesView.actualizarCambiosConfiguracion(configuraciones, modoConfiguracion, infoWifiArr);
    }

    private void actualizarConfiguracion(Configuraciones configuraciones, ModoConfiguracion modoConfiguracion, WifiInfo wifiInfo) {
        this.ajustesView.actualizarCambiosConfiguracion(
                configuraciones,
                modoConfiguracion,
                new String[]{wifiInfo.getBSSID(), wifiInfo.getSSID()});
    }

    private void cargarConfiguracion() {
        ModoConfiguracion modoConfiguracion = configuracionesService.obtenerModoConfiguracionActivo();
        Configuraciones configuraciones = configuracionesService.obtenerConfiguraciones();
        WifiInfo wifiInfo = buscarInfoWifi();

        this.ajustesView.actualizarCambiosConfiguracion(
                configuraciones,
                modoConfiguracion,
                new String[]{wifiInfo.getBSSID(), wifiInfo.getSSID()});
    }

    private void guardarConfiguracion() {
        ModoConfiguracion modoConfiguracion = configuracionesService.obtenerModoConfiguracionActivo();
        Configuracion configuracion = configuracionesService.obtenerConfiguracion(modoConfiguracion);
        configuracion = ajustesView.leerCambiosConfiguracion(configuracion);
        configuracionesService.actualizarConfiguracion(modoConfiguracion, configuracion);
    }

    private WifiInfo buscarInfoWifi() {
        WifiManager wifiManager =
                (WifiManager) ((Context) ajustesView).getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();

        if (wifiInfo.getBSSID() == null) {
            configuracionesService.setModoCasaAutomaticoActivado(false);
        } else {
            configuracionesService.setModoCasaAutomaticoActivado(
                    configuracionesService.isModoCasaAutomaticoConfigurado());
        }

        return wifiInfo;
    }

    private void mostrarListaCategoriasCorrespondiente(ModoConfiguracion modoConfiguracion) {
        if (modoConfiguracion == ModoConfiguracion.CALLE) {
            ajustesView.mostrarListaCategoriasModoCalle();
        } else {
            ajustesView.mostrarListaCategoriasModoCasa();
        }
    }
}
