package com.gitlab.hear4me.phoneApp.model.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.phoneApp.model.db.converter.AlertaConverter;
import com.gitlab.hear4me.phoneApp.model.repository.PhoneAlertaRepository;

@Database(
        entities = {Alerta.class},
        version = 1)
@TypeConverters({AlertaConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract PhoneAlertaRepository phoneAlertaRepository();
}
