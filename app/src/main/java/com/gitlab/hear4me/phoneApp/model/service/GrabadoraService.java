package com.gitlab.hear4me.phoneApp.model.service;

import android.os.Environment;

import com.gitlab.hear4me.compartido.model.domain.AudioWav;
import com.gitlab.hear4me.compartido.model.domain.Grabadora;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

public class GrabadoraService {

    @Inject
    public GrabadoraService() {}

    public AudioWav grabarAudio(long duracion) {
        new Grabadora().grabar(duracion);
        try {
            AudioWav audio = new AudioWav(obtenerArchivoGrabacionTemporal());
            return audio;
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new RuntimeException("Error inesperado al grabar audio");
    }

    private File obtenerArchivoGrabacionTemporal() {
        return new File(Environment.getExternalStorageDirectory(), Grabadora.ARCHIVO_TEMP);
    }
}
