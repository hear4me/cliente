package com.gitlab.hear4me.phoneApp.model.service;

import com.gitlab.hear4me.compartido.model.domain.AudioWav;
import com.gitlab.hear4me.compartido.model.domain.Categoria;
import com.gitlab.hear4me.compartido.model.domain.ClasificadorAudio;
import com.gitlab.hear4me.compartido.model.domain.ModelLoader;
import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.model.domain.ResultadoClasificacion;

import javax.inject.Inject;

public class ClasificadorAudioService {

    private ModelLoader modelLoaderModoCasa;
    private ModelLoader modelLoaderModoCalle;

    @Inject
    public ClasificadorAudioService(ModelLoader modelLoaderModoCasa, ModelLoader modelLoaderModoCalle) {
        this.modelLoaderModoCasa = modelLoaderModoCasa;
        this.modelLoaderModoCalle = modelLoaderModoCalle;
    }

    public ResultadoClasificacion clasificarAudio(AudioWav audio, ModoConfiguracion modoConfiguracionActivo) {
        ModelLoader modelLoaderParaModoActivo;
        int numeroCategorias;

        if (modoConfiguracionActivo == ModoConfiguracion.CALLE) {
            modelLoaderParaModoActivo = modelLoaderModoCalle;
            numeroCategorias = Categoria.deModoCalle().length;
        } else {
            modelLoaderParaModoActivo = modelLoaderModoCasa;
            numeroCategorias = Categoria.deModoCasa().length;
        }

        ClasificadorAudio clasificadorAudio =
                new ClasificadorAudio(modelLoaderParaModoActivo, modoConfiguracionActivo, numeroCategorias);
        ResultadoClasificacion resultado = clasificadorAudio.clasificar(audio);
        return resultado;
    }

}
