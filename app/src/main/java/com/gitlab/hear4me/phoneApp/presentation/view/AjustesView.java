package com.gitlab.hear4me.phoneApp.presentation.view;

import android.view.View;

import com.gitlab.hear4me.compartido.model.domain.Configuracion;
import com.gitlab.hear4me.compartido.model.domain.Configuraciones;
import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.presentation.view.ActivityView;
import com.gitlab.hear4me.phoneApp.presentation.events.AjustesEvents;

public interface AjustesView extends ActivityView, AjustesEvents {
    void mostrarMenu(View view);
    void clickFondoFalso(View view);
    Configuracion leerCambiosConfiguracion(Configuracion configuracion);
    void actualizarCambiosConfiguracion(Configuraciones configuraciones, ModoConfiguracion modoConfiguracion, String[] infoWifi);
    void activarModoCasa();
    void activarModoCalle();
    void activarModoDormir();
    void desactivarModoDormir();
    void mostrarNombreWifi(String nombre);
    void mostrarSubContainerModoCasaAuto(boolean estado);
    void mostrarSubContainerModoDormirAuto(boolean estado);
    void mostrarListaCategoriasModoCasa();
    void mostrarListaCategoriasModoCalle();
    void mostrarHoraDesde(long tiempo);
    void mostrarHoraHasta(long tiempo);
    void modoCasaAutomaticoHabilitado();
    void modoCasaAutomaticoDeshabilitado();
}
