package com.gitlab.hear4me.phoneApp.di.module;

import com.gitlab.hear4me.phoneApp.android.AlarmaReceiver;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ReceiversModule {
    @ContributesAndroidInjector
    public abstract AlarmaReceiver contributesAlarmaReceiver();
}
