package com.gitlab.hear4me.phoneApp.config;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;

public class PermisosAndroid {

    public static final int PERMISSIONS_MULTIPLE_REQUEST = 123;

    private static String[] PERMISOS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.VIBRATE,
            Manifest.permission.WAKE_LOCK,
            Manifest.permission.DISABLE_KEYGUARD,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_WIFI_STATE,
            Manifest.permission.ACCESS_NETWORK_STATE
    };

    private PermisosAndroid() {}

    public static void solicitarMultiplesPermisos(Activity activity) {
        int permiso = activity.checkSelfPermission(PERMISOS[0]);
        if (permiso != PackageManager.PERMISSION_GRANTED) {
            activity.requestPermissions(PERMISOS, PERMISSIONS_MULTIPLE_REQUEST);
        }
    }

}
