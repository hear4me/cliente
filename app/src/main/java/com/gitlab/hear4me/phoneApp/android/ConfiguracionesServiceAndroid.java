package com.gitlab.hear4me.phoneApp.android;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.gitlab.hear4me.compartido.model.domain.Configuraciones;
import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.model.domain.RedModoCasa;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.compartido.util.TareaUtil;
import com.gitlab.hear4me.phoneApp.PhoneApp;

import java.util.Optional;

public class ConfiguracionesServiceAndroid {

    private Context context;
    private ConfiguracionesService configuracionesService;
    private PhoneIntentFacadeAndroid phoneIntentFacadeAndroid;

    public ConfiguracionesServiceAndroid(Context context,
                                         ConfiguracionesService configuracionesService,
                                         PhoneIntentFacadeAndroid phoneIntentFacadeAndroid) {
        this.context = context;
        this.configuracionesService = configuracionesService;
        this.phoneIntentFacadeAndroid = phoneIntentFacadeAndroid;
    }

    public void determinarModoAutomaticamente() {
        Configuraciones configuraciones = configuracionesService.obtenerConfiguraciones();

        if (!configuraciones.isModoCasaAutomaticoConfigurado()) return;

        if (isRedConectada()) {
            WifiManager wifiManager =
                    (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            String bssid = wifiInfo.getBSSID();

            Optional<RedModoCasa> redModoCasa = configuraciones.getRedesModoCasa().stream()
                    .filter((red) -> red.getId().equals(bssid))
                    .findFirst();

            ModoConfiguracion modoConfiguracionNuevo;
            if (!configuraciones.isModoDormirAutomaticoActivado()) {
                modoConfiguracionNuevo = redModoCasa.isPresent()
                        ? ModoConfiguracion.CASA
                        : ModoConfiguracion.CALLE;
            } else {
                modoConfiguracionNuevo = ModoConfiguracion.CASA_DORMIR;
            }

            activarModo(modoConfiguracionNuevo);
        } else if (!configuraciones.isModoDormirAutomaticoActivado()) {
            activarModo(ModoConfiguracion.CALLE);
        }
    }

    private void activarModo(ModoConfiguracion modoConfiguracion) {
        if (PhoneApp.uiAjustesIniciada) {
            TareaUtil.ejecutarEnUiThread(() ->
                    phoneIntentFacadeAndroid.actualizarConfiguracion(modoConfiguracion));
        } else {
            configuracionesService.cambiarModoConfiguracion(modoConfiguracion);
        }
    }

    private boolean isRedConectada() {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled()) {
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            if( wifiInfo.getNetworkId() == -1 ){
                return false;
            }
            return true;
        }
        else {
            return false;
        }
    }
}
