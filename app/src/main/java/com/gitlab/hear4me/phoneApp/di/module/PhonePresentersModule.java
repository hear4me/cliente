package com.gitlab.hear4me.phoneApp.di.module;

import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.compartido.model.service.MicrofonoService;
import com.gitlab.hear4me.phoneApp.android.AlertaServiceAndroid;
import com.gitlab.hear4me.phoneApp.android.ConfiguracionesServiceAndroid;
import com.gitlab.hear4me.phoneApp.android.PhoneIntentFacadeAndroid;
import com.gitlab.hear4me.phoneApp.presentation.presenter.AjustesPresenter;
import com.gitlab.hear4me.phoneApp.presentation.presenter.AlertaPresenter;
import com.gitlab.hear4me.phoneApp.presentation.presenter.HistorialPresenter;
import com.gitlab.hear4me.phoneApp.presentation.presenter.MainPresenter;
import com.gitlab.hear4me.phoneApp.presentation.presenter.impl.AjustesPresenterImpl;
import com.gitlab.hear4me.phoneApp.presentation.presenter.impl.AlertaPresenterImpl;
import com.gitlab.hear4me.phoneApp.presentation.presenter.impl.HistorialPresenterImpl;
import com.gitlab.hear4me.phoneApp.presentation.presenter.impl.MainPresenterImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PhonePresentersModule {

    @Provides
    @Singleton
    MainPresenter mainPresenter(MicrofonoService microfonoService,
                                ConfiguracionesServiceAndroid configuracionesServiceAndroid) {
        return new MainPresenterImpl(microfonoService, configuracionesServiceAndroid);
    }

    @Provides
    @Singleton
    AjustesPresenter ajustesPresenter(ConfiguracionesService configuracionesService,
                                      ConfiguracionesServiceAndroid configuracionesServiceAndroid,
                                      PhoneIntentFacadeAndroid phoneIntentFacadeAndroid) {
        return new AjustesPresenterImpl(configuracionesService, configuracionesServiceAndroid, phoneIntentFacadeAndroid);
    }

    @Provides
    @Singleton
    HistorialPresenter historialPresenter(AlertaService alertaService) {
        return new HistorialPresenterImpl(alertaService);
    }

    @Provides
    @Singleton
    AlertaPresenter alertaPresenter(AlertaService alertaService,
                                    AlertaServiceAndroid alertaServiceAndroid,
                                    ConfiguracionesService configuracionesService) {
        return new AlertaPresenterImpl(alertaService, alertaServiceAndroid, configuracionesService);
    }
}
