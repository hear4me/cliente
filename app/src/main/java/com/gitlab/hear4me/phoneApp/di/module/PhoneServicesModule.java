package com.gitlab.hear4me.phoneApp.di.module;

import com.gitlab.hear4me.compartido.android.IntentFacadeAndroid;
import com.gitlab.hear4me.compartido.model.domain.FiltroResultadoClasificacion;
import com.gitlab.hear4me.compartido.model.domain.ModelLoader;
import com.gitlab.hear4me.compartido.model.repository.AlertaRepository;
import com.gitlab.hear4me.compartido.model.repository.ConfiguracionesRepository;
import com.gitlab.hear4me.compartido.model.repository.MicrofonoRepository;
import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.compartido.model.service.MicrofonoService;
import com.gitlab.hear4me.phoneApp.model.service.ClasificadorAudioService;
import com.gitlab.hear4me.phoneApp.model.service.DetectorPeligroService;
import com.gitlab.hear4me.phoneApp.model.service.GrabadoraService;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PhoneServicesModule {

    @Provides
    @Singleton
    public ConfiguracionesService configuracionService(ConfiguracionesRepository configuracionesRepository) {
        return new ConfiguracionesService(configuracionesRepository);
    }

    @Provides
    @Singleton
    public ClasificadorAudioService clasificadorAudioService(
            @Named("modelLoaderCasa") ModelLoader modelLoaderModoCasa,
            @Named("modelLoaderCalle") ModelLoader modelLoaderModoCalle) {
        return new ClasificadorAudioService(modelLoaderModoCasa, modelLoaderModoCalle);
    }

    @Provides
    @Singleton
    public GrabadoraService grabadoraService() {
        return new GrabadoraService();
    }

    @Provides
    @Singleton
    public MicrofonoService microfonoService(MicrofonoRepository microfonoRepository, IntentFacadeAndroid intentFacadeAndroid) {
        return new MicrofonoService(microfonoRepository, intentFacadeAndroid);
    }

    @Provides
    @Singleton
    public AlertaService alertaService(AlertaRepository alertaRepository) {
        return new AlertaService(alertaRepository);
    }

    @Provides
    @Singleton
    public DetectorPeligroService detectorPeligroService(
            ConfiguracionesService configuracionesService,
            AlertaService alertaService,
            ClasificadorAudioService clasificadorAudioService,
            GrabadoraService grabadoraService) {
        return new DetectorPeligroService(
                configuracionesService,
                alertaService,
                clasificadorAudioService,
                grabadoraService,
                FiltroResultadoClasificacion.modoCasa(),
                FiltroResultadoClasificacion.modoCalle());
    }
}
