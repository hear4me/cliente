package com.gitlab.hear4me.phoneApp.di.module;

import android.content.Context;

import com.gitlab.hear4me.compartido.model.domain.ModelLoader;
import com.gitlab.hear4me.compartido.model.domain.ModelLoaderAsset;
import com.gitlab.hear4me.phoneApp.config.ApplicationProperties;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ModelLoaderModule {

    @Provides
    @Singleton
    @Named("modelLoaderCasa")
    public ModelLoader modelLoaderCasa(Context context) {
        return new ModelLoaderAsset(ApplicationProperties.ARCHIVO_TFLITE_CASA, context);
    }

    @Provides
    @Singleton
    @Named("modelLoaderCalle")
    public ModelLoader modelLoaderCalle(Context context) {
        return new ModelLoaderAsset(ApplicationProperties.ARCHIVO_TFLITE_CALLE, context);
    }
}
