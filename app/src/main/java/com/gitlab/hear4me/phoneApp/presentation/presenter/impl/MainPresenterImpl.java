package com.gitlab.hear4me.phoneApp.presentation.presenter.impl;

import android.view.View;

import com.gitlab.hear4me.compartido.model.service.MicrofonoService;
import com.gitlab.hear4me.compartido.presentation.view.ActivityView;
import com.gitlab.hear4me.phoneApp.android.ConfiguracionesServiceAndroid;
import com.gitlab.hear4me.phoneApp.android.MicrofonoIntentServiceAndroid;
import com.gitlab.hear4me.phoneApp.presentation.presenter.MainPresenter;
import com.gitlab.hear4me.phoneApp.presentation.view.MainView;

import javax.inject.Inject;

public class MainPresenterImpl implements MainPresenter {

    private MainView mainView;
    private final MicrofonoService microfonoService;
    private final ConfiguracionesServiceAndroid configuracionesServiceAndroid;

    @Inject
    public MainPresenterImpl(MicrofonoService microfonoService, ConfiguracionesServiceAndroid configuracionesServiceAndroid) {
        this.microfonoService = microfonoService;
        this.configuracionesServiceAndroid = configuracionesServiceAndroid;
    }

    @Override
    public void irAAjustes(View view) {

    }

    @Override
    public void irAHistorial(View view) {

    }

    @Override
    public boolean activarMicrofono(View view) {
        boolean microfonoActivo = microfonoService.obtenerEstadoMicrofono();
        if (!microfonoActivo) {
            microfonoService.activarMicrofono();
        } else {
            microfonoService.desactivarMicrofono();
        }
        return !microfonoActivo;
    }

    @Override
    public void onCreate(ActivityView activityView) {
        configuracionesServiceAndroid.determinarModoAutomaticamente();
    }

    @Override
    public void onStart(ActivityView activityView) {

    }

    @Override
    public void onResume(ActivityView activityView) {
        this.mainView = (MainView) activityView;
        boolean estadoMicrofono = microfonoService.obtenerEstadoMicrofono();
        boolean ejecutandoEnBackground = MicrofonoIntentServiceAndroid.continuarEjecucion;
        boolean nuevoEstadoMicrofono = estadoMicrofono && ejecutandoEnBackground;
        mainView.actualizarEstadoMicrofono(nuevoEstadoMicrofono);
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void actualizarEstadoMicrofono(boolean estado) {
        mainView.actualizarEstadoMicrofono(estado);
        microfonoService.cambiarEstadoMicrofono(estado);

        if (estado) {
            microfonoService.activarMicrofono();
        } else {
            microfonoService.desactivarMicrofono();
        }
    }
}
