package com.gitlab.hear4me.phoneApp.android;

import android.app.IntentService;
import android.app.Notification;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.compartido.model.service.MicrofonoService;
import com.gitlab.hear4me.phoneApp.PhoneApp;
import com.gitlab.hear4me.phoneApp.model.service.DetectorPeligroService;

import javax.inject.Inject;

public class MicrofonoIntentServiceAndroid extends IntentService {

    public static boolean continuarEjecucion = false;
    public static volatile boolean clasificacionEnEjecucion = false;

    @Inject
    MicrofonoService microfonoService;

    @Inject
    DetectorPeligroService detectorPeligroService;

    @Inject
    NotificacionesAndroid notificacionesAndroid;

    @Inject
    AlertaService alertaService;

    @Inject
    AlertaServiceAndroid alertaServiceAndroid;

    @Inject
    ConfiguracionesServiceAndroid configuracionesServiceAndroid;

    private static final int NOTIFICATION_ID = 9462;

    public MicrofonoIntentServiceAndroid() {
        super("MicrofonoIntentServiceAndroid");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        PhoneApp.getComponent().inject(this);
        notificacionesAndroid.crearCanalNotificaciones(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        microfonoService.desactivarMicrofono();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Notification notificacion = notificacionesAndroid.crearNotificacion(this);
        startForeground(NOTIFICATION_ID, notificacion);
        ejecutarLoop();
    }

    private synchronized void ejecutarLoop() {
        while(continuarEjecucion) {
            alertaServiceAndroid.mostrarAlertaSiguienteSiExiste();
            configuracionesServiceAndroid.determinarModoAutomaticamente();
            if (!clasificacionEnEjecucion) {
                clasificacionEnEjecucion = true;
                detectorPeligroService.buscarPeligro();
                clasificacionEnEjecucion = false;
            }
        }
    }

}
