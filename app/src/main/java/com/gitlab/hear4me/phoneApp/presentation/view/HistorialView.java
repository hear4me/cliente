package com.gitlab.hear4me.phoneApp.presentation.view;

import android.view.View;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.presentation.view.ActivityView;
import com.gitlab.hear4me.phoneApp.presentation.events.HistorialEvents;

import java.util.List;

public interface HistorialView extends ActivityView, HistorialEvents {
    void mostrarMenu(View view);
    void clickFondoFalso(View view);
    void mostrarHistorial(List<Alerta> historialAlertas);
}
