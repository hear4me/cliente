package com.gitlab.hear4me.phoneApp.android;

import com.gitlab.hear4me.compartido.android.IntentFacadeAndroid;
import com.gitlab.hear4me.compartido.android.WearableServiceAndroid;
import com.gitlab.hear4me.compartido.config.MensajesWearable;
import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.model.domain.Configuraciones;
import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.compartido.util.JsonUtil;

public class AlertaServiceAndroid {

    private AlertaService alertaService;
    private IntentFacadeAndroid intentFacadeAndroid;
    private WearableServiceAndroid wearableServiceAndroid;
    private ConfiguracionesService configuracionesService;

    public AlertaServiceAndroid(
            AlertaService alertaService,
            IntentFacadeAndroid intentFacadeAndroid,
            WearableServiceAndroid wearableServiceAndroid,
            ConfiguracionesService configuracionesService) {
        this.alertaService = alertaService;
        this.intentFacadeAndroid = intentFacadeAndroid;
        this.wearableServiceAndroid = wearableServiceAndroid;
        this.configuracionesService = configuracionesService;
    }

    public void mostrarAlertaSiguienteSiExiste() {
        Alerta alertaMasAntiguaSinVer = alertaService.buscarAlertaMasAntiguaSinVer();
        if (alertaMasAntiguaSinVer != null) {
            String alertaJson = JsonUtil.toJson(alertaMasAntiguaSinVer);
            Configuraciones configuraciones = configuracionesService.obtenerConfiguraciones();

            switch (configuraciones.getDispositivo()) {
                case CELULAR:
                    intentFacadeAndroid.mostrarInterfazAlerta(alertaJson);
                    break;
                case RELOJ:
                    alertaService.marcarAlerta(alertaMasAntiguaSinVer);
                    wearableServiceAndroid.enviarMensaje(MensajesWearable.PHONE_ACTIVA_ALERTA_PATH, alertaJson.getBytes());
                    break;
            }
        }
    }
}
