package com.gitlab.hear4me.phoneApp.presentation.events;

import android.view.View;

public interface HistorialEvents {
    void irAAtras(View view);
    void eliminarHistorial(View view);
}
