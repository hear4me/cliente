package com.gitlab.hear4me.phoneApp.presentation.view.impl;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.TextViewCompat;

import com.gitlab.hear4me.phoneApp.PhoneApp;
import com.gitlab.hear4me.phoneApp.R;
import com.gitlab.hear4me.phoneApp.config.PermisosAndroid;
import com.gitlab.hear4me.phoneApp.presentation.presenter.MainPresenter;
import com.gitlab.hear4me.phoneApp.presentation.view.MainView;

import java.io.File;

import javax.inject.Inject;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MainActivity extends AppCompatActivity implements MainView {

    @Inject
    MainPresenter mainPresenter;

    static File file;

    private Button btnAjustes;
    private Button btnHistorial;
    private Button btnGrabar;
    private TextView txtAvisoMic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PhoneApp.getComponent().inject(this);
        mainPresenter.onCreate(this);

        btnAjustes = findViewById(R.id.btnAjustesMas);
        btnGrabar = findViewById(R.id.btnGrabar);
        btnAjustes = findViewById(R.id.btnAjustesMas);
        btnHistorial = findViewById(R.id.btnHistorialMas);
        txtAvisoMic = findViewById(R.id.txtAvisoMic);

        PermisosAndroid.solicitarMultiplesPermisos(this);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "historial.txt");
        btnGrabar.setOnClickListener(this::activarMicrofono);
        btnAjustes.setOnClickListener(this::irAAjustes);
        btnHistorial.setOnClickListener(this::irAHistorial);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mainPresenter.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mainPresenter.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void irAAjustes(View view) {
        mainPresenter.irAAjustes(view);
        Intent intent = new Intent(this, AjustesActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @Override
    public void irAHistorial(View view) {
        mainPresenter.irAHistorial(view);
        Intent intent = new Intent(this, HistorialActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }

    @Override
    public boolean activarMicrofono(View view) {
        boolean nuevoEstado = mainPresenter.activarMicrofono(view);
        setBtnMicrofonoActivo(nuevoEstado);
        return nuevoEstado;
    }

    @Override
    public void actualizarEstadoMicrofono(boolean estado) {
        setBtnMicrofonoActivo(estado);
    }

    private void setBtnMicrofonoActivo(boolean activo) {
        if (activo) {
            txtAvisoMic.setText(R.string.mic_on);
            TextViewCompat.setTextAppearance(txtAvisoMic, R.style.TextoMic_On);

            btnGrabar.setText(R.string.icon_microphone);
            TextViewCompat.setTextAppearance(btnGrabar, R.style.BotonMic_On);

            TextViewCompat.setTextAppearance(btnAjustes, R.style.BotonIcono_MicOn);
            TextViewCompat.setTextAppearance(btnHistorial, R.style.BotonIcono_MicOn);
        } else {
            txtAvisoMic.setText(R.string.mic_off);
            TextViewCompat.setTextAppearance(txtAvisoMic, R.style.TextoMic_Off);

            btnGrabar.setText(R.string.icon_microphone_slash);
            TextViewCompat.setTextAppearance(btnGrabar, R.style.BotonMic_Off);

            TextViewCompat.setTextAppearance(btnAjustes, R.style.BotonIcono_MicOff);
            TextViewCompat.setTextAppearance(btnHistorial, R.style.BotonIcono_MicOff);
        }
    }
}
