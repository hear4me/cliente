package com.gitlab.hear4me.phoneApp.model.repository.impl;

import com.gitlab.hear4me.compartido.android.WearableServiceAndroid;
import com.gitlab.hear4me.compartido.config.MensajesWearable;
import com.gitlab.hear4me.compartido.model.domain.EstadoMicrofono;
import com.gitlab.hear4me.compartido.model.repository.MicrofonoRepository;
import com.gitlab.hear4me.compartido.util.FileUtil;
import com.gitlab.hear4me.compartido.util.JsonUtil;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;

public class MicrofonoRepositoryImpl implements MicrofonoRepository {

    private File archivoMicrofono;
    private WearableServiceAndroid wearableServiceAndroid;

    @Inject
    public MicrofonoRepositoryImpl(
            File archivoMicrofono,
            WearableServiceAndroid wearableServiceAndroid) {
        this.archivoMicrofono = archivoMicrofono;
        this.wearableServiceAndroid = wearableServiceAndroid;
    }

    public EstadoMicrofono cargar() {
        EstadoMicrofono estado = new EstadoMicrofono(false);
        try {
            if (existeArchivoMicrofono()) {
                String input = FileUtil.leerAString(archivoMicrofono);
                estado = JsonUtil.fromJson(input, EstadoMicrofono.class);
            } else {
                archivoMicrofono.createNewFile();
                guardar(estado);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return estado;
    }

    public void guardar(EstadoMicrofono estado) {
        try {
            String output = JsonUtil.toJson(estado);
            FileUtil.guardar(archivoMicrofono, output);
        } catch (IOException e) {
            e.printStackTrace();
        }
        sincronizar(estado);
    }

    private void sincronizar(EstadoMicrofono estado) {
        String estadoMicJson = JsonUtil.toJson(estado);
        wearableServiceAndroid.enviarMensaje(MensajesWearable.PHONE_WRITE_ESTADO_MIC_PATH, estadoMicJson.getBytes());
    }

    private boolean existeArchivoMicrofono() {
        return archivoMicrofono.exists() && archivoMicrofono.length() != 0;
    }
}
