package com.gitlab.hear4me.phoneApp.model.repository;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.gitlab.hear4me.compartido.model.domain.Alerta;

import java.util.List;

@Dao
public interface PhoneAlertaRepository {

    @Query("SELECT * FROM alerta")
    List<Alerta> buscarTodas();

    @Query("UPDATE alerta SET vista = 1 WHERE vista = 0")
    void marcarTodasComoVistas();

    @Query("SELECT * FROM alerta WHERE vista = 0 ORDER BY tiempo ASC")
    List<Alerta> buscarTodasLasAlertasNoVistas();

    @Insert
    void guardar(Alerta alerta);

    @Update
    void actualizar(Alerta alerta);

    @Query("DELETE FROM alerta WHERE vista = 1")
    void borrarAlertasVistas();

    @Query("SELECT count() FROM alerta WHERE vista = 0")
    int contarTodasLasAlertasNoVistas();

    @Query("UPDATE alerta SET vista = 1 WHERE vista = 0 AND id < (SELECT MAX(id) FROM alerta)")
    void marcarTodasMenosLaUltima();

    @Query("DELETE FROM alerta")
    void borrarTodas();
}
