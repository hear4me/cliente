package com.gitlab.hear4me.phoneApp.presentation.presenter;

import com.gitlab.hear4me.compartido.presentation.events.ActivityEvents;
import com.gitlab.hear4me.phoneApp.presentation.events.MainEvents;

public interface MainPresenter extends MainEvents, ActivityEvents {
    void actualizarEstadoMicrofono(boolean estado);
}
