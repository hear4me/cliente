package com.gitlab.hear4me.phoneApp.di.module;

import android.content.Context;

import com.gitlab.hear4me.phoneApp.config.ApplicationProperties;

import java.io.File;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class FileModule {

    @Provides
    @Singleton
    @Named("archivoConfiguracion")
    public File archivoConfiguracion(Context context) {
        return new File(context.getFilesDir(), ApplicationProperties.ARCHIVO_CONFIGURACION);
    }

    @Provides
    @Singleton
    @Named("archivoMicrofono")
    public File archivoMicrofono(Context context) {
        return new File(context.getFilesDir(), ApplicationProperties.ARCHIVO_MICROFONO);
    }
}
