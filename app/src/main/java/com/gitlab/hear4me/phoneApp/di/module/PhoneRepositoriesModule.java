package com.gitlab.hear4me.phoneApp.di.module;

import com.gitlab.hear4me.compartido.android.WearableServiceAndroid;
import com.gitlab.hear4me.compartido.model.repository.AlertaRepository;
import com.gitlab.hear4me.compartido.model.repository.ConfiguracionesRepository;
import com.gitlab.hear4me.compartido.model.repository.MicrofonoRepository;
import com.gitlab.hear4me.compartido.util.TareaUtil;
import com.gitlab.hear4me.phoneApp.model.db.AppDatabase;
import com.gitlab.hear4me.phoneApp.model.repository.PhoneAlertaRepository;
import com.gitlab.hear4me.phoneApp.model.repository.impl.AlertaRepositoryWrapper;
import com.gitlab.hear4me.phoneApp.model.repository.impl.ConfiguracionesRepositoryImpl;
import com.gitlab.hear4me.phoneApp.model.repository.impl.MicrofonoRepositoryImpl;

import java.io.File;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PhoneRepositoriesModule {

    @Provides
    @Singleton
    public ConfiguracionesRepository configuracionRepository(
            @Named("archivoConfiguracion") File archivoConfiguracion,
            WearableServiceAndroid wearableServiceAndroid) {
        return new ConfiguracionesRepositoryImpl(archivoConfiguracion, wearableServiceAndroid);
    }

    @Provides
    @Singleton
    public MicrofonoRepository microfonoRepository(
            @Named("archivoMicrofono") File archivoMicrofono,
            WearableServiceAndroid wearableServiceAndroid) {
        return new MicrofonoRepositoryImpl(archivoMicrofono, wearableServiceAndroid);
    }

    @Provides
    @Singleton
    public AlertaRepository alertaRepository(AppDatabase database) {
        PhoneAlertaRepository phoneAlertaRepository = database.phoneAlertaRepository();
        AlertaRepository alertaRepository = new AlertaRepositoryWrapper(phoneAlertaRepository);
        TareaUtil.ejecutar(() -> alertaRepository.marcarTodasComoVistas());
        return alertaRepository;
    }
}
