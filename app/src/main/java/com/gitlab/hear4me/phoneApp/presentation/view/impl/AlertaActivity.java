package com.gitlab.hear4me.phoneApp.presentation.view.impl;

import android.content.Context;
import android.content.Intent;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.util.JsonUtil;
import com.gitlab.hear4me.compartido.util.TareaUtil;
import com.gitlab.hear4me.phoneApp.PhoneApp;
import com.gitlab.hear4me.phoneApp.R;
import com.gitlab.hear4me.phoneApp.presentation.presenter.AlertaPresenter;
import com.gitlab.hear4me.phoneApp.presentation.view.AlertaView;

import org.apache.commons.lang3.time.DurationFormatUtils;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;

public class AlertaActivity extends AppCompatActivity implements AlertaView {

    @Inject
    AlertaPresenter alertaPresenter;

    private static final int INTERVALO_CONTADOR_TIEMPO_TRANSCURRIDO = 200;

    private static Handler actualizadorContadorTiempoTranscurrido = new Handler();

    private Vibrator vibrador;
    private CircleImageView imgCategoriaDetectada;
    private TextView txtCategoriaDetectada;
    private TextView txtContador;
    private Button btnVerAlertaMasReciente;
    private Button btnDescartarAlerta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alerta);
        PhoneApp.getComponent().inject(this);
        alertaPresenter.onCreate(this);

        imgCategoriaDetectada = findViewById(R.id.imgCategoriaDetectada);
        imgCategoriaDetectada.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        txtCategoriaDetectada = findViewById(R.id.txtCategoriaDetectada);
        txtContador = findViewById(R.id.txtContador);

        btnVerAlertaMasReciente = findViewById(R.id.btnVerAlertaMasReciente);
        btnDescartarAlerta = findViewById(R.id.btnDescartarAlerta);

        btnVerAlertaMasReciente.setOnClickListener(this::mostrarAlertaMasReciente);
        btnDescartarAlerta.setOnClickListener(this::descartarAlertaActual);

        if (vibrador == null) {
            vibrador = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        } else {
            vibrador.cancel();
        }

        activarAlerta(getIntent());
    }

    @Override
    protected void onResume() {
        super.onResume();
        alertaPresenter.onResume(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        activarAlerta(intent);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        alertaPresenter.onWindowFocusChanged(hasFocus);
    }

    @Override
    protected void onPause() {
        super.onPause();
        alertaPresenter.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        alertaPresenter.onDestroy();
    }

    @Override
    public void alertaActivada(Alerta alerta) {
        alertaPresenter.alertaActivada(alerta);
    }

    @Override
    public void mostrarAlertaMasReciente(View view) {
        alertaPresenter.mostrarAlertaMasReciente(view);
    }

    @Override
    public void descartarAlertaActual(View view) {
        alertaPresenter.descartarAlertaActual(view);
    }

    @Override
    public void activarFlashAtras() {
        cambiarEstadoFlash(true);
    }

    @Override
    public void activarVibracion() {
        long[] pattern = {0, 700, 300};
        vibrador.vibrate(pattern, 1);
    }

    @Override
    public void desactivarFlashAtras() {
        cambiarEstadoFlash(false);
    }

    @Override
    public void desactivarVibracion() {
        vibrador.cancel();
    }

    @Override
    public void actualizarCantidadAlertasEnCola(String textoBotonActualizado) {
        btnDescartarAlerta.setText(textoBotonActualizado);
    }

    @Override
    public void actualizarTiempoTranscurrido(Alerta alerta) {
        actualizadorContadorTiempoTranscurrido.removeCallbacksAndMessages(null);
        TareaUtil.repetirEnUiThread(actualizadorContadorTiempoTranscurrido,
                () -> actualizarContadorTiempoTranscurrido(alerta), INTERVALO_CONTADOR_TIEMPO_TRANSCURRIDO);
    }

    @Override
    public void cerrarAlerta() {
        finish();
    }

    @Override
    public void mostrarAlerta(Alerta alerta) {
        mostrarAlerta(alerta, txtCategoriaDetectada, imgCategoriaDetectada);
    }

    private void activarAlerta(Intent intent) {
        if (intent.getExtras() != null) {
            String resultadoJson = intent.getExtras().getString("resultado_clasificacion");
            Alerta alerta = parsearResultadoClasificacion(resultadoJson);
            alertaActivada(alerta);
        }
    }

    private void mostrarAlerta(Alerta alerta, TextView txtResultado, CircleImageView imgResultado) {
        txtResultado.setText(alerta.getPrediccion().getCategoria().getNombre());
        int idImagenCategoria = getResources().getIdentifier(
                alerta.getPrediccion().getCategoria().toString(), "drawable", getPackageName());
        imgResultado.setImageResource(idImagenCategoria);
    }

    private void actualizarContadorTiempoTranscurrido(Alerta alerta) {
        long tiempoActual = System.currentTimeMillis();
        long tiempoQuePaso = tiempoActual - alerta.getTiempo().getTime();

        String tiempoQuePasoConFormato =
                DurationFormatUtils.formatDuration(tiempoQuePaso, "HH:mm:ss", true);

        txtContador.setText(tiempoQuePasoConFormato);
    }

    private Alerta parsearResultadoClasificacion(String resultadoJson) {
        return JsonUtil.fromJson(resultadoJson, Alerta.class);
    }

    private void cambiarEstadoFlash(boolean estado) {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            String cameraId = cameraManager.getCameraIdList()[0];
            cameraManager.setTorchMode(cameraId, estado);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

}
