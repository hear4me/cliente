package com.gitlab.hear4me.phoneApp.presentation.presenter;

import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.presentation.events.ActivityEvents;
import com.gitlab.hear4me.phoneApp.presentation.events.AjustesEvents;

public interface AjustesPresenter extends AjustesEvents, ActivityEvents {
    void activarModoCorrespondiente();
    void actualizarConfiguracion(ModoConfiguracion modoConfiguracionSiguiente);
    void cambioEnLaRed();
    void horaHastaSeteada(long tiempo);
    void horaDesdeSeteada(long tiempo);
    void habilitarModoCasaAutomatico();
    void deshabilitarModoCasaAutomatico();
}
