package com.gitlab.hear4me.phoneApp.android;

import com.gitlab.hear4me.compartido.android.WearableServiceAndroid;
import com.gitlab.hear4me.compartido.config.MensajesWearable;
import com.gitlab.hear4me.compartido.model.domain.Configuraciones;
import com.gitlab.hear4me.compartido.model.domain.EstadoMicrofono;
import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.model.repository.AlertaRepository;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.compartido.model.service.MicrofonoService;
import com.gitlab.hear4me.compartido.util.JsonUtil;
import com.gitlab.hear4me.compartido.util.TareaUtil;
import com.gitlab.hear4me.phoneApp.PhoneApp;
import com.gitlab.hear4me.phoneApp.presentation.presenter.AjustesPresenter;
import com.gitlab.hear4me.phoneApp.presentation.presenter.MainPresenter;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import javax.inject.Inject;

public class DatalayerListenerService extends WearableListenerService {

    @Inject
    ConfiguracionesService configuracionesService;

    @Inject
    MicrofonoService microfonoService;

    @Inject
    AjustesPresenter ajustesPresenter;

    @Inject
    MainPresenter mainPresenter;

    @Inject
    WearableServiceAndroid wearableServiceAndroid;

    @Inject
    AlertaRepository alertaRepository;

    @Inject
    ConfiguracionesServiceAndroid configuracionesServiceAndroid;

    public DatalayerListenerService() {}

    @Override
    public void onCreate() {
        super.onCreate();
        PhoneApp.getComponent().inject(this);
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);
        byte[] data = messageEvent.getData();

        switch (messageEvent.getPath()) {
            /** MAIN */
            case MensajesWearable.WATCH_WRITE_ESTADO_MIC_PATH:
                actualizarEstadoMicrofono(data);
                break;
            case MensajesWearable.WATCH_REQ_ESTADO_MIC_PATH:
                respoderSolicitudEstadoMicrofono();
                break;
            /** CONFIGURACION */
            case MensajesWearable.WATCH_WRITE_CONFIG_PATH:
                modificarConfiguracion(data);
                break;
            case MensajesWearable.WATCH_REQ_CONFIG_PATH:
                responderSolicitudConfiguracion();
                break;
        }
    }

    private void actualizarEstadoMicrofono(byte[] data) {
        String nuevoEstadoMicJson = new String(data);
        EstadoMicrofono nuevoEstadoMicrofono = JsonUtil.fromJson(nuevoEstadoMicJson, EstadoMicrofono.class);
        TareaUtil.ejecutarEnUiThread(() ->
                mainPresenter.actualizarEstadoMicrofono(nuevoEstadoMicrofono.getEstado()));
    }

    private void respoderSolicitudEstadoMicrofono() {
        EstadoMicrofono estadoMicrofono = new EstadoMicrofono(microfonoService.obtenerEstadoMicrofono());
        String estadoMicJson = JsonUtil.toJson(estadoMicrofono);
        wearableServiceAndroid.enviarMensaje(MensajesWearable.PHONE_REQ_ANS_ESTADO_MIC_PATH, estadoMicJson.getBytes());
    }

    /***
     * Se ejecuta cuando en el reloj se actualiza la configuracion
     */
    private void modificarConfiguracion(byte[] data) {
        String configuracionesJson = new String(data);
        Configuraciones configuraciones = JsonUtil.fromJson(configuracionesJson, Configuraciones.class);
        ModoConfiguracion modoConfiguracionSeleccionado = configuraciones.getModoConfiguracionSeleccionado();

        if (configuracionesService.obtenerConfiguraciones().isModoCasaAutomaticoActivado()) {
            configuracionesServiceAndroid.determinarModoAutomaticamente();
        } else {
            if (PhoneApp.uiAjustesIniciada) {
                TareaUtil.ejecutarEnUiThread(() -> {
                    ajustesPresenter.actualizarConfiguracion(modoConfiguracionSeleccionado);
                    ajustesPresenter.activarModoCorrespondiente();
                });
            } else {
                configuracionesService.cambiarModoConfiguracion(modoConfiguracionSeleccionado);
            }
        }
    }

    private void responderSolicitudConfiguracion() {
        Configuraciones configuraciones = configuracionesService.obtenerConfiguraciones();
        String configuracionesJson = JsonUtil.toJson(configuraciones);
        wearableServiceAndroid.enviarMensaje(MensajesWearable.PHONE_REQ_ANS_CONFIG_PATH, configuracionesJson.getBytes());
    }
}
