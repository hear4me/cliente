package com.gitlab.hear4me.phoneApp;

import android.app.Application;
import android.content.BroadcastReceiver;

import com.gitlab.hear4me.phoneApp.di.component.DaggerPhoneAppComponent;
import com.gitlab.hear4me.phoneApp.di.component.PhoneAppComponent;
import com.gitlab.hear4me.phoneApp.di.module.AppModule;
import com.gitlab.hear4me.phoneApp.di.module.FileModule;
import com.gitlab.hear4me.phoneApp.di.module.ModelLoaderModule;
import com.gitlab.hear4me.phoneApp.di.module.PhonePresentersModule;
import com.gitlab.hear4me.phoneApp.di.module.PhoneRepositoriesModule;
import com.gitlab.hear4me.phoneApp.di.module.PhoneServicesModule;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasBroadcastReceiverInjector;

public class PhoneApp extends Application implements HasBroadcastReceiverInjector {

    @Inject
    DispatchingAndroidInjector<BroadcastReceiver> broadcastReceiverInjector;

    public static boolean uiAjustesIniciada = false;

    private static PhoneAppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = buildAppComponent();
        component.inject(this);
    }

    public static PhoneAppComponent getComponent() {
        return component;
    }

    private PhoneAppComponent buildAppComponent() {
        return DaggerPhoneAppComponent.builder()
                .appModule(new AppModule(this))
                .fileModule(new FileModule())
                .modelLoaderModule(new ModelLoaderModule())
                .phonePresentersModule(new PhonePresentersModule())
                .phoneRepositoriesModule(new PhoneRepositoriesModule())
                .phoneServicesModule(new PhoneServicesModule())
                .build();
    }

    @Override
    public AndroidInjector<BroadcastReceiver> broadcastReceiverInjector() {
        return broadcastReceiverInjector;
    }
}
