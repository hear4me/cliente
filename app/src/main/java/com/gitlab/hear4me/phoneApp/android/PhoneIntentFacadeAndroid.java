package com.gitlab.hear4me.phoneApp.android;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.gitlab.hear4me.compartido.android.IntentFacadeAndroid;
import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.phoneApp.presentation.presenter.AjustesPresenter;
import com.gitlab.hear4me.phoneApp.presentation.view.impl.AlertaActivity;

import javax.inject.Inject;

import dagger.Lazy;

public class PhoneIntentFacadeAndroid implements IntentFacadeAndroid {

    private Context context;
    private Lazy<AjustesPresenter> ajustesPresenter;

    private static final int REQ_CODE_ALARMA_CASA = 1;
    private static final int REQ_CODE_ALARMA_DORMIR = 2;

    @Inject
    public PhoneIntentFacadeAndroid(Context context, Lazy<AjustesPresenter> ajustesPresenter) {
        this.context = context;
        this.ajustesPresenter = ajustesPresenter;
    }

    public void activarMicrofono() {
        Intent serviceIntent = new Intent(context, MicrofonoIntentServiceAndroid.class);
        MicrofonoIntentServiceAndroid.continuarEjecucion = true;
        context.startService(serviceIntent);
    }

    public void desactivarMicrofono() {
        Intent serviceIntent = new Intent(context, MicrofonoIntentServiceAndroid.class);
        MicrofonoIntentServiceAndroid.continuarEjecucion = false;
        context.stopService(serviceIntent);
    }

    public void mostrarInterfazAlerta(String alertaJson) {
        Intent intent = new Intent(context, AlertaActivity.class)
                .setAction("hear4me_alerta")
                .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_ANIMATION)
                .putExtra("resultado_clasificacion", alertaJson);
        context.startActivity(intent);
    }

    public void actualizarConfiguracion(ModoConfiguracion modoConfiguracion) {
        AjustesPresenter ajustesPresenter = this.ajustesPresenter.get();
        ajustesPresenter.actualizarConfiguracion(modoConfiguracion);
        ajustesPresenter.activarModoCorrespondiente();
    }

    public void configurarAlarmaCasa(Context context, long tiempo) {
        context = context.getApplicationContext();
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmaReceiver.class);
        intent.setAction("hear4me_alarma_casa");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, REQ_CODE_ALARMA_CASA, intent, 0);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, tiempo, pendingIntent);
    }

    public void cancelarAlarmaCasa(Context context) {
        context = context.getApplicationContext();
        Intent intent = new Intent(context, AlarmaReceiver.class);
        intent.setAction("hear4me_alarma_casa");
        PendingIntent sender = PendingIntent.getBroadcast(context, REQ_CODE_ALARMA_CASA, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    public void configurarAlarmaDormir(Context context, long tiempo) {
        context = context.getApplicationContext();
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmaReceiver.class);
        intent.setAction("hear4me_alarma_dormir");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, REQ_CODE_ALARMA_DORMIR, intent, 0);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, tiempo, pendingIntent);
    }

    public void cancelarAlarmaDormir(Context context) {
        context = context.getApplicationContext();
        Intent intent = new Intent(context, AlarmaReceiver.class);
        intent.setAction("hear4me_alarma_dormir");
        PendingIntent sender = PendingIntent.getBroadcast(context, REQ_CODE_ALARMA_DORMIR, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

}
