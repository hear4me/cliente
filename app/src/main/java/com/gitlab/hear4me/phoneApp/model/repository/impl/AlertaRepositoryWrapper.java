package com.gitlab.hear4me.phoneApp.model.repository.impl;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.model.repository.AlertaRepository;
import com.gitlab.hear4me.phoneApp.model.repository.PhoneAlertaRepository;

import java.util.List;

public class AlertaRepositoryWrapper implements AlertaRepository {

    private PhoneAlertaRepository alertaRepository;

    public AlertaRepositoryWrapper(PhoneAlertaRepository phoneAlertaRepository) {
        alertaRepository = phoneAlertaRepository;
    }

    @Override
    public List<Alerta> buscarTodas() {
        return alertaRepository.buscarTodas();
    }

    @Override
    public void guardar(Alerta alerta) {
        alertaRepository.guardar(alerta);
    }

    @Override
    public void actualizar(Alerta alerta) {
        alertaRepository.actualizar(alerta);
    }

    @Override
    public int contarTodasLasAlertasNoVistas() {
        return alertaRepository.contarTodasLasAlertasNoVistas();
    }

    @Override
    public void marcarTodasComoVistas() {
        alertaRepository.marcarTodasComoVistas();
    }

    @Override
    public void marcarTodasMenosLaUltima() {
        alertaRepository.marcarTodasMenosLaUltima();
    }

    @Override
    public List<Alerta> buscarTodasLasAlertasNoVistas() {
        return alertaRepository.buscarTodasLasAlertasNoVistas();
    }

    @Override
    public void borrarTodas() {
        alertaRepository.borrarTodas();
    }

    @Override
    public void borrarAlertasVistas() {
        alertaRepository.borrarAlertasVistas();
    }
}
