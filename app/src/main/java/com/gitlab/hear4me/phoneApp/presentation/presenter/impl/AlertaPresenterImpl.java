package com.gitlab.hear4me.phoneApp.presentation.presenter.impl;

import android.view.View;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.model.domain.Configuracion;
import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.model.domain.ModoNotificacion;
import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.compartido.presentation.view.ActivityView;
import com.gitlab.hear4me.compartido.util.TareaUtil;
import com.gitlab.hear4me.phoneApp.android.AlertaServiceAndroid;
import com.gitlab.hear4me.phoneApp.presentation.presenter.AlertaPresenter;
import com.gitlab.hear4me.phoneApp.presentation.view.AlertaView;

import java.util.Map;

import javax.inject.Inject;

public class AlertaPresenterImpl implements AlertaPresenter {

    private static final String TXT_DESCARTAR_ALERTA = "DESCARTAR ALERTA";
    private static boolean alertaVisible = false;

    private AlertaView alertaView;

    private AlertaService alertaService;
    private AlertaServiceAndroid alertaServiceAndroid;
    private ConfiguracionesService configuracionesService;
    private boolean alertaDescartada = false;

    @Inject
    public AlertaPresenterImpl(AlertaService alertaService, AlertaServiceAndroid alertaServiceAndroid, ConfiguracionesService configuracionesService) {
        this.alertaService = alertaService;
        this.alertaServiceAndroid = alertaServiceAndroid;
        this.configuracionesService = configuracionesService;
    }

    @Override
    public void alertaActivada(Alerta alerta) {
        if (!alertaVisible) {
            alertaVisible = true;
            alertaView.mostrarAlerta(alerta);
            alertaView.actualizarTiempoTranscurrido(alerta);
            activarNotificaciones();
        }
        actualizarContadorAlertas();
    }

    @Override
    public void mostrarAlertaMasReciente(View view) {
        desactivarNotificaciones();
        cerrarVistaSiNoHayMasAlertas();
        TareaUtil.ejecutar(() -> {
            alertaService.marcarTodasLasAlertasMenosLaUltima();
            alertaServiceAndroid.mostrarAlertaSiguienteSiExiste();
        });
    }

    @Override
    public void descartarAlertaActual(View view) {
        alertaDescartada = true;
        desactivarNotificaciones();
        alertaView.cerrarAlerta();
        TareaUtil.ejecutar(() -> {
            alertaService.marcarAlertaActual();
            alertaServiceAndroid.mostrarAlertaSiguienteSiExiste();
        });
    }

    @Override
    public void onCreate(ActivityView activityView) {
        this.alertaView = (AlertaView) activityView;
    }

    @Override
    public void onStart(ActivityView activityView) {

    }

    @Override
    public void onResume(ActivityView activityView) {

    }

    @Override
    public void onPause() {
    }

    @Override
    public void onStop() {
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (!hasFocus) {
            alertaVisible = false;
            desactivarNotificaciones();
            if (!alertaDescartada) {
                TareaUtil.ejecutar(() -> alertaService.marcarAlertaActual());
            } else {
                alertaDescartada = false;
            }
        }
    }

    private Map<ModoNotificacion, Boolean> buscarModosNotificacion() {
        ModoConfiguracion modoActual =
                configuracionesService.obtenerModoConfiguracionActivo();
        Configuracion configuracion =
                configuracionesService.obtenerConfiguracion(modoActual);

        return configuracion.getModosNotificacion();
    }

    private void activarNotificaciones() {
        Map<ModoNotificacion, Boolean> modosNotificacion = buscarModosNotificacion();
        if (modosNotificacion.get(ModoNotificacion.LUZ)) {
            alertaView.activarFlashAtras();
        }
        if (modosNotificacion.get(ModoNotificacion.VIBRACION)) {
            alertaView.activarVibracion();
        }
    }

    private void actualizarContadorAlertas() {
        TareaUtil.ejecutar(() -> {
            int alertasNoVistas = alertaService.contarTodasLasAlertasNoVistas() - 1;
            String textoBotonActualizado = (alertasNoVistas > 0)
                    ? String.format("%s (+%d)", TXT_DESCARTAR_ALERTA, alertasNoVistas)
                    : TXT_DESCARTAR_ALERTA;

            alertaView.actualizarCantidadAlertasEnCola(textoBotonActualizado);
        });
    }

    private void cerrarVistaSiNoHayMasAlertas() {
        TareaUtil.ejecutar(() -> {
            if (alertaService.contarTodasLasAlertasNoVistas() - 1 > 0) {
                alertaService.marcarAlertaActual();
                alertaView.cerrarAlerta();
            }
        });
    }

    private void desactivarNotificaciones() {
        Map<ModoNotificacion, Boolean> modosNotificacion = buscarModosNotificacion();
        if (modosNotificacion.get(ModoNotificacion.LUZ)) {
            alertaView.desactivarFlashAtras();
        }
        if (modosNotificacion.get(ModoNotificacion.VIBRACION)) {
            alertaView.desactivarVibracion();
        }
    }
}
