package com.gitlab.hear4me.phoneApp.presentation.view.impl;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.TextViewCompat;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.model.domain.Prediccion;
import com.gitlab.hear4me.compartido.util.DialogUtil;
import com.gitlab.hear4me.phoneApp.PhoneApp;
import com.gitlab.hear4me.phoneApp.R;
import com.gitlab.hear4me.phoneApp.presentation.presenter.HistorialPresenter;
import com.gitlab.hear4me.phoneApp.presentation.view.HistorialView;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.inject.Inject;

public class HistorialActivity extends AppCompatActivity implements HistorialView {

    @Inject
    HistorialPresenter historialPresenter;

    private TableLayout lista;
    private Button btnHistorialMas;
    private Button btnEliminarHistorial;
    private ConstraintLayout fondoFalso;
    private Button btnHistorialBack;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);
        PhoneApp.getComponent().inject(this);

        lista = findViewById(R.id.lista);
        btnHistorialMas = findViewById(R.id.btnHistorialMas);
        fondoFalso = findViewById(R.id.HistorialFondoFalso);
        btnEliminarHistorial = findViewById(R.id.btnEliminarHistorial);
        btnHistorialBack = findViewById(R.id.btnHistorialBack);

        btnHistorialMas.setOnClickListener(this::mostrarMenu);
        fondoFalso.setOnClickListener(this::clickFondoFalso);
        btnEliminarHistorial.setOnClickListener(this::eliminarHistorial);
        btnHistorialBack.setOnClickListener(this::irAAtras);
    }

    @Override
    protected void onResume() {
        super.onResume();
        historialPresenter.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        historialPresenter.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    public void mostrarMenu(View view) {
        fondoFalso.setVisibility(View.VISIBLE);
    }

    @Override
    public void clickFondoFalso(View view) {
        fondoFalso.setVisibility(View.GONE);
    }

    @Override
    public void mostrarHistorial(List<Alerta> historialAlertas) {
        for (Alerta alerta : historialAlertas) {
            TableRow row = new TableRow(this);
            row.setWeightSum(10f);
            lista.addView(row);

            Prediccion prediccion = alerta.getPrediccion();

            SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");
            String fecha = formatoFecha.format(alerta.getTiempo().getTime());
            String hora = formatoHora.format(alerta.getTiempo().getTime());

            String[] textos = {
                    prediccion.getCategoria().getNombre(),
                    fecha,
                    hora};

            float[] weights = {5f, 3f, 2f};

            for (int i = 0; i < 3; i++) {
                TextView text;
                text = new TextView(this);
                row.addView(text);

                TableRow.LayoutParams params = (TableRow.LayoutParams) text.getLayoutParams();
                params.width = 0;
                params.weight = weights[i];

                text.setLayoutParams(params);
                text.setText(textos[i]);
                TextViewCompat.setTextAppearance(text, R.style.TextoItemHistorial);
            }
        }
    }

    @Override
    public void irAAtras(View view) {
        historialPresenter.irAAtras(view);
        finish();
    }

    @Override
    public void eliminarHistorial(View view) {
        DialogUtil.mostrar(this,
                "Confirmar borrar historial",
                "Esta acción no se puede deshacer",
                "Borrar",
                () -> {
                    historialPresenter.eliminarHistorial(view);
                    finish();
                });
    }

}
