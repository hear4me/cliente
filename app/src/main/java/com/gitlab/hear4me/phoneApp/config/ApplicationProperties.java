package com.gitlab.hear4me.phoneApp.config;

public class ApplicationProperties {

    private ApplicationProperties() {}

    public static final String ARCHIVO_CONFIGURACION = "configuracion.json";
    public static final String ARCHIVO_HISTORIAL = "historial.json";
    public static final String ARCHIVO_MICROFONO = "microfono.json";
    public static final String ARCHIVO_TFLITE_CASA = "clasificador_sonidos_casa.tflite";
    public static final String ARCHIVO_TFLITE_CALLE = "clasificador_sonidos_calle.tflite";
}
