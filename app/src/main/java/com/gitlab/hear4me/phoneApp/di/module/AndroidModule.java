package com.gitlab.hear4me.phoneApp.di.module;

import android.content.Context;

import com.gitlab.hear4me.compartido.android.IntentFacadeAndroid;
import com.gitlab.hear4me.compartido.android.WearableServiceAndroid;
import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.phoneApp.android.AlertaServiceAndroid;
import com.gitlab.hear4me.phoneApp.android.ConfiguracionesServiceAndroid;
import com.gitlab.hear4me.phoneApp.android.MicrofonoIntentServiceAndroid;
import com.gitlab.hear4me.phoneApp.android.NotificacionesAndroid;
import com.gitlab.hear4me.phoneApp.android.PhoneIntentFacadeAndroid;
import com.gitlab.hear4me.phoneApp.presentation.presenter.AjustesPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AndroidModule {

    @Provides
    @Singleton
    public IntentFacadeAndroid intentFacadeAndroid(Context context, AjustesPresenter ajustesPresenter) {
        return new PhoneIntentFacadeAndroid(context, () -> ajustesPresenter);
    }

    @Provides
    @Singleton
    public MicrofonoIntentServiceAndroid microfonoIntentServiceAndroid() {
        return new MicrofonoIntentServiceAndroid();
    }

    @Provides
    @Singleton
    public NotificacionesAndroid notificacionesAndroid() {
        return new NotificacionesAndroid();
    }

    @Provides
    @Singleton
    public AlertaServiceAndroid alertaServiceAndroid(AlertaService alertaService,
                                                     IntentFacadeAndroid intentFacadeAndroid,
                                                     WearableServiceAndroid wearableServiceAndroid,
                                                     ConfiguracionesService configuracionesService) {
        return new AlertaServiceAndroid(alertaService, intentFacadeAndroid, wearableServiceAndroid, configuracionesService);
    }

    @Provides
    @Singleton
    public WearableServiceAndroid wearableServiceAndroid(Context context) {
        return new WearableServiceAndroid(context);
    }

    @Provides
    @Singleton
    public ConfiguracionesServiceAndroid configuracionesServiceAndroid(Context context,
                                                                       ConfiguracionesService configuracionesService,
                                                                       PhoneIntentFacadeAndroid phoneIntentFacadeAndroid) {
        return new ConfiguracionesServiceAndroid(context, configuracionesService, phoneIntentFacadeAndroid);
    }


}
