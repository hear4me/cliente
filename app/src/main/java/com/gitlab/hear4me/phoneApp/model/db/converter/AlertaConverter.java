package com.gitlab.hear4me.phoneApp.model.db.converter;

import androidx.room.TypeConverter;

import com.gitlab.hear4me.compartido.model.domain.Prediccion;
import com.gitlab.hear4me.compartido.util.JsonUtil;

import java.sql.Timestamp;

public class AlertaConverter {

    @TypeConverter
    public static Prediccion jsonToPrediccion(String json) {
        return JsonUtil.fromJson(json, Prediccion.class);
    }

    @TypeConverter
    public static String prediccionToJson(Prediccion prediccion) {
        return JsonUtil.toJson(prediccion);
    }

    @TypeConverter
    public static Timestamp timeToTimestamp(long time) {
        return new Timestamp(time);
    }

    @TypeConverter
    public static long  timestampToTime(Timestamp timestamp) {
        return timestamp.getTime();
    }

}
