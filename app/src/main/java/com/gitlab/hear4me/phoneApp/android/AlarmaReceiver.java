package com.gitlab.hear4me.phoneApp.android;

import android.app.AlarmManager;
import android.content.Context;
import android.content.Intent;

import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.phoneApp.presentation.presenter.AjustesPresenter;

import javax.inject.Inject;

import dagger.android.DaggerBroadcastReceiver;

public class AlarmaReceiver extends DaggerBroadcastReceiver {

    @Inject
    AjustesPresenter ajustesPresenter;

    @Inject
    PhoneIntentFacadeAndroid phoneIntentFacadeAndroid;

    @Inject
    ConfiguracionesService configuracionesService;

    @Inject
    ConfiguracionesServiceAndroid configuracionesServiceAndroid;

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        if (configuracionesService.isModoDormirAutomaticoConfigurado()) {
            if (intent.getAction().equals("hear4me_alarma_dormir")) {
                ajustesPresenter.deshabilitarModoCasaAutomatico();
                configuracionesService.setModoCasaAutomaticoActivado(false);
                configuracionesService.setModoDormirAutomaticoActivado(true);
                ajustesPresenter.clickModoDormir(null);
            } else if (intent.getAction().equals("hear4me_alarma_casa")) {
                ajustesPresenter.habilitarModoCasaAutomatico();
                configuracionesService.setModoCasaAutomaticoActivado(
                        configuracionesService.isModoCasaAutomaticoConfigurado());
                configuracionesService.setModoDormirAutomaticoActivado(false);
                configuracionesServiceAndroid.determinarModoAutomaticamente();
                ajustesPresenter.activarModoCorrespondiente();

                long tiempoProximaAlarmaModoDormir = configuracionesService.obtenerHoraDesde() + AlarmManager.INTERVAL_DAY;
                configuracionesService.setHoraDesde(tiempoProximaAlarmaModoDormir);
                phoneIntentFacadeAndroid.configurarAlarmaDormir(context, tiempoProximaAlarmaModoDormir);
                long tiempoProximaAlarmaModoCasa = configuracionesService.obtenerHoraHasta() + AlarmManager.INTERVAL_DAY;
                configuracionesService.setHoraHasta(tiempoProximaAlarmaModoCasa);
                phoneIntentFacadeAndroid.configurarAlarmaCasa(context, tiempoProximaAlarmaModoCasa);
            } else if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                phoneIntentFacadeAndroid.configurarAlarmaDormir(context,
                        configuracionesService.obtenerHoraDesde() + AlarmManager.INTERVAL_DAY);
                phoneIntentFacadeAndroid.configurarAlarmaCasa(context,
                        configuracionesService.obtenerHoraHasta() + AlarmManager.INTERVAL_DAY);
            }
        }
    }

}