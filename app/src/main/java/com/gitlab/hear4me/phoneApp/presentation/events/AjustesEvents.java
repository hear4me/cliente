package com.gitlab.hear4me.phoneApp.presentation.events;

import android.view.View;

public interface AjustesEvents {
    void irAAtras(View view);
    void deshacerCambios(View view);
    void restablecerConfiguracion(View view);
    void mostrarOpcionesModosAutomaticos(View view);
    void mostrarOpcionesDispositivos(View view);
    void mostrarOpcionesCategorias(View view);
    void mostrarOpcionesModosNotificacion(View view);
    void activarCelular(View view);
    void activarReloj(View view);
    void clickModoCasa(View view);
    void clickModoCalle(View view);
    void clickModoDormir(View view);
    void clickSwitchModoCasaAuto(View view);
    void clickSwitchModoDormirAuto(View view);
    void clickSwitchActivarAutoParaRed(View view);
    void clickDesde(View view);
    void clickHasta(View view);
}
