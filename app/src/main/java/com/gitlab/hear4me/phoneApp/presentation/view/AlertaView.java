package com.gitlab.hear4me.phoneApp.presentation.view;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.presentation.view.ActivityView;
import com.gitlab.hear4me.phoneApp.presentation.events.AlertaEvents;

public interface AlertaView extends ActivityView, AlertaEvents {
    void mostrarAlerta(Alerta alerta);
    void activarFlashAtras();
    void activarVibracion();
    void desactivarFlashAtras();
    void desactivarVibracion();
    void actualizarCantidadAlertasEnCola(String textoBotonActualizado);
    void actualizarTiempoTranscurrido(Alerta alerta);
    void cerrarAlerta();
}
