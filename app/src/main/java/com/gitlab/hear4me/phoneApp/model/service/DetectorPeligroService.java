package com.gitlab.hear4me.phoneApp.model.service;

import com.gitlab.hear4me.compartido.model.domain.AudioWav;
import com.gitlab.hear4me.compartido.model.domain.Configuracion;
import com.gitlab.hear4me.compartido.model.domain.FiltroResultadoClasificacion;
import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.model.domain.ResultadoClasificacion;
import com.gitlab.hear4me.compartido.model.service.AlertaService;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;

import javax.inject.Inject;

public class DetectorPeligroService {

    private static long DURACION_GRABACION = 2500;

    private ConfiguracionesService configuracionesService;
    private AlertaService alertaService;
    private ClasificadorAudioService clasificadorAudioService;
    private GrabadoraService grabadoraService;

    private FiltroResultadoClasificacion filtroResultadoClasificacionModoCasa;
    private FiltroResultadoClasificacion filtroResultadoClasificacionModoCalle;

    @Inject
    public DetectorPeligroService(
            ConfiguracionesService configuracionesService,
            AlertaService alertaService,
            ClasificadorAudioService clasificadorAudioService,
            GrabadoraService grabadoraService,
            FiltroResultadoClasificacion filtroResultadoClasificacionModoCasa,
            FiltroResultadoClasificacion filtroResultadoClasificacionModoCalle) {
        this.configuracionesService = configuracionesService;
        this.alertaService = alertaService;
        this.clasificadorAudioService = clasificadorAudioService;
        this.grabadoraService = grabadoraService;
        this.filtroResultadoClasificacionModoCasa = filtroResultadoClasificacionModoCasa;
        this.filtroResultadoClasificacionModoCalle = filtroResultadoClasificacionModoCalle;
    }

    public void buscarPeligro() {
        AudioWav audioWav = grabadoraService.grabarAudio(DURACION_GRABACION);

        ModoConfiguracion modoConfiguracionActivo = configuracionesService.obtenerModoConfiguracionActivo();
        Configuracion configuracion = configuracionesService.obtenerConfiguracion(modoConfiguracionActivo);

        ResultadoClasificacion resultadoClasificacion =
                clasificadorAudioService.clasificarAudio(audioWav, modoConfiguracionActivo);

        FiltroResultadoClasificacion filtroResultadoClasificacion = modoConfiguracionActivo == ModoConfiguracion.CALLE
                ? filtroResultadoClasificacionModoCalle
                : filtroResultadoClasificacionModoCasa;

        filtroResultadoClasificacion.filtrarResultado(resultadoClasificacion, configuracion)
                .ifPresent((prediccion) -> alertaService.activarAlerta(prediccion));
    }
}
