package com.gitlab.hear4me.phoneApp.di.component;

import com.gitlab.hear4me.phoneApp.PhoneApp;
import com.gitlab.hear4me.phoneApp.android.DatalayerListenerService;
import com.gitlab.hear4me.phoneApp.android.MicrofonoIntentServiceAndroid;
import com.gitlab.hear4me.phoneApp.di.module.AndroidModule;
import com.gitlab.hear4me.phoneApp.di.module.AppModule;
import com.gitlab.hear4me.phoneApp.di.module.DatabaseModule;
import com.gitlab.hear4me.phoneApp.di.module.FileModule;
import com.gitlab.hear4me.phoneApp.di.module.ModelLoaderModule;
import com.gitlab.hear4me.phoneApp.di.module.PhonePresentersModule;
import com.gitlab.hear4me.phoneApp.di.module.PhoneRepositoriesModule;
import com.gitlab.hear4me.phoneApp.di.module.PhoneServicesModule;
import com.gitlab.hear4me.phoneApp.di.module.ReceiversModule;
import com.gitlab.hear4me.phoneApp.presentation.view.impl.AjustesActivity;
import com.gitlab.hear4me.phoneApp.presentation.view.impl.AlertaActivity;
import com.gitlab.hear4me.phoneApp.presentation.view.impl.HistorialActivity;
import com.gitlab.hear4me.phoneApp.presentation.view.impl.MainActivity;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        AppModule.class,
        FileModule.class,
        ModelLoaderModule.class,
        PhonePresentersModule.class,
        PhoneRepositoriesModule.class,
        PhoneServicesModule.class,
        AndroidModule.class,
        DatabaseModule.class,
        ReceiversModule.class})
public interface PhoneAppComponent {
    void inject(PhoneApp phoneApp);
    void inject(MainActivity mainActivity);
    void inject(AjustesActivity ajustesActivity);
    void inject(HistorialActivity historialActivity);
    void inject(AlertaActivity alertaActivity);
    void inject(MicrofonoIntentServiceAndroid microfonoIntentServiceAndroid);
    void inject(DatalayerListenerService wearableServiceAndroid);
}
