package com.gitlab.hear4me.phoneApp.presentation.presenter;

import com.gitlab.hear4me.compartido.presentation.events.ActivityEvents;
import com.gitlab.hear4me.phoneApp.presentation.events.AlertaEvents;

public interface AlertaPresenter extends AlertaEvents, ActivityEvents {
    void onWindowFocusChanged(boolean hasFocus);
}
