package com.gitlab.hear4me.phoneApp.presentation.events;

import android.view.View;

import com.gitlab.hear4me.compartido.model.domain.Alerta;

public interface AlertaEvents {
    void alertaActivada(Alerta alerta);
    void mostrarAlertaMasReciente(View view);
    void descartarAlertaActual(View view);
}
