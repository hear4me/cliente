package com.gitlab.hear4me.phoneApp.presentation.events;

import android.view.View;

public interface MainEvents {
    void irAAjustes(View view);
    void irAHistorial(View view);
    boolean activarMicrofono(View view);
}
