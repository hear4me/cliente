package com.gitlab.hear4me.phoneApp.util;

import com.gitlab.hear4me.compartido.model.domain.Categoria;
import com.gitlab.hear4me.compartido.model.domain.Prediccion;
import com.gitlab.hear4me.compartido.util.JsonUtil;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class JsonUtilTest {

    @Test
    public void fromJson_stringConPrediccion_retornaPrediccion() {
        Prediccion esperado = new Prediccion(Categoria.ALARMA_AUTO, 0.5f);
        String json = "{valor:0.5,categoria:ALARMA_AUTO}";
        Prediccion resultado = JsonUtil.fromJson(json, Prediccion.class);
        Assertions.assertThat(resultado).isEqualToComparingFieldByField(esperado);
    }

    @Test
    public void toJson_prediccionValida_retornaJson() {
        String esperado = "{\n  \"valor\": 0.5,\n  \"categoria\": \"ALARMA_AUTO\"\n}";
        Prediccion prediccion = new Prediccion(Categoria.ALARMA_AUTO, 0.5f);
        String json = JsonUtil.toJson(prediccion);
        Assertions.assertThat(json).isEqualTo(esperado);
    }

    @Test
    public void prettyPrint_jsonValido_retornaPrettyJson() {
        String esperado = "{\n  \"valor\": 0.5,\n  \"categoria\": \"ALARMA_AUTO\"\n}";
        String json = "{valor:0.5,categoria:ALARMA_AUTO}";
        String resultado = JsonUtil.prettyPrint(json);
        Assertions.assertThat(resultado).isEqualTo(esperado);
    }
}
