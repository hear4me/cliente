package com.gitlab.hear4me.phoneApp.model.repository;

import com.gitlab.hear4me.compartido.model.domain.Categoria;
import com.gitlab.hear4me.compartido.model.domain.Configuracion;
import com.gitlab.hear4me.compartido.model.domain.Configuraciones;
import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.model.repository.ConfiguracionesRepository;
import com.gitlab.hear4me.phoneApp.builder.ConfiguracionesBuilder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class ConfiguracionesRepositoryTest {

    private ConfiguracionesRepository configuracionesRepository;
    private File archivoTemporal;

    @BeforeEach
    public void beforeEach(@TempDir File archivoTemporal) {
        this.archivoTemporal = archivoTemporal;
    }

    @Test
    public void cargar_modoDefault_retornaConfiguracionModoDefault() throws IOException {
        archivoTemporal = new File(archivoTemporal, "cargar_modoDefault_retornaConfiguracionModoDefault");
        archivoTemporal.createNewFile();
        configuracionesRepository = new ConfiguracionesRepository(archivoTemporal);

        ModoConfiguracion modoConfiguracion = ModoConfiguracion.CASA;

        Configuraciones configuraciones = configuracionesRepository.cargar();
        Configuracion configuracion = configuraciones.getConfiguracion(modoConfiguracion);

        assertThat(configuracion.getModoConfiguracion())
                .isEqualTo(modoConfiguracion);
    }

    @Test
    public void cargar_modoDistintoAlDefault_retornaConfiguracionModoDistintoAlDefault() throws IOException {
        archivoTemporal = new File(archivoTemporal, "cargar_modoDistintoAlDefault_retornaConfiguracionModoDistintoAlDefault");
        archivoTemporal.createNewFile();
        configuracionesRepository = new ConfiguracionesRepository(archivoTemporal);

        ModoConfiguracion modoConfiguracion = ModoConfiguracion.CALLE;

        Configuraciones configuraciones = configuracionesRepository.cargar();
        Configuracion configuracion = configuraciones.getConfiguracion(modoConfiguracion);

        assertThat(configuracion.getModoConfiguracion())
                .isEqualTo(modoConfiguracion);
    }

    @Test
    public void guardar_configuracionesConCambios_cambiosGuardados() throws IOException {
        archivoTemporal = new File(archivoTemporal, "guardar_configuracionesDistintasAlDefault_restableceConfiguracionesAlDefault");
        archivoTemporal.createNewFile();
        configuracionesRepository = new ConfiguracionesRepository(archivoTemporal);

        Configuraciones configuraciones = new ConfiguracionesBuilder()
                .conModoCasa()
                .conAlarmaAutoDesactivada()
                .build();
        configuracionesRepository.guardar(configuraciones);

        Configuraciones configuracionesGuardadas = configuracionesRepository.cargar();

        ModoConfiguracion modoConfiguracion = ModoConfiguracion.CASA;
        Categoria categoria = Categoria.ALARMA_AUTO;
        Configuracion configuracionGuardada = configuracionesGuardadas.getConfiguracion(modoConfiguracion);
        assertThat(configuracionGuardada.getCategorias().get(categoria))
                .isFalse();

        Configuracion configuracionModificada = configuracionesGuardadas.getConfiguracion(modoConfiguracion);
        configuracionModificada.getCategorias().put(categoria, true);
        configuracionesRepository.guardar(configuracionesGuardadas);

        Configuracion configuracionModificadaGuardada = configuracionesRepository.cargar()
                .getConfiguracion(modoConfiguracion);

        assertThat(configuracionModificadaGuardada.getCategorias().get(categoria))
                .isTrue();
    }
}
