package com.gitlab.hear4me.phoneApp.model.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class ResultadoClasificacionTest {

    @Test
    public void getClase_tresClases_retornaMayorValor() {
        List<Prediccion> predicciones = new ArrayList<>();
        predicciones.add(new Prediccion(Categoria.BEBE_LLORANDO, 1f));
        predicciones.add(new Prediccion(Categoria.SIRENA_BOMBEROS, 0.2f));
        predicciones.add(new Prediccion(Categoria.PERRO, 0.1f));

        ResultadoClasificacion resultadoClasificacion = new ResultadoClasificacion(predicciones);
        Assertions.assertEquals(Categoria.BEBE_LLORANDO, resultadoClasificacion.getPrediccion(0).getCategoria());
    }

    @Test
    public void getClase_tresClasesDesordenadas_retornaMayorValor() {
        List<Prediccion> predicciones = new ArrayList<>();
        predicciones.add(new Prediccion(Categoria.SIRENA_BOMBEROS, 0.2f));
        predicciones.add(new Prediccion(Categoria.BEBE_LLORANDO, 1f));
        predicciones.add(new Prediccion(Categoria.PERRO, 0.1f));

        ResultadoClasificacion resultadoClasificacion = new ResultadoClasificacion(predicciones);
        Assertions.assertEquals(Categoria.BEBE_LLORANDO, resultadoClasificacion.getPrediccion(0).getCategoria());
    }
}
