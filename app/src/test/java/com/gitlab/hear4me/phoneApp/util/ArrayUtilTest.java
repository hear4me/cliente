package com.gitlab.hear4me.phoneApp.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import org.junit.jupiter.api.Test;

public class ArrayUtilTest {

    @Test
    public void remodelar_arrayValido_retornaArrayRemodelado() {
        float[][][] input = new float[][][] {
                {{3, 3}, {2, 2}},
                {{4, 4}, {1, 1}},
                {{5, 5}, {0, 0}}
        };
        float[][][][] esperado = new float[][][][] {
                {{{3}, {3}}, {{2}, {2}}},
                {{{4}, {4}}, {{1}, {1}}},
                {{{5}, {5}}, {{0}, {0}}}
        };
        float[][][][] resultado = ArrayUtil.remodelar(input);

        assertThat(resultado).isEqualTo(esperado);
    }

    @Test
    public void remodelar_arrayVacio_lanzaExcepcion() {
        float[][][] input = new float[][][] {{{}}};

        assertThatIllegalArgumentException()
                .isThrownBy(() -> ArrayUtil.remodelar(input))
                .withMessage("No se acepta un array vacio como parametro");
    }

    @Test
    public void remodelar_arrayNulo_lanzaExcepcion() {
        assertThatIllegalArgumentException()
                .isThrownBy(() -> ArrayUtil.remodelar(null))
                .withMessage("No se acepta nulo como parametro");
    }
}
