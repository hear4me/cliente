package com.gitlab.hear4me.phoneApp.model.service;

import com.gitlab.hear4me.compartido.model.repository.ConfiguracionesRepository;
import com.gitlab.hear4me.compartido.model.service.ConfiguracionesService;
import com.gitlab.hear4me.phoneApp.builder.ConfiguracionesBuilder;
import com.gitlab.hear4me.phoneApp.model.domain.Categoria;
import com.gitlab.hear4me.phoneApp.model.domain.Configuracion;
import com.gitlab.hear4me.phoneApp.model.domain.Configuraciones;
import com.gitlab.hear4me.phoneApp.model.domain.ModoConfiguracion;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;

public class ConfiguracionesServiceTest {

    private ConfiguracionesRepository configuracionesRepository;
    private ConfiguracionesService configuracionesService;

    @BeforeEach
    public void beforeEach(@TempDir File tempFile) throws IOException {
        tempFile = new File(tempFile, "buscarConfiguracion_modoDistintoAlDefault_retornaConfiguracionModoDistintoAlDefault");
        tempFile.createNewFile();
        configuracionesRepository = new ConfiguracionesRepository(tempFile);
        configuracionesService = new ConfiguracionesService(configuracionesRepository);
    }

    @Test
    public void restablecerConfiguraciones_configuracionDefault_sigueSiendoConfiguracionDefault() throws IOException {
        Configuraciones configuraciones = new ConfiguracionesBuilder()
                .conModoCasa()
                .conAlarmaAutoActivada()
                .build();
        configuracionesRepository.guardar(configuraciones);

        configuracionesService.restablecerConfiguraciones();

        ModoConfiguracion modoConfiguracion = ModoConfiguracion.CASA;
        Configuracion configuracion = configuracionesRepository.cargar()
                .getConfiguracion(modoConfiguracion);

        Categoria categoria = Categoria.ALARMA_AUTO;
        assertThat(configuracion.getCategorias().get(categoria))
                .isTrue();
    }

    @Test
    public void restablecerConfiguraciones_configuracionDistintaAlDefault_restableceConfiguracionAlDefault() throws IOException {
        Configuraciones configuraciones = new ConfiguracionesBuilder()
                .conModoCalle()
                .conAlarmaAutoDesactivada()
                .build();
        configuracionesRepository.guardar(configuraciones);

        configuracionesService.restablecerConfiguraciones();

        ModoConfiguracion modoConfiguracion = ModoConfiguracion.CALLE;
        Configuracion configuracion = configuracionesRepository.cargar()
                .getConfiguracion(modoConfiguracion);

        Categoria categoria = Categoria.ALARMA_AUTO;
        assertThat(configuracion.getCategorias().get(categoria))
                .isTrue();
    }
}