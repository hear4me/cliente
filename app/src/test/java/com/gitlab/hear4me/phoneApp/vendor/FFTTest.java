package com.gitlab.hear4me.phoneApp.vendor;

import org.jtransforms.fft.FloatFFT_1D;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FFTTest {

    @Test
    public void fft_inputValidoLongitudPar() {
        float[] data = { 0f, 0.1f, 0.2f, 0.3f };
        float[] esperado = { 0.6f, -0.2f, -0.2f, 0.2f };
        FloatFFT_1D fft = new FloatFFT_1D(data.length);
        fft.realForward(data); // data[1] es el ultimo real
        Assertions.assertArrayEquals(esperado, data, 0.01f);
    }

    @Test
    public void fft_inputValidoLongitudImpar() {
        float[] data = { 0f, 0.1f, 0.2f, 0.3f, 0.4f };
        float[] esperado = { 1, 0.08f, -0.25f, 0.34f, -0.25f };
        FloatFFT_1D fft = new FloatFFT_1D(data.length);
        fft.realForward(data); // data[1] es el ultimo imaginario
        Assertions.assertArrayEquals(esperado, data, 0.01f);
    }
}
