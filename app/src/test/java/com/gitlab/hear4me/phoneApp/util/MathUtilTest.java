package com.gitlab.hear4me.phoneApp.util;

import com.gitlab.hear4me.compartido.util.MathUtil;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class MathUtilTest {

    @Test
    public void multiplicacionElementos_inputValido_retornaMatriz() {
        float[][] esperado = { {0, 0.1f}, {0, 0.2f}, {0, 0.3f}, {0, 0.4f} };
        float[][] matriz = { {0, 0.1f}, {0.1f, 0.2f}, {0.2f, 0.3f}, {0.3f, 0.4f} };
        float[] vector = {0, 1};
        float[][] resultado = MathUtil.multiplicacionElementos(matriz, vector);

        for (int i = 0; i < esperado.length; ++i) {
            assertArrayEquals(esperado[i], resultado[i], 0.01f);
        }
    }

    @Test
    public void calcularFFT_inputValidoLongitudPar_retornaArray() {
        float[] data = { 0f, 0.1f, 0.2f, 0.3f };
        float[] esperado = { 0.6f, 0, -0.2f, 0.2f, -0.2f, 0 };
        float[] resultado = MathUtil.calcularFFT(data, data.length);

        assertArrayEquals(esperado, resultado, 0.01f);
    }

    @Test
    public void calcularFFT_inputValidoLongitudImpar_retornaArray() {
        float[] data = { 0f, 0.1f, 0.2f, 0.3f, 0.4f };
        float[] esperado = { 1, 0, -0.25f, 0.34f, -0.25f, 0.08f };
        float[] resultado = MathUtil.calcularFFT(data, data.length);

        assertArrayEquals(esperado, resultado, 0.01f);
    }

    @Test
    public void calcularAbsArrayComplejo_inputValido_retornaArray() {
        float[] esperado = { 0.22f, 0.5f };
        float[] complejos = { 0.1f, 0.2f, 0.3f, 0.4f };
        float[] resultado = MathUtil.calcularAbsArrayComplejo(complejos);

        assertArrayEquals(esperado, resultado, 0.01f);
    }

    @Test
    public void linspace_inputValidoEnteros_retornaArray() {
        float[] esperado = {2, 5, 8, 11};
        float[] resultado = MathUtil.linspace(2, 11, 4);

        assertArrayEquals(esperado, resultado, 0.01f);
    }

    @Test
    public void linspace_inputValidoFloats_retornaArray() {
        float[] esperado = {2, 2.25f, 2.5f, 2.75f, 3};
        float[] resultado = MathUtil.linspace(2, 3, 5);

        assertArrayEquals(esperado, resultado, 0.01f);
    }

    @Test
    public void linspaceArrays_inputValidoEnteros_retornaMatriz() {
        float[][] esperado = { {8000, 8500, 9000}, {12000, 12500, 13000}, {16000, 16500, 17000} };
        float[] a = {8000, 8500, 9000};
        float[] b = {16000, 16500, 17000};
        float[][] resultado = MathUtil.linspace(a, b, 3);

        for (int i = 0; i < esperado.length; ++i) {
            assertArrayEquals(esperado[i], resultado[i], 0.01f);
        }
    }

    @Test
    public void multiplicarMatrices_inputValido_retornaMatriz() {
        float[][] esperado = { {6,24}, {4,17}, {2,7} };
        float[][] m1 = { {2,4}, {1,3}, {1,1} };
        float[][] m2 = { {1,2}, {1,5} };
        float[][] resultado = MathUtil.multiplicarMatrices(m1, m2);

        for (int i = 0; i < esperado.length; ++i) {
            assertArrayEquals(esperado[i], resultado[i], 0.01f);
        }
    }

    @Test
    public void multiplicarMatrices_inputDimensionIncompatible_lanzaExcepcion() {
        float[][] esperado = { {6,24}, {4,17}, {2,7} };
        float[][] m1 = { {2,4,2}, {1,3,1} };
        float[][] m2 = { {1,2}, {1,5} };

        assertThatIllegalArgumentException()
                .isThrownBy(() -> MathUtil.multiplicarMatrices(m1, m2))
                .withMessage("La longitud de la segunda dimension del primer" +
                        " array debe ser igual a la primera dimension del" +
                        " segundo array.");
    }
}
