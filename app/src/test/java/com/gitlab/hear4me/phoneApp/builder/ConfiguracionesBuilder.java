package com.gitlab.hear4me.phoneApp.builder;

import com.gitlab.hear4me.compartido.model.domain.Categoria;
import com.gitlab.hear4me.compartido.model.domain.Configuracion;
import com.gitlab.hear4me.compartido.model.domain.Configuraciones;
import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.model.domain.ModoNotificacion;

public class ConfiguracionesBuilder {

    private Configuraciones configuraciones;

    public ConfiguracionesBuilder() {
        configuraciones = new Configuraciones();
    }

    public ConfiguracionesBuilder conModoCasa() {
        configuraciones.setModoConfiguracionSeleccionado(ModoConfiguracion.CASA);
        return this;
    }

    public ConfiguracionesBuilder conModoCalle() {
        configuraciones.setModoConfiguracionSeleccionado(ModoConfiguracion.CALLE);
        return this;
    }

    public ConfiguracionesBuilder conAlarmaAutoActivada() {
        Configuracion configuracion = configuraciones.getConfiguracion(
                configuraciones.getModoConfiguracionSeleccionado());
        configuracion.getCategorias().put(Categoria.ALARMA_AUTO, true);
        return this;
    }

    public ConfiguracionesBuilder conVibracionActivada() {
        Configuracion configuracion = configuraciones.getConfiguracion(
                configuraciones.getModoConfiguracionSeleccionado());
        configuracion.getModosNotificacion().put(ModoNotificacion.VIBRACION, true);
        return this;
    }

    public ConfiguracionesBuilder conAlarmaAutoDesactivada() {
        Configuracion configuracion = configuraciones.getConfiguracion(
                configuraciones.getModoConfiguracionSeleccionado());
        configuracion.getCategorias().put(Categoria.ALARMA_AUTO, false);
        return this;
    }

    public ConfiguracionesBuilder conVibracionDesactivada() {
        Configuracion configuracion = configuraciones.getConfiguracion(
                configuraciones.getModoConfiguracionSeleccionado());
        configuracion.getModosNotificacion().put(ModoNotificacion.VIBRACION, false);
        return this;
    }

    public Configuraciones build() {
        return configuraciones;
    }
}
