package com.gitlab.hear4me.phoneApp.util;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

public class DateUtilTest {

    @Test
    public void formatHora_horaValida_retornaHoraFormateada() {
        Date tiempo = new Date(2000 - 1900, 11, 1, 16, 0);
        String hora = DateUtil.formatHora(tiempo);

        assertThat(hora).isEqualTo("16:00");
    }

    @Test
    public void formatHora_fechaValida_retornaHoraFormateada() {
        Date tiempo = new Date(2000 - 1900, 11, 1, 16, 0);
        String hora = DateUtil.formatFecha(tiempo);

        assertThat(hora).isEqualTo("01/12/2000");
    }
}
