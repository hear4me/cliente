package com.gitlab.hear4me.phoneApp.util;

import com.gitlab.hear4me.compartido.util.MelFeaturesUtil;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MelFeaturesUtilTest {

    @Test
    public void wavFileToExamples_inputValido_retornaAudioPreprocesado() {

    }

    @Test
    public void waveFormToExamples_inputValido_retornaAudioPreprocesado() {

    }

    @Test
    public void logMelSpectrogram_inputValido_retornaMelSpectrogram() {

    }

    @Test
    public void stftMagnitude_inputValido_retornaStftMagnitude() {
        float[] data = {0f, 0.1f, 0.2f, 0.3f, 0.4f};
        float[][] esperado = { {0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f}, {0.2f, 0.2f, 0.2f, 0.2f, 0.2f, 0.2f},
                {0.3f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f}, {0.4f, 0.4f, 0.4f, 0.4f, 0.4f, 0.4f}};
        float[][] real = MelFeaturesUtil.stftMagnitude(data, 10, 1, 2);
        Assertions.assertArrayEquals(esperado, real);
    }

    @Test
    public void spectrogramToMelMatrix_inputValido_retornaMatriz() {
        float[][] resultado =
                MelFeaturesUtil.spectrogramToMelMatrix(10, 10, 16000, 0, 7500);
        throw new UnsupportedOperationException();
    }

    @Test
    public void hertzToMel_inputValido_retornaMelScale() {
        float[] esperado = { 2840.03f, 2903.01f, 2962.65f };
        float[] input = { 8000, 8500, 9000 };
        float[] real = MelFeaturesUtil.hertzToMel(input);
        Assertions.assertArrayEquals(esperado, real, 0.01f);
    }

    @Test
    public void frame_inputValidoArray_retornaMatriz_1() {
        float[] data = {0f, 0.1f, 0.2f, 0.3f, 0.4f};
        float[][] esperado = new float[][] { {0f, 0.1f}, {0.1f,0.2f}, {0.2f, 0.3f}, {0.3f, 0.4f} };
        float[][] real = MelFeaturesUtil.frame(data, 1, 2);
        Assertions.assertArrayEquals(esperado, real);
    }

    @Test
    public void frame_inputValidoArray_retornaMatriz_2() {
        float[] data = {0f, 0.1f, 0.2f, 0.3f, 0.4f};
        float[][] esperado = new float[][] { {0f, 0.1f, 0.2f}, {0.2f,0.3f, 0.4f} };
        float[][] real = MelFeaturesUtil.frame(data, 2, 3);
        Assertions.assertArrayEquals(esperado, real);
    }

    @Test
    public void frame_inputValidoMatriz_retornaMatriz() {
        float[][] data = { {1,2}, {3,4}, {5,6} };
        float[][][] esperado = { { {1,2}, {3,4} }, { {3,4}, {5,6} } };
        float[][][] real = MelFeaturesUtil.frame(data, 1, 2);
        Assertions.assertArrayEquals(esperado, real);
    }

    @Test
    public void periodicHann_inputValido_retornaPeriodicHann() {
        float[] esperado = new float[] {0, 0.095f, 0.345f, 0.654f, 0.904f, 1, 0.904f, 0.645f, 0.345f, 0.095f};
        float[] real = MelFeaturesUtil.periodicHann(10);
        Assertions.assertArrayEquals(esperado, real, 0.01f);
    }
}
