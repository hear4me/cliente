# Hear4Me
Aplicación de celular que ayuda a personas con discapacidad auditiva a detectar sonidos que pueden representar un peligro para el usuario.

## Requisitos
Al descargar el proyecto con git va a hacer falta luego descargar un archivo .tflite que contiene los pesos de la red neuronal que se encarga de detectar los sonidos que representan un peligro para el usuario. Sin este archivo la aplicación no puede funcionar.

Este archivo .tflite es temporal y solo sirve como placeholder. Se puede descargar desde [acá](https://drive.google.com/file/d/1o8IyZyA6SA9aMhoEuIpVmlEE9DFJ1usf/view?usp=sharing "acá").

Se debe crear una nueva carpeta con nombre "assets" dentro del directorio `app/src/main` del proyecto y colocar el archivo .tflite descargado ahí.