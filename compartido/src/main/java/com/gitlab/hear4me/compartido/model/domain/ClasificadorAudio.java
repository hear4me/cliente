package com.gitlab.hear4me.compartido.model.domain;

import org.tensorflow.lite.Interpreter;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class ClasificadorAudio {

    private final int numEtiquetas;
    private final ModoConfiguracion modoConfiguracion;

    private float[][] etiquetaProbArr;
    private Interpreter tflite;
    private Interpreter.Options tfliteOptions;
    private MappedByteBuffer tfliteModel;
    private ModelLoader modelLoader;

    public ClasificadorAudio(ModelLoader modelLoader, ModoConfiguracion modoConfiguracion, int numEtiquetas) {
        this.modelLoader = modelLoader;
        this.numEtiquetas = numEtiquetas;
        tfliteOptions = new Interpreter.Options();
        this.modoConfiguracion = modoConfiguracion;
    }

    /**
     * Estima una probabilidad para cada clase
     *
     * @param audio Audio preprocesado
     * @return Una Clasificacion con la probabilidad de cada clase
     */
    public ResultadoClasificacion clasificar(AudioWav audio) {
        try {
            iniciar();
            float[][][][] input = audio.preProcesar();
            etiquetaProbArr = new float[input.length][numEtiquetas];
            tflite.run(input, etiquetaProbArr);
            float[] promedios = promediarProbs(etiquetaProbArr);
            return arrProbsToResultadoClasificacion(promedios);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            cerrar();
        }
        return null;
    }

    private void iniciar() throws IOException {
        if (tflite != null) return;
        tfliteModel = modelLoader.cargarModelo();
        tflite = new Interpreter(tfliteModel, tfliteOptions);
    }

    private void cerrar() {
        if (tflite == null) return;
        tflite.close();
        tflite = null;
        tfliteModel = null;
    }

    private float[] promediarProbs(float[][] etiquetaProbArr) {
        float[] resultado = new float[etiquetaProbArr[0].length];
        for (int i = 0; i < etiquetaProbArr[0].length; ++i) {
            for (float[] etiquetaProb : etiquetaProbArr) {
                resultado[i] += etiquetaProb[i];
            }
            resultado[i] /= etiquetaProbArr.length;
        }
        return resultado;
    }

    private ResultadoClasificacion arrProbsToResultadoClasificacion(float[] prediccionesArr) {
        List<Prediccion> predicciones = new ArrayList<>();

        Categoria[] categorias = modoConfiguracion == ModoConfiguracion.CALLE
                ? Categoria.deModoCalle()
                : Categoria.deModoCasa();
        for (int i = 0; i < numEtiquetas; ++i) {
            predicciones.add(new Prediccion(categorias[i], prediccionesArr[i]));
        }

        return new ResultadoClasificacion(predicciones);
    }
}