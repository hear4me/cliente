package com.gitlab.hear4me.compartido.model.domain;

public enum Dispositivo {
    CELULAR, RELOJ
}
