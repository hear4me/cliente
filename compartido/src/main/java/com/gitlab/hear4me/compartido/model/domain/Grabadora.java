package com.gitlab.hear4me.compartido.model.domain;

import android.media.AudioFormat;
import android.media.MediaRecorder;
import android.os.Environment;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.IOException;

import omrecorder.AudioRecordConfig;
import omrecorder.OmRecorder;
import omrecorder.PullTransport;
import omrecorder.PullableSource;
import omrecorder.Recorder;

public class Grabadora {

    public static final String ARCHIVO_TEMP = "temp.wav";

    private Recorder recorder;

    public Grabadora() {
        recorder = OmRecorder.wav(
                new PullTransport.Default(mic()), file());
    }

    public void grabar(long milisegundos) {
        recorder.startRecording();
        try {
            Thread.sleep(milisegundos);
            recorder.stopRecording();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private PullableSource mic() {
        return new PullableSource.Default(
                new AudioRecordConfig.Default(
                        MediaRecorder.AudioSource.MIC, AudioFormat.ENCODING_PCM_16BIT,
                        AudioFormat.CHANNEL_IN_MONO, 16000
                )
        );
    }

    @NonNull
    private File file() {
        return new File(Environment.getExternalStorageDirectory(), ARCHIVO_TEMP);
    }
}
