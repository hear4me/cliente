package com.gitlab.hear4me.compartido.presentation.events;

import com.gitlab.hear4me.compartido.presentation.view.ActivityView;

public interface ActivityEvents {
    void onCreate(ActivityView activityView);
    void onStart(ActivityView activityView);
    void onResume(ActivityView activityView);
    void onPause();
    void onStop();
    void onDestroy();
}
