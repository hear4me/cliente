package com.gitlab.hear4me.compartido.model.repository;

import com.gitlab.hear4me.compartido.model.domain.Configuraciones;

public interface ConfiguracionesRepository {
    Configuraciones cargar();
    void guardar(Configuraciones configuraciones);
}
