package com.gitlab.hear4me.compartido.model.domain;

import java.io.IOException;
import java.nio.MappedByteBuffer;

public abstract class ModelLoader {

    private String modelPath;

    public ModelLoader(String modelPath) {
        this.modelPath = modelPath;
    }

    public abstract MappedByteBuffer cargarModelo() throws IOException;

    public String getModelPath() {
        return modelPath;
    }
}
