package com.gitlab.hear4me.compartido.model.domain;

import com.google.common.collect.ImmutableMap;

import static com.gitlab.hear4me.compartido.model.domain.Categoria.ALARMA_AUTO;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.ALARMA_INCENDIO;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.BEBE_LLORANDO;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.DISPAROS;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.GATO;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.PERRO;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.SIRENA_AMBULANCIA;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.SIRENA_BOMBEROS;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.SIRENA_POLICIA;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.VIDRIO_ROTO;

class FiltroResultadoClasificacionModoCasa extends FiltroResultadoClasificacion {

    FiltroResultadoClasificacionModoCasa() {
        super();

        for (Categoria categoria : Categoria.deModoCasa()) {
            repeticiones.put(categoria, 0);
        }

        limitesRepeticiones = ImmutableMap.<Categoria, Integer>builder()
                .put(ALARMA_AUTO, 10)
                .put(ALARMA_INCENDIO, 10)
                .put(PERRO, 3)
                .put(SIRENA_AMBULANCIA, 10)
                .put(BEBE_LLORANDO, 5)
                .put(SIRENA_BOMBEROS, 10)
                .put(DISPAROS, 2)
                .put(GATO, 3)
                .put(SIRENA_POLICIA, 10)
                .put(VIDRIO_ROTO, 1)
                .build();

        repeticionesMinimas = ImmutableMap.<Categoria, Integer>builder()
                .put(ALARMA_AUTO, 0)
                .put(ALARMA_INCENDIO, 0)
                .put(PERRO, 0)
                .put(SIRENA_AMBULANCIA, 0)
                .put(BEBE_LLORANDO, 1)
                .put(SIRENA_BOMBEROS, 0)
                .put(DISPAROS, 0)
                .put(GATO, 0)
                .put(SIRENA_POLICIA, 0)
                .put(VIDRIO_ROTO, 1)
                .build();

        umbralesRepeticiones = ImmutableMap.<Categoria, Float>builder()
                .put(ALARMA_AUTO, 0.75f)
                .put(ALARMA_INCENDIO, 0.75f)
                .put(PERRO, 0.75f)
                .put(SIRENA_AMBULANCIA, 0.75f)
                .put(BEBE_LLORANDO, 0.85f)
                .put(SIRENA_BOMBEROS, 0.75f)
                .put(DISPAROS, 0.96f)
                .put(GATO, 0.75f)
                .put(SIRENA_POLICIA, 0.75f)
                .put(VIDRIO_ROTO, 0.96f)
                .build();

        multiplicadoresRepeticion = ImmutableMap.<Categoria, Float>builder()
                .put(ALARMA_AUTO, 0.15f)
                .put(ALARMA_INCENDIO, 0.05f)
                .put(PERRO, 0.15f)
                .put(SIRENA_AMBULANCIA, 0.05f)
                .put(BEBE_LLORANDO, 0.02f)
                .put(SIRENA_BOMBEROS, 0.15f)
                .put(DISPAROS, 0.02f)
                .put(GATO, 0.15f)
                .put(SIRENA_POLICIA, 0.15f)
                .put(VIDRIO_ROTO, 0.02f)
                .build();

        umbralesPorCategoria = ImmutableMap.<Categoria, Float>builder()
                .put(ALARMA_AUTO, 0.8f)
                .put(ALARMA_INCENDIO, 0.96f)
                .put(PERRO, 0.96f)
                .put(SIRENA_AMBULANCIA, 0.94f)
                .put(BEBE_LLORANDO, 0.985f)
                .put(SIRENA_BOMBEROS, 0.89f)
                .put(DISPAROS, 0.995f)
                .put(GATO, 0.96f)
                .put(SIRENA_POLICIA, 0.89f)
                .put(VIDRIO_ROTO, 0.996f)
                .build();
    }
}
