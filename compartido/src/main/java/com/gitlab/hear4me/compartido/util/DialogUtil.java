package com.gitlab.hear4me.compartido.util;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;

public class DialogUtil {

    public static void mostrar(Context contexto,
                               String titulo,
                               String mensaje,
                               String textoConfirmacion,
                               Runnable accionAlConfirmar) {
        AlertDialog.Builder builder = new AlertDialog.Builder(contexto);

        builder.setCancelable(true);
        builder.setTitle(titulo);
        builder.setMessage(mensaje);

        builder.setPositiveButton(textoConfirmacion, (dialogInterface, i) ->
                accionAlConfirmar.run());

        builder.setNegativeButton("Cancelar",
                (dialogInterface, i) -> dialogInterface.cancel());

        builder.show();
    }

}
