package com.gitlab.hear4me.compartido.model.domain;


import com.gitlab.hear4me.compartido.util.ArrayUtil;
import com.gitlab.hear4me.compartido.util.MelFeaturesUtil;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * Audio WAV que incluye el encabezado
 */
public class AudioWav {

    private static final int SAMPLE_RATE = 16000;
    private static final float EXAMPLE_WINDOW_SECONDS = 0.96f;
    private static final float EXAMPLE_HOP_SECONDS = 0.96f;
    private static final float LOG_OFFSET = 0.01f;
    private static final float STFT_WINDOW_LENGTH_SECONDS = 0.025f;
    private static final float STFT_HOP_LENGTH_SECONDS = 0.010f;
    private static final int NUM_MEL_BINS = 64;
    private static final int MEL_MIN_HZ = 125;
    private static final int MEL_MAX_HZ = 7500;

    private byte[] data;

    public AudioWav(byte[] bytes) {
        setData(bytes);
    }

    public AudioWav(File archivo) throws IOException {
        setData(FileUtils.readFileToByteArray(archivo));
    }

    public final void setData(byte[] bytes) {
        data = bytes;
    }

    /**
     * @return bytes del audio sin el header o null si no es valido
     */
    public byte[] getData() {
        final int posDataHeader = 36;

        byte[] bytes = data;
        ByteBuffer bf = ByteBuffer.wrap(bytes);

        String str = new String(Arrays.copyOfRange(bytes, posDataHeader, posDataHeader + 4));
        if (str.equals("data")) {
            bf.position(posDataHeader + 4);
            bf.order(ByteOrder.LITTLE_ENDIAN);
            int rawDataSize = bf.getInt();
            byte[] rawData = Arrays.copyOfRange(bytes, posDataHeader + 8, posDataHeader + 8 + rawDataSize);
            return rawData;
        } else {
            return null;
        }
    }

    public float[][][][] preProcesar() {
        float[][][] resultado = MelFeaturesUtil.wavFileToExamples(this,
                EXAMPLE_WINDOW_SECONDS,
                EXAMPLE_HOP_SECONDS,
                SAMPLE_RATE,
                LOG_OFFSET,
                STFT_WINDOW_LENGTH_SECONDS,
                STFT_HOP_LENGTH_SECONDS,
                NUM_MEL_BINS,
                MEL_MIN_HZ,
                MEL_MAX_HZ
        );

        return ArrayUtil.remodelar(resultado);
    }
}
