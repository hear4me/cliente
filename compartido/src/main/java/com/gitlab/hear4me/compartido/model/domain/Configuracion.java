package com.gitlab.hear4me.compartido.model.domain;

import java.util.HashMap;
import java.util.Map;

/** Configuración con las opciones establecidas por el usuario */
public class Configuracion {

    private ModoConfiguracion modoConfiguracion;
    private Map<Categoria, Boolean> categorias;
    private Map<ModoNotificacion, Boolean> modosNotificacion;

    public Configuracion(ModoConfiguracion modoConfiguracion) {
        this.modoConfiguracion = modoConfiguracion;
        categorias = new HashMap<>();
        modosNotificacion = new HashMap<>();
    }

    public ModoConfiguracion getModoConfiguracion() {
        return modoConfiguracion;
    }

    public Map<Categoria, Boolean> getCategorias() {
        return categorias;
    }

    public Map<ModoNotificacion, Boolean> getModosNotificacion() {
        return modosNotificacion;
    }

    void inicializar() {
        Categoria[] categorias = modoConfiguracion == ModoConfiguracion.CALLE
                ? Categoria.deModoCalle()
                : Categoria.deModoCasa();
        for (Categoria categoria : categorias) {
            this.categorias.put(categoria, true);
        }
        for (ModoNotificacion modoNotificacion: ModoNotificacion.values()) {
            modosNotificacion.put(modoNotificacion, true);
        }
    }
}
