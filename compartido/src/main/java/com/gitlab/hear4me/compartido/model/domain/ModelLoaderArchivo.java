package com.gitlab.hear4me.compartido.model.domain;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class ModelLoaderArchivo extends ModelLoader {

    public ModelLoaderArchivo(String modelPath) {
        super(modelPath);
    }

    @Override
    public MappedByteBuffer cargarModelo() throws IOException {
        InputStream is = getClass().getResourceAsStream(getModelPath());
        File temp = File.createTempFile("audio", "wav");
        FileUtils.copyInputStreamToFile(is, temp);
        FileChannel fileChannel = new FileInputStream(temp).getChannel();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
    }
}
