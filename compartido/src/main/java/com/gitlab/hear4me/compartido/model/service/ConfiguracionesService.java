package com.gitlab.hear4me.compartido.model.service;

import com.gitlab.hear4me.compartido.model.domain.Configuracion;
import com.gitlab.hear4me.compartido.model.domain.Configuraciones;
import com.gitlab.hear4me.compartido.model.domain.Dispositivo;
import com.gitlab.hear4me.compartido.model.domain.ModoConfiguracion;
import com.gitlab.hear4me.compartido.model.domain.RedModoCasa;
import com.gitlab.hear4me.compartido.model.repository.ConfiguracionesRepository;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

public class ConfiguracionesService {

    private ConfiguracionesRepository configuracionesRepository;

    @Inject
    public ConfiguracionesService(ConfiguracionesRepository configuracionesRepository) {
        this.configuracionesRepository = configuracionesRepository;
    }

    public Configuracion obtenerConfiguracion(ModoConfiguracion modoConfiguracion) {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        Configuracion configuracion = configuraciones.getConfiguracion(modoConfiguracion);
        return configuracion;
    }

    public void actualizarConfiguracion(ModoConfiguracion modoConfiguracion, Configuracion configuracion) {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        configuraciones.setConfiguracion(modoConfiguracion, configuracion);
        configuracionesRepository.guardar(configuraciones);
    }

    public void restablecerConfiguraciones() {
        Configuraciones nuevasConfiguraciones = new Configuraciones();
        configuracionesRepository.guardar(nuevasConfiguraciones);
    }

    public ModoConfiguracion obtenerModoConfiguracionActivo() {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        return configuraciones.getModoConfiguracionSeleccionado();
    }

    public void cambiarModoConfiguracion(ModoConfiguracion modoConfiguracion) {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        configuraciones.setModoConfiguracionSeleccionado(modoConfiguracion);
        configuracionesRepository.guardar(configuraciones);
    }

    public void actualizarModoConfiguracion(ModoConfiguracion modoConfiguracion) {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        configuraciones.setModoConfiguracionSeleccionado(modoConfiguracion);
    }

    public Configuraciones obtenerConfiguraciones() {
        return configuracionesRepository.cargar();
    }

    public boolean isModoCasaAutomaticoActivado() {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        return configuraciones.isModoCasaAutomaticoActivado();
    }

    public void setModoCasaAutomaticoActivado(boolean nuevoEstado) {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        configuraciones.setModoCasaAutomaticoActivado(nuevoEstado);
        configuracionesRepository.guardar(configuraciones);
    }

    public boolean isModoCasaAutomaticoConfigurado() {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        return configuraciones.isModoCasaAutomaticoConfigurado();
    }

    public void setModoCasaAutomaticoConfigurado(boolean nuevoEstado) {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        configuraciones.setModoCasaAutomaticoConfigurado(nuevoEstado);
        configuracionesRepository.guardar(configuraciones);
    }

    public boolean isModoDormirAutomaticoConfigurado() {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        return configuraciones.isModoDormirAutomaticoConfigurado();
    }

    public void setModoDormirAutomaticoConfigurado(boolean nuevoEstado) {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        configuraciones.setModoDormirAutomaticoConfigurado(nuevoEstado);
        configuracionesRepository.guardar(configuraciones);
    }

    public void setModoDormirAutomaticoActivado(boolean nuevoEstado) {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        configuraciones.setModoDormirAutomaticoActivado(nuevoEstado);
        configuracionesRepository.guardar(configuraciones);
    }

    public void activarModoAutomaticoParaRed(String bssid, String ssid) {
        if (bssid == null) return;

        Configuraciones configuraciones = configuracionesRepository.cargar();
        List<RedModoCasa> redesModoCasa = configuraciones.getRedesModoCasa();

        Optional<RedModoCasa> redModoCasa = redesModoCasa.stream()
                .filter((red) -> red.getId().equals(bssid))
                .findFirst();
        if (redModoCasa.isPresent()) {
            redesModoCasa.remove(redModoCasa.get());
        } else {
            redesModoCasa.add(new RedModoCasa(bssid, ssid));
        }

        configuracionesRepository.guardar(configuraciones);
    }

    public long obtenerHoraDesde() {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        return configuraciones.getHoraDesde();
    }

    public long obtenerHoraHasta() {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        return configuraciones.getHoraHasta();
    }

    public void setHoraDesde(long hora) {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        configuraciones.setHoraDesde(hora);
        configuracionesRepository.guardar(configuraciones);
    }

    public void setHoraHasta(long hora) {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        configuraciones.setHoraHasta(hora);
        configuracionesRepository.guardar(configuraciones);
    }

    public boolean isDentroDeHorarioModoDormirAutomatico() {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        long horaDesde = configuraciones.getHoraDesde();
        long horaHasta = configuraciones.getHoraHasta();
        long horaActual = System.currentTimeMillis();

        return horaDesde < horaActual && horaHasta > horaActual;
    }

    public void setDispositivo(Dispositivo dispositivo) {
        Configuraciones configuraciones = configuracionesRepository.cargar();
        configuraciones.setDispositivo(dispositivo);
        configuracionesRepository.guardar(configuraciones);
    }
}
