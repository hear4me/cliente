package com.gitlab.hear4me.compartido.model.domain;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public abstract class FiltroResultadoClasificacion {

    protected Map<Categoria, Integer> repeticiones;
    protected Map<Categoria, Integer> limitesRepeticiones;
    protected Map<Categoria, Integer> repeticionesMinimas;
    protected Map<Categoria, Float> umbralesPorCategoria;
    protected Map<Categoria, Float> umbralesRepeticiones;
    protected Map<Categoria, Float> multiplicadoresRepeticion;

    private Prediccion ultimaAlerta;
    private static final double PRODUCTO_REDUCTOR_REPETICION = 0.845;

    public FiltroResultadoClasificacion() {
        repeticiones = new HashMap<>();
        limitesRepeticiones = new HashMap<>();
    }

    public static FiltroResultadoClasificacionModoCasa modoCasa() {
        return new FiltroResultadoClasificacionModoCasa();
    }

    public static FiltroResultadoClasificacionModoCalle modoCalle() {
        return new FiltroResultadoClasificacionModoCalle();
    }

    public final Optional<Prediccion> filtrarResultado(ResultadoClasificacion resultadoClasificacion, Configuracion configuracion) {
        Optional<Prediccion> resultado = resultadoClasificacion.getPredicciones().stream()
                .filter(prediccion -> filtrarCategoriasDesactivadas(prediccion, configuracion))
                .map(this::buscarLosQueSuperanUmbralDeRepeticion)
                .filter(this::filtrarMayorIgualAMinimoRepeticiones)
                .filter(this::filtrarMayorAUmbral)
                .max(Comparator.comparingDouble(Prediccion::getValor));
        resultado.ifPresent((prediccion) -> {
            ultimaAlerta = prediccion;
            repeticiones.put(prediccion.getCategoria(), 0);
        });
        return resultado;
    }

    private boolean filtrarCategoriasDesactivadas(Prediccion prediccion, Configuracion configuracion) {
        Map<Categoria, Boolean> categorias = configuracion.getCategorias();
        return categorias.get(prediccion.getCategoria());
    }

    private Prediccion buscarLosQueSuperanUmbralDeRepeticion(Prediccion prediccion) {
        Categoria categoria = prediccion.getCategoria();
        if (prediccion.getValor() >=
                umbralesRepeticiones.get(categoria) * Math.pow(PRODUCTO_REDUCTOR_REPETICION, repeticiones.get(categoria))) {
            repeticiones.put(categoria, repeticiones.get(categoria) + 1);
            if (repeticiones.get(categoria) > limitesRepeticiones.get(categoria)) {
                repeticiones.put(categoria, 0);
            }
        } else if (repeticiones.get(categoria) > 0 && !unicaCategoriaConRepeticiones(categoria)) {
            repeticiones.put(categoria, repeticiones.get(categoria) - 1);
        }
        return prediccion;
    }

    private boolean unicaCategoriaConRepeticiones(Categoria categoria) {
        for (Categoria otraCategoria : repeticiones.keySet()) {
            if (categoria != otraCategoria && repeticiones.get(otraCategoria) > 0) {
                return false;
            }
        }
        return true;
    }

    private boolean filtrarMayorIgualAMinimoRepeticiones(Prediccion prediccion) {
        return repeticiones.get(prediccion.getCategoria()) > repeticionesMinimas.get(prediccion.getCategoria());
    }

    private boolean filtrarMayorAUmbral(Prediccion prediccion) {
        float valorCalculado = prediccion.getValor()
                + repeticiones.get(prediccion.getCategoria()) * multiplicadoresRepeticion.get(prediccion.getCategoria());
        return valorCalculado >= umbralesPorCategoria.get(prediccion.getCategoria());
    }

}
