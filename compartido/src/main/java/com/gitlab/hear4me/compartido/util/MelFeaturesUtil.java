package com.gitlab.hear4me.compartido.util;

import com.gitlab.hear4me.compartido.model.domain.AudioWav;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Traduccion - con variaciones - a java de https://github.com/tensorflow/models/blob/master/research/audioset/vggish/mel_features.py
 */
public class MelFeaturesUtil {

    private MelFeaturesUtil() {}

    private static final float MEL_BREAK_FREQUENCY_HERTZ = 700;
    private static final float MEL_HIGH_FREQUENCY_Q = 1127;

    public static float[][][] wavFileToExamples(
            AudioWav audio, float exampleWindowSecs, float exampleHopSecs,
            int sampleRate, float logOffset, float windowLengthSecs, float hopLengthSecs, int numMelBins,
            int lowerEdgeHertz, int upperEdgeHertz) {
        byte[] bytes = audio.getData();

        short[] shorts = new short[bytes.length / 2];
        float[] floats = new float[shorts.length];

        ByteBuffer temp = ByteBuffer.wrap(bytes);
        temp.order(ByteOrder.nativeOrder()).asShortBuffer().get(shorts);

        for (int i = 0; i < floats.length; ++i) {
            floats[i] = shorts[i] / 32768f;
        }

        return waveFormToExamples(floats, exampleWindowSecs, exampleHopSecs, sampleRate, logOffset, windowLengthSecs, hopLengthSecs, numMelBins, lowerEdgeHertz, upperEdgeHertz);
    }

    /**
     * Devuelve un array de ejemplos que son input para la red
     * @param data sonido mono, cada elemento en el rango [-1.0 ,+1.0]
     * @param sampleRate
     */
    public static float[][][] waveFormToExamples(
            float[] data, float exampleWindowSecs, float exampleHopSecs,
            int sampleRate, float logOffset, float windowLengthSecs, float hopLengthSecs, int numMelBins,
            int lowerEdgeHertz, int upperEdgeHertz) {
        float[][] logMel = logMelSpectrogram(data, sampleRate, logOffset, windowLengthSecs, hopLengthSecs, numMelBins, lowerEdgeHertz, upperEdgeHertz);

        float featuresSampleRate = 1 / hopLengthSecs;
        int exampleWindowLength = Math.round(exampleWindowSecs * featuresSampleRate);
        int exampleHopLength = Math.round(exampleHopSecs * featuresSampleRate);

        float[][][] logMelExamples = frame(logMel, exampleWindowLength, exampleHopLength);

        return logMelExamples;
    }

    public static float[][] logMelSpectrogram(
            float[] data, int sampleRate, float logOffset, float windowLengthSecs, float hopLengthSecs, int numMelBins, int lowerEdgeHertz, int upperEdgeHertz) {
        int windowLengthSamples = (int) Math.floor(sampleRate * windowLengthSecs);
        int hopLengthSamples = (int) Math.floor(sampleRate * hopLengthSecs);
        int fftLength = (int) Math.pow(2, Math.ceil(log2(windowLengthSamples)));

        float[][] spectrogram = MelFeaturesUtil.stftMagnitude(data, fftLength, hopLengthSamples, windowLengthSamples);
        float[][] melSpectrogram = MathUtil.multiplicarMatrices(spectrogram, spectrogramToMelMatrix(
                numMelBins, spectrogram[0].length, sampleRate, lowerEdgeHertz, upperEdgeHertz));

        for (int i = 0; i < melSpectrogram.length; ++i) {
            for (int j = 0; j < melSpectrogram[0].length; ++j) {
                melSpectrogram[i][j] = (float) Math.log(melSpectrogram[i][j] + logOffset);
            }
        }

        return melSpectrogram;
    }

    public static float[][] stftMagnitude(float[] data, int fftlength, int hopLength, int windowLength) {
        final float[][] frames = frame(data, hopLength, windowLength);
        final float[] window = periodicHann(windowLength);
        final float[][] windowedFrames = MathUtil.multiplicacionElementos(frames, window);

        float[][] resultado = new float[frames.length][fftlength + (fftlength + 1) % 2 + 1];
        for (int i = 0; i < frames.length; ++i) {
            float[] complejos = MathUtil.calcularFFT(windowedFrames[i], fftlength);
            resultado[i] = MathUtil.calcularAbsArrayComplejo(complejos);
        }

        return resultado;
    }

    public static float[][] spectrogramToMelMatrix(
            int numMelBins, int numSpectrogramBins, int sampleRate, int lowerEdgeHertz, int upperEdgeHertz) {
        int nyquistHertz = sampleRate / 2;
        float[] spectrogramBinsHertz = MathUtil.linspace(0, nyquistHertz, numSpectrogramBins);
        float[] spectrogramBinsMel = hertzToMel(spectrogramBinsHertz);

        float[] bandEdgesMel = MathUtil.linspace(
                hertzToMel(lowerEdgeHertz), hertzToMel(upperEdgeHertz), numMelBins + 2);

        float[][] melWeightsMatrix = new float[numSpectrogramBins][numMelBins];

        float[] lowerSlope = new float[numSpectrogramBins];
        float[] upperSlope = new float[numSpectrogramBins];

        for (int i = 0; i < numMelBins; ++i) {
            float lowerEdgeMel = bandEdgesMel[i];
            float centerMel = bandEdgesMel[i+1];
            float upperEdgeMel = bandEdgesMel[i+2];
            for (int j = 0; j < numSpectrogramBins; ++j) {
                lowerSlope[j] = (spectrogramBinsMel[j] - lowerEdgeMel) / (centerMel - lowerEdgeMel);
                upperSlope[j] = (upperEdgeMel - spectrogramBinsMel[j]) / (upperEdgeMel - centerMel);
            }
            for (int j = 0; j < numSpectrogramBins; ++j) {
                melWeightsMatrix[j][i] = Math.max(0, Math.min(lowerSlope[j], upperSlope[j]));
            }
        }

        for (int j = 0; j < numMelBins; ++j) {
            melWeightsMatrix[0][j] = 0;
        }

        return melWeightsMatrix;
    }

    public static float hertzToMel(int frequenciesHertz) {
        return MEL_HIGH_FREQUENCY_Q * (float) Math.log(1 + frequenciesHertz / MEL_BREAK_FREQUENCY_HERTZ);
    }

    public static float[] hertzToMel(float[] frequenciesHertz) {
        float[] resultado = new float[frequenciesHertz.length];
        for (int i = 0; i < resultado.length; ++i) {
            resultado[i] = MEL_HIGH_FREQUENCY_Q * (float) Math.log(1 + frequenciesHertz[i] / MEL_BREAK_FREQUENCY_HERTZ);
        }
        return resultado;
    }

    public static float[][] frame(float[] data, int hopLength,  int windowLength) {
        final int numSamples = data.length;
        final int numFrames = 1 + (int) Math.floor((numSamples - windowLength) / hopLength);

        float[][] resultado = new float[numFrames][windowLength];

        for (int i = 0; i < numFrames; ++i) {
            for (int j = 0; j < windowLength; ++j) {
                resultado[i][j] = data[i * hopLength + j];
            }
        }

        return resultado;
    }

    public static float[][][] frame(float[][] data, int hopLength,  int windowLength) {
        final int numSamples = data.length;
        final int numFrames = 1 + (int) Math.floor((numSamples - windowLength) / hopLength);

        float[][][] resultado = new float[numFrames][windowLength][data[0].length];

        for (int i = 0; i < numFrames; ++i) {
            for (int j = 0; j < windowLength; ++j) {
                resultado[i][j] = data[i * hopLength + j];
            }
        }

        return resultado;
    }

    public static float[] periodicHann(int windowLength) {
        float[] resultado = new float[windowLength];

        for (int i = 0; i < windowLength; ++i) {
            resultado[i] = (float) (0.5 - (0.5 * Math.cos(2 * Math.PI / windowLength * i)));
        }

        return resultado;
    }

    private static double log2(double x) {
        return Math.log(x) / Math.log(2);
    }

}
