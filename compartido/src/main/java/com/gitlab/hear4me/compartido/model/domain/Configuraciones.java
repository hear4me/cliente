package com.gitlab.hear4me.compartido.model.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Administrador de configuraciones */
public class Configuraciones {

    /** Asocia una configuración a cada modo de uso */
    private Map<ModoConfiguracion, Configuracion> configuraciones;
    private ModoConfiguracion modoConfiguracionSeleccionado;

    private boolean modoCasaAutomaticoConfigurado;
    private boolean modoCasaAutomaticoActivado;
    private List<RedModoCasa> redesModoCasa;

    private boolean modoDormirAutomaticoConfigurado;
    private boolean modoDormirAutomaticoActivado;
    private long horaDesde;
    private long horaHasta;

    private Dispositivo dispositivo;

    public Configuraciones() {
        modoConfiguracionSeleccionado = ModoConfiguracion.CASA;
        dispositivo = Dispositivo.CELULAR;
        configuraciones = new HashMap<>();
        redesModoCasa = new ArrayList<>();
        for (ModoConfiguracion modoConfiguracion : ModoConfiguracion.values()) {
            Configuracion configuracion = new Configuracion(modoConfiguracion);
            configuracion.inicializar();
            configuraciones.put(modoConfiguracion, configuracion);
        }
    }

    public Dispositivo getDispositivo() {
        return dispositivo;
    }

    public void setDispositivo(Dispositivo dispositivo) {
        this.dispositivo = dispositivo;
    }

    public Configuracion getConfiguracion(ModoConfiguracion modoConfiguracion) {
        return getConfiguraciones().get(modoConfiguracion);
    }

    public void setConfiguracion(ModoConfiguracion modoConfiguracion, Configuracion configuracion) {
        getConfiguraciones().put(modoConfiguracion, configuracion);
    }

    public Map<ModoConfiguracion, Configuracion> getConfiguraciones() {
        return configuraciones;
    }

    public ModoConfiguracion getModoConfiguracionSeleccionado() {
        return modoConfiguracionSeleccionado;
    }

    public void setModoConfiguracionSeleccionado(ModoConfiguracion modoConfiguracionSeleccionado) {
        this.modoConfiguracionSeleccionado = modoConfiguracionSeleccionado;
    }

    public boolean isModoCasaAutomaticoActivado() {
        return modoCasaAutomaticoActivado;
    }

    public List<RedModoCasa> getRedesModoCasa() {
        return redesModoCasa;
    }

    public void setModoCasaAutomaticoActivado(boolean nuevoEstado) {
        modoCasaAutomaticoActivado = nuevoEstado;
    }

    public boolean isModoCasaAutomaticoConfigurado() {
        return modoCasaAutomaticoConfigurado;
    }

    public void setModoCasaAutomaticoConfigurado(boolean modoCasaAutomaticoConfigurado) {
        this.modoCasaAutomaticoConfigurado = modoCasaAutomaticoConfigurado;
    }

    public boolean isModoDormirAutomaticoConfigurado() {
        return modoDormirAutomaticoConfigurado;
    }

    public void setModoDormirAutomaticoConfigurado(boolean nuevoEstado) {
        modoDormirAutomaticoConfigurado = nuevoEstado;
    }

    public boolean isModoDormirAutomaticoActivado() {
        return modoDormirAutomaticoActivado;
    }

    public void setModoDormirAutomaticoActivado(boolean nuevoEstado) {
        modoDormirAutomaticoActivado = nuevoEstado;
    }

    public long getHoraDesde() {
        return horaDesde;
    }

    public long getHoraHasta() {
        return horaHasta;
    }

    public void setHoraDesde(long hora) {
        horaDesde = hora;
    }

    public void setHoraHasta(long hora) {
        horaHasta = hora;
    }
}
