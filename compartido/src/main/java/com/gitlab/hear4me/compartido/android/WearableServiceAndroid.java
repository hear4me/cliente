package com.gitlab.hear4me.compartido.android;

import android.app.Activity;
import android.content.Context;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;

import java.util.List;

public class WearableServiceAndroid {

    private Context context;

    public WearableServiceAndroid(Context context) {
        this.context = context;
    }

    public void enviarMensaje(String path, byte[] datos) {
        Wearable.getNodeClient(context).getConnectedNodes().addOnCompleteListener((tarea) -> {
            GoogleApiAvailability availability = GoogleApiAvailability.getInstance();
            int resultCode = availability.isGooglePlayServicesAvailable(context);
            if (resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED) {
                return;
            }
            List<Node> nodes = tarea.getResult();
            for (Node node : nodes) {
                Wearable.getMessageClient(context).sendMessage(node.getId(), path, datos);
            }
        }
        );
    }

}
