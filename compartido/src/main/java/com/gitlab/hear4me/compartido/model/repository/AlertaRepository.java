package com.gitlab.hear4me.compartido.model.repository;

import com.gitlab.hear4me.compartido.model.domain.Alerta;

import java.util.List;

public interface AlertaRepository {
    List<Alerta> buscarTodas();
    void guardar(Alerta alerta);
    void actualizar(Alerta alerta);
    int contarTodasLasAlertasNoVistas();
    void marcarTodasComoVistas();
    void marcarTodasMenosLaUltima();
    List<Alerta> buscarTodasLasAlertasNoVistas();
    void borrarTodas();
    void borrarAlertasVistas();
}
