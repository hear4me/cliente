package com.gitlab.hear4me.compartido.util;

import org.jtransforms.fft.FloatFFT_1D;

import java.security.InvalidParameterException;

public class MathUtil {

    private MathUtil() {}

    public static float[][] multiplicacionElementos(float[][] matriz, float[] vector) {
        if (matriz[0].length != vector.length) return null;
        float[][] resultado = new float[matriz.length][matriz[0].length];

        for (int i = 0; i < matriz.length; ++i) {
            for (int j = 0; j < vector.length; ++j) {
                resultado[i][j] = matriz[i][j] * vector[j];
            }
        }

        return resultado;
    }

    /**
     * Calcula FFT en un nuevo array con padding de ceros o truncamiento segun la longitudFFT
     * @param data array de longitud N
     * @param longitudFFT si N > longitudFFT se hace padding de ceros, si N < longitudFFT se reduce el tamaño del array
     * @return array de longitud longitudFFT+2 si N es par, o longitudFFT+1 si N es impar.
     * Los elementos en posiciones pares son numeros reales y los elementos en posiciones impares
     * son numeros imaginarios.
     */
    public static float[] calcularFFT(float[] data, int longitudFFT) {
        float[] aux = new float[longitudFFT];
        float[] resultado;

        if (longitudFFT % 2 == 0) { // si es par
            resultado = new float[longitudFFT + 2];
        } else {
            resultado = new float[longitudFFT + 1];
        }

        System.arraycopy(data, 0, aux, 0, Math.min(data.length, longitudFFT));

        FloatFFT_1D fft = new FloatFFT_1D(longitudFFT);
        fft.realForward(aux);

        System.arraycopy(aux, 0, resultado, 0, longitudFFT);

        resultado[1] = 0;
        if (longitudFFT % 2 == 0) { // si es par
            resultado[longitudFFT] = aux[1];
            resultado[longitudFFT + 1] = 0;
        } else {
            resultado[longitudFFT - 1] = aux[longitudFFT - 1];
            resultado[longitudFFT] = aux[1];
        }

        return resultado;
    }

    public static float[] calcularAbsArrayComplejo(float[] complejos) {
        float[] resultado = new float[complejos.length / 2];
        for (int i = 0; i < resultado.length; ++i) {
            resultado[i] = (float) Math.sqrt(complejos[2*i] * complejos[2*i] + complejos[2*i+1] * complejos[2*i+1]);
        }
        return resultado;
    }

    /**
     * Devuelve un array {@code A} con {@code n} numeros, empezando por {@code start} y terminando en {@code stop},
     * de modo que se cumpla para todo {@code i}: {@code abs(A[i]-A[i-1]) = abs(A[i]-A[i+1])}.
     * Es decir, cada elemento esta separado por un mismo valor.
     * @param start
     * @param stop
     * @param n
     * @return
     */
    public static float[] linspace(float start, float stop, int n) {
        float[] resultado = new float[n];
        float espacio = (stop - start) / (n - 1);

        resultado[0] = start;
        resultado[n-1] = stop;

        for (int i = 1; i < n-1; ++i) {
            resultado[i] = resultado[i-1] + espacio;
        }

        return resultado;
    }

    public static float[][] linspace(float[] start, float[] stop, int n) {
        if (start.length != stop.length) throw new InvalidParameterException();
        float[][] resultado = new float[n][start.length];
        float[] espacios = new float[start.length];

        resultado[0] = start;
        resultado[n-1] = stop;

        for (int i = 0; i < espacios.length; ++i) {
            espacios[i] = (stop[i] - start[i]) / (n - 1);
        }

        for (int i = 1; i < n-1; ++i) {
            for (int j = 0; j < espacios.length; ++j) {
                resultado[i][j] = resultado[i - 1][j] + espacios[j];
            }
        }

        return resultado;
    }

    public static float[][] multiplicarMatrices(float[][] m1, float[][] m2) {
        if (m1[0].length != m2.length) {
            throw new IllegalArgumentException("La longitud de la segunda" +
                    " dimension del primer array debe ser igual a la primera" +
                    " dimension del segundo array.");
        }

        float[][] resultado = new float[m1.length][m2[0].length];
        for (int k = 0; k < m2[0].length; ++k) {
            for (int i = 0; i < m1.length; ++i) {
                for (int j = 0; j < m1[0].length; ++j) {
                    resultado[i][k] += m1[i][j] * m2[j][k];
                }
            }
        }
        return resultado;
    }
}
