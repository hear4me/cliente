package com.gitlab.hear4me.compartido.util;

public class ArrayUtil {

    private ArrayUtil() { }

    /**
     * Convierte un array con forma [a][b][c] a uno con forma [a][b][c][1]
     */
    public static float[][][][] remodelar(float[][][] array) {
        if (array == null) {
            throw new IllegalArgumentException("No se acepta nulo como parametro");
        }
        if (array[0][0].length == 0) {
            throw new IllegalArgumentException("No se acepta un array vacio como parametro");
        }

        float[][][][] resultado = new float[array.length][array[0].length][array[0][0].length][1];

        for (int i = 0; i < array.length; ++i) {
            for (int j = 0; j < array[0].length; ++j) {
                for (int k = 0; k < array[0][0].length; ++k) {
                    resultado[i][j][k][0] = array[i][j][k];
                }
            }
        }

        return resultado;
    }

}
