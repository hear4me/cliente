package com.gitlab.hear4me.compartido.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.lang.reflect.Type;

public class JsonUtil {

    private static final String FORMATO_FECHA = "dd/MM/yyyy_HH:mm:ss";

    public static <T> T fromJson(String json, Class<T> clase) {
        Gson gson = new GsonBuilder()
                .enableComplexMapKeySerialization()
                .setPrettyPrinting()
                .setDateFormat(FORMATO_FECHA)
                .create();
        return gson.fromJson(json, (Type) clase);
    }

    public static String toJson(Object objeto) {
        Gson gson = new GsonBuilder()
                .enableComplexMapKeySerialization()
                .setPrettyPrinting()
                .setDateFormat(FORMATO_FECHA)
                .create();
        return gson.toJson(objeto);
    }

    public static String prettyPrint(String jsonString) {
        JsonObject json = new JsonParser().parse(jsonString).getAsJsonObject();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(json);
    }
}
