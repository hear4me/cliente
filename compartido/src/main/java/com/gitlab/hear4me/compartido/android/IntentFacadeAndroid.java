package com.gitlab.hear4me.compartido.android;

public interface IntentFacadeAndroid {
    void activarMicrofono();
    void desactivarMicrofono();
    void mostrarInterfazAlerta(String alertaJson);
}
