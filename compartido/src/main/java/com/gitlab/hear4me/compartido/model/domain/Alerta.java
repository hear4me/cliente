package com.gitlab.hear4me.compartido.model.domain;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.sql.Timestamp;

@Entity
public class Alerta {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private Prediccion prediccion;
    private Timestamp tiempo;
    private boolean vista;

    public Alerta() {}

    public Alerta(Prediccion prediccion) {
        this.prediccion = prediccion;
        tiempo = new Timestamp(System.currentTimeMillis());
        vista = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Prediccion getPrediccion() {
        return prediccion;
    }

    public void setPrediccion(Prediccion prediccion) {
        this.prediccion = prediccion;
    }

    public Timestamp getTiempo() {
        return tiempo;
    }

    public void setTiempo(Timestamp tiempo) {
        this.tiempo = tiempo;
    }

    public boolean isVista() {
        return vista;
    }

    public void setVista(boolean vista) {
        this.vista = vista;
    }
}
