package com.gitlab.hear4me.compartido.model.domain;

public enum ModoConfiguracion {
    CASA, CALLE, CASA_DORMIR, CALLE_DORMIR
}
