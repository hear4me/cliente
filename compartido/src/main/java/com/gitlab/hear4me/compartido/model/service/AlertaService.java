package com.gitlab.hear4me.compartido.model.service;

import com.gitlab.hear4me.compartido.model.domain.Alerta;
import com.gitlab.hear4me.compartido.model.domain.Prediccion;
import com.gitlab.hear4me.compartido.model.repository.AlertaRepository;

import java.util.List;
import java.util.stream.Collectors;

public class AlertaService {

    private AlertaRepository alertaRepository;

    public AlertaService(AlertaRepository alertaRepository) {
        this.alertaRepository = alertaRepository;
    }

    public void activarAlerta(Prediccion prediccion) {
        Alerta alerta = new Alerta(prediccion);
        alertaRepository.guardar(alerta);
    }

    public Alerta buscarAlertaMasAntiguaSinVer() {
        if (alertaRepository.contarTodasLasAlertasNoVistas() > 0) {
            return alertaRepository.buscarTodasLasAlertasNoVistas().get(0);
        }
        return null;
    }

    public void borrarAlertasVistas() {
        alertaRepository.borrarAlertasVistas();
    }

    public void marcarAlertaActual() {
        Alerta alertaActual = buscarAlertaMasAntiguaSinVer();
        if (alertaActual == null) {
            throw new IllegalStateException("No existe ninguna alerta para marcar");
        }
        alertaActual.setVista(true);
        alertaRepository.actualizar(alertaActual);
    }

    public int contarTodasLasAlertasNoVistas() {
        return alertaRepository.contarTodasLasAlertasNoVistas();
    }

    public void marcarTodasLasAlertasMenosLaUltima() {
        alertaRepository.marcarTodasMenosLaUltima();
    }

    public List<Alerta> buscarHistorial() {
        return alertaRepository.buscarTodas().stream()
                .sorted((a1, a2) ->
                        Long.compare(a2.getTiempo().getTime(), a1.getTiempo().getTime()))
                .collect(Collectors.toList());
    }

    public void borrarHistorial() {
        alertaRepository.borrarTodas();
    }

    public void marcarAlerta(Alerta alerta) {
        alerta.setVista(true);
        alertaRepository.actualizar(alerta);
    }
}
