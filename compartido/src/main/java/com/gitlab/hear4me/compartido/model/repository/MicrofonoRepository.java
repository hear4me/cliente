package com.gitlab.hear4me.compartido.model.repository;

import com.gitlab.hear4me.compartido.model.domain.EstadoMicrofono;

public interface MicrofonoRepository {
    EstadoMicrofono cargar();
    void guardar(EstadoMicrofono estado);
}
