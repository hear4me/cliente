package com.gitlab.hear4me.compartido.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileUtil {

    public static String leerAString(File archivo) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new FileReader(archivo));

        String contenido;
        while ((contenido = br.readLine()) != null){
            sb.append(contenido).append("\n");
        }

        return sb.toString();
    }

    public static void guardar(File archivo, String texto) throws IOException {
        FileWriter fileWriter = new FileWriter(archivo, false);
        fileWriter.write(texto);
        fileWriter.flush();
    }
}
