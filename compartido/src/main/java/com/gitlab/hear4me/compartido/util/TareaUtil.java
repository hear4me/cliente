package com.gitlab.hear4me.compartido.util;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.Executors;

public class TareaUtil {

    public static void ejecutar(Runnable tarea) {
        Executors.newSingleThreadExecutor().submit(tarea);
    }

    public static void ejecutarEnUiThread(Runnable tarea) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(tarea);
    }

    public static void repetirEnUiThread(Handler handler, Runnable tarea, int milisegundos) {
        handler.post(tarea);
        handler.postDelayed(() -> repetirEnUiThread(handler, tarea, milisegundos), milisegundos);
    }

}
