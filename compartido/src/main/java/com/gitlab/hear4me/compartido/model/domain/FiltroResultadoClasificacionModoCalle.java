package com.gitlab.hear4me.compartido.model.domain;

import com.google.common.collect.ImmutableMap;

import static com.gitlab.hear4me.compartido.model.domain.Categoria.ALARMA_AUTO;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.BEBE_LLORANDO;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.BOCINA;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.DISPAROS;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.SIRENA_AMBULANCIA;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.SIRENA_BOMBEROS;
import static com.gitlab.hear4me.compartido.model.domain.Categoria.SIRENA_POLICIA;

class FiltroResultadoClasificacionModoCalle extends FiltroResultadoClasificacion {

    FiltroResultadoClasificacionModoCalle() {
        super();

        for (Categoria categoria : Categoria.deModoCalle()) {
            repeticiones.put(categoria, 0);
        }

        limitesRepeticiones = ImmutableMap.<Categoria, Integer>builder()
                .put(ALARMA_AUTO, 10)
                .put(SIRENA_AMBULANCIA, 10)
                .put(BEBE_LLORANDO, 5)
                .put(BOCINA, 2)
                .put(SIRENA_BOMBEROS, 10)
                .put(DISPAROS, 2)
                .put(SIRENA_POLICIA, 10)
                .build();

        repeticionesMinimas = ImmutableMap.<Categoria, Integer>builder()
                .put(ALARMA_AUTO, 1)
                .put(SIRENA_AMBULANCIA, 0)
                .put(BEBE_LLORANDO, 1)
                .put(BOCINA, 1)
                .put(SIRENA_BOMBEROS, 1)
                .put(DISPAROS, 1)
                .put(SIRENA_POLICIA, 0)
                .build();

        umbralesRepeticiones = ImmutableMap.<Categoria, Float>builder()
                .put(ALARMA_AUTO, 0.87f)
                .put(SIRENA_AMBULANCIA, 0.65f)
                .put(BEBE_LLORANDO, 0.75f)
                .put(BOCINA, 0.92f)
                .put(SIRENA_BOMBEROS, 0.75f)
                .put(DISPAROS, 0.75f)
                .put(SIRENA_POLICIA, 0.75f)
                .build();

        multiplicadoresRepeticion = ImmutableMap.<Categoria, Float>builder()
                .put(ALARMA_AUTO, 0.05f)
                .put(SIRENA_AMBULANCIA, 0.15f)
                .put(BEBE_LLORANDO, 0.15f)
                .put(BOCINA, 0.05f)
                .put(SIRENA_BOMBEROS, 0.15f)
                .put(DISPAROS, 0.05f)
                .put(SIRENA_POLICIA, 0.15f)
                .build();

        umbralesPorCategoria = ImmutableMap.<Categoria, Float>builder()
                .put(ALARMA_AUTO, 0.91f)
                .put(SIRENA_AMBULANCIA, 0.75f)
                .put(BEBE_LLORANDO, 0.89f)
                .put(BOCINA, 0.98f)
                .put(SIRENA_BOMBEROS, 0.89f)
                .put(DISPAROS, 0.98f)
                .put(SIRENA_POLICIA, 0.89f)
                .build();
    }
}
