package com.gitlab.hear4me.compartido.model.domain;

public class Prediccion {

    private float valor;
    private Categoria categoria;

    public Prediccion(Categoria categoria, float valor) {
        this.categoria = categoria;
        this.valor = valor;
    }

    public float getValor() {
        return valor;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    @Override
    public String toString() {
        return String.format("%s: %.2f%%", categoria.getNombre(), valor * 100);
    }
}
