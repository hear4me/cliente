package com.gitlab.hear4me.compartido.model.domain;

import java.util.Collections;
import java.util.List;

public class ResultadoClasificacion {

    private final List<Prediccion> predicciones;
    
    private boolean ordenado;

    public ResultadoClasificacion(List<Prediccion> predicciones) {
        this.predicciones = predicciones;
    }

    public Prediccion getPrediccion(int i) {
        if (!ordenado) ordenar();
        return predicciones.get(i);
    }

    public List<Prediccion> getPredicciones() {
        return predicciones;
    }
    
    private void ordenar() {
        ordenado = true;
        Collections.sort(predicciones, (p1, p2) -> Float.compare(p2.getValor(), p1.getValor()));
    }
}
