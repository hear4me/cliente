package com.gitlab.hear4me.compartido.util;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static String formatHora(Date tiempo) {
        String formato = "HH:mm";
        DateFormat formater = new SimpleDateFormat(formato);
        return formater.format(tiempo);
    }

    public static String formatFecha(Date tiempo) {
        String formato = "dd/MM/yyyy";
        DateFormat formater = new SimpleDateFormat(formato);
        return formater.format(tiempo);
    }
}
