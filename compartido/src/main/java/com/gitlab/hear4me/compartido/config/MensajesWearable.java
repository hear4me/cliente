package com.gitlab.hear4me.compartido.config;

public class MensajesWearable {

    /*** CELULAR ***/

    // UI Main
    public static final String PHONE_WRITE_ESTADO_MIC_PATH = "/phone-modifico-estado-mic";
    public static final String PHONE_REQ_ANS_ESTADO_MIC_PATH = "/phone-estado-mic";

    // UI Ajustes
    public static final String PHONE_WRITE_CONFIG_PATH = "/phone-modifico-config";
    public static final String PHONE_REQ_ANS_CONFIG_PATH = "/phone-config";

    // UI Alerta
    public static final String PHONE_ACTIVA_ALERTA_PATH = "/phone-alerta-activada";

    /*** RELOJ ***/

    // UI Main
    public static final String WATCH_REQ_ESTADO_MIC_PATH = "/watch-require-estado-mic";
    public static final String WATCH_WRITE_ESTADO_MIC_PATH = "/watch-modifico-estado-mic";

    // UI Ajustes
    public static final String WATCH_WRITE_CONFIG_PATH = "/watch-modifico-config";
    public static final String WATCH_REQ_CONFIG_PATH = "/watch-requiere-config";

    private MensajesWearable() {}
}
