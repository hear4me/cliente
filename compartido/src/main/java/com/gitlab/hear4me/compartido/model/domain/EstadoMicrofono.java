package com.gitlab.hear4me.compartido.model.domain;

public class EstadoMicrofono {

    private boolean estado = false;

    public EstadoMicrofono(boolean estado) {
        this.estado = estado;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
}
