package com.gitlab.hear4me.compartido.model.service;

import com.gitlab.hear4me.compartido.android.IntentFacadeAndroid;
import com.gitlab.hear4me.compartido.model.domain.EstadoMicrofono;
import com.gitlab.hear4me.compartido.model.repository.MicrofonoRepository;

import javax.inject.Inject;

public class MicrofonoService {

    private IntentFacadeAndroid intentFacadeAndroid;
    private MicrofonoRepository microfonoRepository;

    @Inject
    public MicrofonoService(
            MicrofonoRepository microfonoRepository,
            IntentFacadeAndroid intentFacadeAndroid) {
        this.microfonoRepository = microfonoRepository;
        this.intentFacadeAndroid = intentFacadeAndroid;
    }

    public void activarMicrofono() {
        cambiarEstadoMicrofono(true);
        intentFacadeAndroid.activarMicrofono();
    }

    public void desactivarMicrofono() {
        cambiarEstadoMicrofono(false);
        intentFacadeAndroid.desactivarMicrofono();
    }

    public boolean obtenerEstadoMicrofono() {
        return microfonoRepository.cargar().getEstado();
    }

    public void cambiarEstadoMicrofono(boolean estado) {
        EstadoMicrofono estadoMicrofono = microfonoRepository.cargar();
        estadoMicrofono.setEstado(estado);
        microfonoRepository.guardar(estadoMicrofono);
    }

    public void actualizarEstadoMicrofono(boolean estado) {
        EstadoMicrofono estadoMicrofono = microfonoRepository.cargar();
        estadoMicrofono.setEstado(estado);
    }

}
