package com.gitlab.hear4me.compartido.model.domain;

public enum ModoNotificacion {
    VIBRACION, LUZ
}
