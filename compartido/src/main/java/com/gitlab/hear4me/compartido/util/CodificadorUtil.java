package com.gitlab.hear4me.compartido.util;

import com.google.common.io.BaseEncoding;

public abstract class CodificadorUtil {

    private CodificadorUtil() {}

    public static String codificar(byte[] bytes) {
        return BaseEncoding.base64().encode(bytes);
    }

    public static byte[] decodificar(String str) {
        return BaseEncoding.base64().decode(str);
    }
}
