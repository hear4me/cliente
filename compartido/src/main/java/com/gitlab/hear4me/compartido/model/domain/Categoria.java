package com.gitlab.hear4me.compartido.model.domain;

public enum Categoria {
    ALARMA_AUTO,
    ALARMA_INCENDIO,
    BEBE_LLORANDO,
    BOCINA,
    DISPAROS,
    GATO,
    PERRO,
    SIRENA_AMBULANCIA,
    SIRENA_BOMBEROS,
    SIRENA_POLICIA,
    VIDRIO_ROTO;

    @Override
    public String toString() {
        return name().toLowerCase();
    }

    public String getNombre() {
        String nombreSinFormato = this.toString();
        String nombreConFormato = "";
        String[] split = nombreSinFormato.split("_");
        for (String palabra : split) {
            nombreConFormato += palabra.substring(0, 1).toUpperCase() + palabra.substring(1);
            nombreConFormato += " ";
        }
        return nombreConFormato;
    }

    public static Categoria[] deModoCasa() {
        return new Categoria[] {
            ALARMA_AUTO,
            ALARMA_INCENDIO,
            PERRO,
            SIRENA_AMBULANCIA,
            BEBE_LLORANDO,
            SIRENA_BOMBEROS,
            DISPAROS,
            GATO,
            SIRENA_POLICIA,
            VIDRIO_ROTO
        };
    }

    public static Categoria[] deModoCalle() {
        return new Categoria[] {
            ALARMA_AUTO,
            SIRENA_AMBULANCIA,
            BEBE_LLORANDO,
            BOCINA,
            SIRENA_BOMBEROS,
            DISPAROS,
            SIRENA_POLICIA
        };
    }

}
