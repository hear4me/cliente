package com.gitlab.hear4me.compartido.model.domain;

import android.content.Context;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.io.InputStream;

@RunWith(AndroidJUnit4.class)
public class ClasificadorAudioTest {

    @Test
    public void clasificar_inputValido_retornaPerroPrimero() throws IOException {
        Context appContext = ApplicationProvider.getApplicationContext();
        ModelLoader modelLoader = new ModelLoaderAsset("clasificador_sonidos.tflite", appContext);

        ClasificadorAudio ca = new ClasificadorAudio(modelLoader, ModoConfiguracion.CASA,  3);

        InputStream is = getClass().getResourceAsStream("/perro_ladrando_1.wav");

        ResultadoClasificacion resultadoClasificacion = ca.clasificar(new AudioWav(IOUtils.toByteArray(is)));

        Assert.assertEquals(Categoria.PERRO, resultadoClasificacion.getPrediccion(0).getCategoria());
    }

    @Test
    public void clasificar_inputValido_retornaBebePrimero() throws IOException {
        Context appContext = ApplicationProvider.getApplicationContext();
        ModelLoader modelLoader = new ModelLoaderAsset("clasificador_sonidos.tflite", appContext);

        ClasificadorAudio ca = new ClasificadorAudio(modelLoader, ModoConfiguracion.CASA, 3);

        InputStream is = getClass().getResourceAsStream("/bebe_llorando_1.wav");

        ResultadoClasificacion resultadoClasificacion = ca.clasificar(new AudioWav(IOUtils.toByteArray(is)));

        Assert.assertEquals(Categoria.BEBE_LLORANDO, resultadoClasificacion.getPrediccion(0).getCategoria());
    }

    @Test
    public void clasificar_inputValido_retornaBomberoPrimero() throws IOException {
        Context appContext = ApplicationProvider.getApplicationContext();
        ModelLoader modelLoader = new ModelLoaderAsset("clasificador_sonidos.tflite", appContext);

        ClasificadorAudio ca = new ClasificadorAudio(modelLoader, ModoConfiguracion.CASA, 3);

        InputStream is = getClass().getResourceAsStream("/sirena_bomberos_1.wav");

        ResultadoClasificacion resultadoClasificacion = ca.clasificar(new AudioWav(IOUtils.toByteArray(is)));

        Assert.assertEquals(Categoria.SIRENA_BOMBEROS, resultadoClasificacion.getPrediccion(0).getCategoria());
    }
}
